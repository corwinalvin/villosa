<?php
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}

function getDefault($value='')
{
	if ($value ==1) {
		return 'Default';
	}else{
		return 'Not Default';
	}
}

function getVariant($isi='')
{
	$value = preg_replace('/[^A-Za-z]/', '', $isi);
	if ($value == "cpsr") {
		return "Warna, Pattern, Size, Roll";
	}else if($value == "cps"){
		return "Warna, Pattern, Size";
	}else if($value == "cps"){
		return "Warna, Pattern";
	}else{
		return "Warna";
	}
}

function getStatusItem($value='')
{
	if($value == 1){
		return 'Approved';
	}else{
		return 'Not Approved';
	}
}

function getStatusCustomer($var)
{
	if($var == 1){
		return 'Banned';
	}else{
		return 'Tidak Banned';
	}
}
