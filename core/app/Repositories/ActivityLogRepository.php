<?php

namespace App\Repositories;

use App\Models\ActivityLog;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ActivityLogRepository
 * @package App\Repositories
 * @version February 23, 2018, 5:39 am UTC
 *
 * @method ActivityLog findWithoutFail($id, $columns = ['*'])
 * @method ActivityLog find($id, $columns = ['*'])
 * @method ActivityLog first($columns = ['*'])
*/
class ActivityLogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'log_name',
        'description',
        'subject_id',
        'subject_type',
        'causer_id',
        'causer_type',
        'properties'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ActivityLog::class;
    }
}
