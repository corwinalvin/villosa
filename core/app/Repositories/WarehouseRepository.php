<?php

namespace App\Repositories;

use App\Models\Warehouse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class WarehouseRepository
 * @package App\Repositories
 * @version February 17, 2018, 2:26 pm UTC
 *
 * @method Warehouse findWithoutFail($id, $columns = ['*'])
 * @method Warehouse find($id, $columns = ['*'])
 * @method Warehouse first($columns = ['*'])
*/
class WarehouseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'default',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Warehouse::class;
    }
}
