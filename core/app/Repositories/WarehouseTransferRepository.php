<?php

namespace App\Repositories;

use App\Models\WarehouseTransfer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class WarehouseTransferRepository
 * @package App\Repositories
 * @version February 25, 2018, 1:49 pm UTC
 *
 * @method WarehouseTransfer findWithoutFail($id, $columns = ['*'])
 * @method WarehouseTransfer find($id, $columns = ['*'])
 * @method WarehouseTransfer first($columns = ['*'])
*/
class WarehouseTransferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'warehouse_from',
        'warehouse_to',
        'item_variant',
        'value',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarehouseTransfer::class;
    }
}
