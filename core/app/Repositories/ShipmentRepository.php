<?php

namespace App\Repositories;

use App\Models\Shipment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ShipmentRepository
 * @package App\Repositories
 * @version February 28, 2018, 6:40 am UTC
 *
 * @method Shipment findWithoutFail($id, $columns = ['*'])
 * @method Shipment find($id, $columns = ['*'])
 * @method Shipment first($columns = ['*'])
*/
class ShipmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'so_id',
        'inv_id',
        'date',
        'status',
        'reference',
        'note',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Shipment::class;
    }
}
