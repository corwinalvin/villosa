<?php

namespace App\Repositories;

use App\Models\Satuan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SatuanRepository
 * @package App\Repositories
 * @version February 17, 2018, 2:33 pm UTC
 *
 * @method Satuan findWithoutFail($id, $columns = ['*'])
 * @method Satuan find($id, $columns = ['*'])
 * @method Satuan first($columns = ['*'])
*/
class SatuanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'created_by',
        'update_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Satuan::class;
    }
}
