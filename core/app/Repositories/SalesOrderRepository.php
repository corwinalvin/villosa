<?php

namespace App\Repositories;

use App\Models\SalesOrder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SalesOrderRepository
 * @package App\Repositories
 * @version February 21, 2018, 3:36 am UTC
 *
 * @method SalesOrder findWithoutFail($id, $columns = ['*'])
 * @method SalesOrder find($id, $columns = ['*'])
 * @method SalesOrder first($columns = ['*'])
*/
class SalesOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'sales_id',
        'payment_id',
        'date',
        'sales_type_id',
        'reference',
        'status',
        'note',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SalesOrder::class;
    }
}
