<?php

namespace App\Repositories;

use App\Models\ItemVariant;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ItemVariantRepository
 * @package App\Repositories
 * @version February 20, 2018, 9:14 am UTC
 *
 * @method ItemVariant findWithoutFail($id, $columns = ['*'])
 * @method ItemVariant find($id, $columns = ['*'])
 * @method ItemVariant first($columns = ['*'])
*/
class ItemVariantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id',
        'warna',
        'pattern',
        'size',
        'roll'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ItemVariant::class;
    }
}
