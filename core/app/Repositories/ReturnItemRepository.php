<?php

namespace App\Repositories;

use App\Models\ReturnItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReturnItemRepository
 * @package App\Repositories
 * @version March 11, 2018, 2:18 pm UTC
 *
 * @method ReturnItem findWithoutFail($id, $columns = ['*'])
 * @method ReturnItem find($id, $columns = ['*'])
 * @method ReturnItem first($columns = ['*'])
*/
class ReturnItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sales_id',
        'shipment_id',
        'keterangan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReturnItem::class;
    }
}
