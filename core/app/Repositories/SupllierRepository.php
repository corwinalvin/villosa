<?php

namespace App\Repositories;

use App\Models\Supllier;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SupllierRepository
 * @package App\Repositories
 * @version February 17, 2018, 2:34 pm UTC
 *
 * @method Supllier findWithoutFail($id, $columns = ['*'])
 * @method Supllier find($id, $columns = ['*'])
 * @method Supllier first($columns = ['*'])
*/
class SupllierRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'phone',
        'street',
        'city',
        'state',
        'zipcode',
        'status',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Supllier::class;
    }
}
