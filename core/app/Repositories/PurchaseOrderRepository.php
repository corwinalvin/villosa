<?php

namespace App\Repositories;

use App\Models\PurchaseOrder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PurchaseOrderRepository
 * @package App\Repositories
 * @version February 28, 2018, 4:42 am UTC
 *
 * @method PurchaseOrder findWithoutFail($id, $columns = ['*'])
 * @method PurchaseOrder find($id, $columns = ['*'])
 * @method PurchaseOrder first($columns = ['*'])
*/
class PurchaseOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'supplier',
        'date',
        'reference',
        'status',
        'note',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PurchaseOrder::class;
    }
}
