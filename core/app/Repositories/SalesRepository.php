<?php

namespace App\Repositories;

use App\Models\Sales;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SalesRepository
 * @package App\Repositories
 * @version February 17, 2018, 2:33 pm UTC
 *
 * @method Sales findWithoutFail($id, $columns = ['*'])
 * @method Sales find($id, $columns = ['*'])
 * @method Sales first($columns = ['*'])
*/
class SalesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'tax_included',
        'default_sales_types',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sales::class;
    }
}
