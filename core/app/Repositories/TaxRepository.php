<?php

namespace App\Repositories;

use App\Models\Tax;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TaxRepository
 * @package App\Repositories
 * @version March 2, 2018, 7:29 am UTC
 *
 * @method Tax findWithoutFail($id, $columns = ['*'])
 * @method Tax find($id, $columns = ['*'])
 * @method Tax first($columns = ['*'])
*/
class TaxRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'rate',
        'default_taxes',
        'created_by',
        'deleted_by',
        'updated_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tax::class;
    }
}
