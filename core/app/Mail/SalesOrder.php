<?php

namespace App\Mail;

use App\Models\SalesOrder as SO;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SalesOrder extends Mailable
{
    use Queueable, SerializesModels;

    protected $so, $pdf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SO $so, $pdf)
    {
        $this->so = $so;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('vendor.mail.sales')->with([
            'order_id' => $this->so->reference,
            'order_price' => $this->so->amount,
            'intro' => 'Sales Order Notifikasi',
            'status' => '<strong>disetujui</strong>'
        ])->attachData($this->pdf, $this->so->reference.'.pdf', [
            'mime' => 'application/pdf',
         ]);
    }
}
