<?php

namespace App\Mail;

use App\Models\SalesOrder as SO;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;
    protected $so;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SO $so)
    {
        $this->so = $so;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('vendor.mail.sales')->with([
            'order_id' => $this->so->reference,
            'order_price' => $this->so->amount,
            'intro' => 'Invoice Notifikasi',
            'status' => '<strong>dibayar</strong>'
        ]);
    }
}
