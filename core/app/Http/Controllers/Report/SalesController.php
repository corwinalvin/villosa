<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SalesOrder;
use App\Models\Supplier;
use App\Models\Customer;
use App\Models\PaymentMethod;
use App\Models\Sales;
use DB;
use Auth;

class SalesController extends Controller
{
	public function __construct()
	{
		$data['customers'] = Customer::get();
		$data['payments'] = PaymentMethod::get();
		$data['types'] = Sales::get();
		view()->share($data);
	}

    public function index()
    {
    	$rp = SalesOrder::select(DB::Raw('YEAR(sales_orders.created_at) as year, QUARTER(sales_orders.created_at) as quarter , count(sales_orders.id) as idcount'))->groupBy(DB::raw('YEAR(sales_orders.created_at)',  'QUARTER(sales_orders.created_at)'))->orderByRaw('YEAR(created_at)','QUARTER(created_at)')->get();
    	$data['report_graph'] = $rp;
    	$data['total_sales'] = SalesOrder::get()->count();
    	$data['item_sold'] = SalesOrder::join('sales_order_details', 'sales_orders.id', 'sales_order_details.sales_id')->groupBy('sales_order_details.item_id_variant')->where('status_invoice', '>=' ,1)->get()->count();
    	$data['cost_value'] = rupiah(SalesOrder::selectRaw('sum(amount) as cost_value')->first()->cost_value);
    	$data['percentage'] = (SalesOrder::where( DB::raw('MONTH(created_at)'), '=', date('n') )->get()->count() / 100) * SalesOrder::where(DB::raw('MONTH(created_at)'), '=', date('m'))->get()->count();
    	return view('reports.sales.index', $data);
    }

    public function detail($value='')
    {
    	return view('reports.sales.table', compact('value'));
	}
	
	public function filter(Request $request){
		$input = $request->all();
		if($input['early_date'] !== null && $input['end_date'] !== null){
			$input['date'] = array('early_date' => $input['early_date'], 'end_date' => $input['end_date']);
		}
		unset($input['early_date']);
		unset($input['end_date']);
		$hasil = SalesOrder::filter($input)->get();
		return view('reports.sales.filter', compact('hasil'));
	}
}
