<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ItemVariant;
use App\Models\Item;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Warehouse;
use App\Models\Department;
use DB;

class WarehousesController extends Controller
{
	public function __construct(){
		$data['brands'] = Brand::get();
		$data['categories'] = Category::get();
		$data['warehouses'] = Warehouse::get();
		$data['departments'] = Department::get();
		view()->share($data);
	}

     public function index()
    {
    	$data['report_graph'] = Item::join('item_variant', 'items.id', 'item_variant.item_id')->join('warehouses', 'item_variant.warehouse','warehouses.id')->select(DB::raw('items.name, sum(item_variant.value) as qtyItem, warehouses.name as whName'))->groupBy('item_variant.item_id')->get();
    	$data['item_stock'] = array_sum(ItemVariant::join('warehouses', 'item_variant.warehouse', 'warehouses.id')->groupBy('item_variant.item_id', 'item_variant.id')->select(DB::raw('sum(item_variant.value) as item_stock'))->pluck('item_stock')->toArray());
    	$data['cost_value'] = rupiah(ItemVariant::selectRaw('sum(price*value) as cost_value')->first()->cost_value);
    	return view('reports.warehouse.index', $data);
    }

    public function detail($value='')
    {
    	return view('reports.warehouse.table', compact('value'));
    }

    public function filter(Request $request){
		$input = $request->all();
		$data = Item::filter($input)->get();
		return view('reports.warehouse.filter', compact('data'));
	}

	public function apiNamaBarang(){
		$item = Item::filter(array('name' => request()->get('q')))->get();
		return response()->json($item);
	}

	public function apiNamaVariant(){
		$id = Item::where('name', request()->id)->first()->id;
		$items = ItemVariant::where('item_id', $id)->groupBy('id_variant')->get();
		$option = "<option value>-Pilih Variant-</option>";
		foreach($items as $item){
			$option.='<option value="'.$item->id.'">'.$item->warna.' '.$item->pattern.' '.$item->size.' '.$item->roll.'</option>';
		}
		return $option;
	}
}
