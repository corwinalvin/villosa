<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PurchaseOrder;
use DB;
use Auth;
use App\Models\Supllier;
use App\Models\PaymentMethod;
class PurchaseController extends Controller
{	
	public function __construct()
	{
		$data['suppliers'] = Supllier::get();
		$data['payments'] = PaymentMethod::get();
		view()->share($data);
	}
  	public function index()
    {
    	$rp = PurchaseOrder::select(DB::Raw('YEAR(purchase_order.created_at) as year, QUARTER(purchase_order.created_at) as quarter , count(purchase_order.id) as idcount'))->groupBy(DB::raw('YEAR(purchase_order.created_at)',  'QUARTER(purchase_order.created_at)'))->orderByRaw('YEAR(created_at)','QUARTER(created_at)')->get();
    	$data['report_graph'] = $rp;
    	$data['total_sales'] = PurchaseOrder::get()->count();
    	$data['item_sold'] = PurchaseOrder::join('purchase_order_details', 'purchase_order.id', 'purchase_order_details.purchase_order_id')->groupBy('purchase_order_details.item_id_variant')->get()->count();
    	$data['cost_value'] = rupiah(PurchaseOrder::selectRaw('sum(amount) as cost_value')->first()->cost_value);
    	$data['percentage'] = (PurchaseOrder::where( DB::raw('MONTH(created_at)'), '=', date('n') )->get()->count() / 100) * PurchaseOrder::where(DB::raw('MONTH(created_at)'), '=', date('m'))->get()->count();
    	return view('reports.purchase.index', $data);
    }

    public function detail($value='')
    {
    	return view('reports.purchase.table', compact('value'));
	}
	
	public function filter(Request $request){
		$input = $request->all();
		if($input['early_date'] !== null && $input['end_date'] !== null){
			$input['date'] = array('early_date' => $input['early_date'], 'end_date' => $input['end_date']);
		}
		unset($input['early_date']);
		unset($input['end_date']);
		$hasil = PurchaseOrder::filter($input)->get();
		return view('reports.purchase.filter', compact('hasil'));
	}
}
