<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSalesOrderRequest;
use App\Http\Requests\UpdateSalesOrderRequest;
use App\Repositories\SalesOrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\DataTables\InvoiceDataTable;
use App\DataTables\InvoiceUnpaidDataTable;
use App\Models\Customer;
use App\Models\PaymentMethod;
use App\Models\Sales;
use App\Models\SalesOrder;

class InvoicesController extends AppBaseController
{

    private $salesOrderRepository;

    public function __construct(SalesOrderRepository $salesOrderRepo)
    {
        $this->salesOrderRepository = $salesOrderRepo;
    }


    /**
     * Display a listing of the InvoicesController.
     *
     * @param Request $request
     * @return Response
     */

    public function index(InvoiceUnpaidDataTable $invoiceUnpaidDataTable)
    {
        return $invoiceUnpaidDataTable->render('sales_orders.invoice.index');
    }    

    public function getPaid(InvoiceDataTable $dataTable)
    {
        return $dataTable->render('sales_orders.invoice.index');
    }

    /**
     * Show the form for creating a new InvoicesController.
     *
     * @return Response
     */
    public function create()
    {
        return view('invoices_controllers.create');
    }

    /**
     * Store a newly created InvoicesController in storage.
     *
     * @param CreateInvoicesControllerRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoicesControllerRequest $request)
    {
        $input = $request->all();

        $invoicesController = $this->invoicesControllerRepository->create($input);

        Flash::success('Invoices Controller saved successfully.');

        return redirect(route('invoicesControllers.index'));
    }


    /**
     * Display the specified InvoicesController.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $salesOrder = $this->salesOrderRepository
            ->with('buyer')
            ->with('detailOrders.items')
            ->with('detailOrders.itemVariant')
            ->findWithoutFail($id);
        
        if (empty($salesOrder)) {
            Flash::error('Sales Order not found');

            return redirect(route('salesOrders.index'));
        }
        return view('sales_orders.invoice.show')->with('salesOrder', $salesOrder);
    }

    /**
     * Show the form for editing the specified InvoicesController.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $salesOrder = $this->salesOrderRepository->findWithoutFail($id);

        if (empty($salesOrder)) {
            Flash::error('Invoices not found');

            return redirect(route('salesOrders.index'));
        }

        foreach (Customer::select('id', 'name')->get() as $key => $value) {
            $cus[$value->id] = $value->name;
        }   
        foreach (PaymentMethod::select('id', 'name')->get() as $key => $value) {
            $pay[$value->id] = $value->name;
        }
        foreach (Sales::select('id', 'name')->get() as $key => $value) {
            $sal[$value->id] = $value->name;
        }
       
        $data['customers'] = $cus;
        $data['payments'] = $pay;
        $data['sales'] = $sal;
        $data['code'] = self::kode();
        $data['salesOrder'] = $salesOrder;

        return view('sales_orders.edit')->with($data);
    }

    /**
     * Update the specified InvoicesController in storage.
     *
     * @param  int              $id
     * @param UpdateInvoicesControllerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoicesControllerRequest $request)
    {
        $invoicesController = $this->invoicesControllerRepository->findWithoutFail($id);

        if (empty($invoicesController)) {
            Flash::error('Invoices Controller not found');

            return redirect(route('invoicesControllers.index'));
        }

        $invoicesController = $this->invoicesControllerRepository->update($request->all(), $id);

        Flash::success('Invoices Controller updated successfully.');

        return redirect(route('invoicesControllers.index'));
    }

    /**
     * Remove the specified InvoicesController from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $salesOrder = $this->salesOrderRepository->findWithoutFail($id);

        if (empty($salesOrder)) {
            Flash::error('Invoices not found');

            return redirect(route('invoices.index'));
        }

        SalesOrderDetail::whereSalesId($id)->delete();

        $this->salesOrderRepository->delete($id);

        Flash::success('Invoice deleted successfully.');

        return redirect(route('invoices.index'));
    }

    public function kode()
    {
        $reperence = SalesOrder::selectRaw('RIGHT(RTRIM(reference), 5) as kode, SUBSTR(reference, 1, 3) as lain')
                                ->orderBy('lain')
                                ->where(DB::raw("SUBSTR(reference, 1, 3)"), 'INV')
                                ->limit(1)
                                ->first();
        if($reperence){
            $kode = intval($reperence->kode) + 1;
        }else{
            $kode = 1;
        }
        return $kodemax = str_pad($kode, 5,"0", STR_PAD_LEFT);
    }
}
