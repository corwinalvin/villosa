<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePurchaseOrderRequest;
use App\Http\Requests\UpdatePurchaseOrderRequest;
use App\Repositories\PurchaseOrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Supllier;
use App\Models\PurchaseOrder;
use App\Models\ItemVariant;
use App\Models\PurchaseOrderDetail;
use PDF;

class PurchaseOrderController extends AppBaseController
{
    /** @var  PurchaseOrderRepository */
    private $purchaseOrderRepository;

    public function __construct(PurchaseOrderRepository $purchaseOrderRepo)
    {
        $this->purchaseOrderRepository = $purchaseOrderRepo;
    }

    /**
     * Display a listing of the PurchaseOrder.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('purchase_orders.index');
    }

    public function kode()
    {
        $reperence = PurchaseOrder::selectRaw('RIGHT(RTRIM(reference), 5) as kode')
                                ->orderBy('reference')
                                ->limit(1)
                                ->first();
        if($reperence){
            $kode = intval($reperence->kode) + 1;
        }else{
            $kode = 1;
        }
        return $kodemax = 'PO'.str_pad($kode, 5,"0", STR_PAD_LEFT);
    }

    /**
     * Show the form for creating a new PurchaseOrder.
     *
     * @return Response
     */
    public function create()
    {
        foreach (Supllier::get() as $key => $value) {
            $suppliers[$value->id] = $value->name;
        }
        $data['suppliers'] = $suppliers;
        $data['code'] = $this->kode();
        return view('purchase_orders.create', $data);
    }

    /**
     * Store a newly created PurchaseOrder in storage.
     *
     * @param CreatePurchaseOrderRequest $request
     *
     * @return Response
     */
    public function store(CreatePurchaseOrderRequest $request)
    {
        $input = $request->all();
        $harga = 0;
        if (!isset($input['selected_items'])) {
            return redirect()->back()->withErrors(array('message' => 'Please choose items'));
        }

        $purchaseOrder = $this->purchaseOrderRepository->create($input);

        for ($i=0; $i <count($input['selected_items']) ; $i++) { 
            $detailPO['purchase_order_id'] = $purchaseOrder->id;
            $detailPO['item_id'] = $input['selected_items'][$i];
            $detailPO['item_id_variant'] = $input['selected_variants'][$i];
            $detailPO['warehouse'] = $input['warehouse'][$i];
            $detailPO['qty'] = $input['qty'][$i];
            $harga += (($input['qty'][$i]*ItemVariant::where('id_variant', $input['selected_variants'][$i])->first()->price));
            PurchaseOrderDetail::create($detailPO);
        }
        $this->purchaseOrderRepository->update(['amount' => $harga], $purchaseOrder->id);
        Flash::success('Purchase Order saved successfully.');

        return redirect(route('purchaseOrders.index'));
    }


    public function approve($id='')
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('purchaseOrders.index'));
        }

        PurchaseOrder::where('id', $id)->update(['status' => 1]);

        Flash::success('Purchase Order updated successfully.');

        return redirect(route('purchaseOrders.index'));
    }

    /**
     * Display the specified PurchaseOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $purchaseOrder = $this->purchaseOrderRepository
        ->with('suppliernya')
        ->with('purchaseOrders.items')
        ->with('purchaseOrders.itemVariant')
        ->with('purchaseOrders.warehouse')
        ->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('purchaseOrders.index'));
        }
        
        view()->share('purchaseOrder', $purchaseOrder);
        $pdf = PDF::loadView('purchase_orders.po-print');
        return $pdf->stream('Purchase Order'.$purchaseOrder->reference.'.pdf');

        // return view('purchase_orders.show')->with('purchaseOrder', $purchaseOrder);
    }

    /**
     * Show the form for editing the specified PurchaseOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('purchaseOrders.index'));
        }

        foreach (Supllier::get() as $key => $value) {
            $suppliers[$value->id] = $value->name;
        }
        $cari = PurchaseOrderDetail::where('purchase_order_id', $id)->join('item_variant', 'purchase_order_details.item_id_variant', 'item_variant.id_variant')->select('item_variant.id', 'purchase_order_details.qty', 'purchase_order_details.warehouse');
        foreach ($cari->get() as $key => $value) {
            $item[] = $value->id;
            $qty[] = $value->qty;
            $warehouse[] = $value->warehouse;
        }
        $data['suppliers'] = $suppliers;
        $data['code'] = $purchaseOrder->reference;
        $data['items'] = $item;
        $data['qty'] = $qty;
        $data['purchaseOrder'] = $purchaseOrder;
        $data['warehouse'] = $warehouse;
        $data['id'] = $id;
        return view('purchase_orders.edit')->with($data);
    }

    /**
     * Update the specified PurchaseOrder in storage.
     *
     * @param  int              $id
     * @param UpdatePurchaseOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePurchaseOrderRequest $request)
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);
        $input = $request->all();
        $harga = 0;
        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('purchaseOrders.index'));
        }
       
        if (!isset($input['selected_items'])) {
            return redirect()->back()->withErrors(array('message' => 'Please choose items'));
        }
       
        $purchaseOrder = $this->purchaseOrderRepository->update($request->all(), $id);
        PurchaseOrderDetail::where('purchase_order_id',$input['purchase_order_id'])->delete();
        for ($i=0; $i <count($input['selected_items']) ; $i++) { 
            $detailPurchase['purchase_order_id'] = $input['purchase_order_id'];
            $detailPurchase['item_id'] = $input['selected_items'][$i];
            $detailPurchase['item_id_variant'] = $input['selected_variants'][$i];
            $detailPurchase['warehouse'] = $input['warehouse'][$i];
            $detailPurchase['qty'] = $input['qty'][$i];
            $harga += (($input['qty'][$i]*ItemVariant::where('id_variant',$input['selected_variants'][$i])->first()->price));
            PurchaseOrderDetail::create($detailPurchase);
        }
        $this->purchaseOrderRepository->update(['amount' => $harga], $purchaseOrder->id);
        Flash::success('Purchase Order updated successfully.');

        return redirect(route('purchaseOrders.index'));
    }

    /**
     * Remove the specified PurchaseOrder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('purchaseOrders.index'));
        }

        $this->purchaseOrderRepository->delete($id);
        PurchaseOrderDetail::wherePurchaseOrderId($id)->delete();

        Flash::success('Purchase Order deleted successfully.');

        return redirect(route('purchaseOrders.index'));
    }
}
