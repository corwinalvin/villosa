<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesOrder;
use App\Models\PurchaseOrder;
use App\Models\Item;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rp = SalesOrder::select(DB::Raw('YEAR(sales_orders.created_at) as year, QUARTER(sales_orders.created_at) as quarter , count(sales_orders.id) as idcount'))->groupBy(DB::raw('YEAR(sales_orders.created_at)',  'QUARTER(sales_orders.created_at)'))->orderByRaw('YEAR(created_at)','QUARTER(created_at)')->get();
        $data['report_graph'] = $rp;
        $rpPO = PurchaseOrder::select(DB::Raw('YEAR(purchase_order.created_at) as year, QUARTER(purchase_order.created_at) as quarter , count(purchase_order.id) as idcount'))->groupBy(DB::raw('YEAR(purchase_order.created_at)',  'QUARTER(purchase_order.created_at)'))->orderByRaw('YEAR(created_at)','QUARTER(created_at)')->get();
        $data['report_graphPO'] = $rpPO;
        $data['report_graphWH'] = Item::join('item_variant', 'items.id', 'item_variant.item_id')->join('warehouses', 'item_variant.warehouse','warehouses.id')->select(DB::raw('items.name, sum(item_variant.value) as qtyItem, warehouses.name as whName'))->groupBy('item_variant.item_id')->get();
        $data['cost_value'] = rupiah(SalesOrder::selectRaw('sum(amount) as cost_value')->first()->cost_value);
        $data['cost_po'] = rupiah(PurchaseOrder::selectRaw('sum(amount) as cost_value')->first()->cost_value);
        $data['total_order'] = SalesOrder::where('sales_orders.status', 1)->get()->count();
        $data['pending_order'] = SalesOrder::where('status', 0)->get()->count();
        $data['data_pending_order'] = SalesOrder::with('createdBy')->where('status', 0)->orWhere(function($query){
            $query->where('status', 1)->where('status_invoice', 0);
        })->take(8)->get();
        return view('home', $data);
    }
}
