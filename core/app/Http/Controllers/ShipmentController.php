<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateShipmentRequest;
use App\Http\Requests\UpdateShipmentRequest;
use App\Repositories\ShipmentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\SalesOrder;
use App\Models\SalesOrderDetail;
use App\Models\Shipment;
use App\Models\ShipmentDetail;
use PDF;
use App\DataTables\ShipmentNotShippedDataTable;
use App\DataTables\ShipmentShippedDataTable;

class ShipmentController extends AppBaseController
{
    /** @var  ShipmentRepository */
    private $shipmentRepository;

    public function __construct(ShipmentRepository $shipmentRepo)
    {
        $this->shipmentRepository = $shipmentRepo;
    }

    /**
     * Display a listing of the Shipment.
     *
     * @param Request $request
     * @return Response
     */
    public function index(ShipmentNotShippedDataTable $notShippedDataTable)
    {
        return $notShippedDataTable->render('shipments.index');
    }    

    public function getApproved(ShipmentShippedDataTable $dataTable)
    {
        return $dataTable->render('shipments.index');
    }

    /**
     * Show the form for creating a new Shipment.
     *
     * @return Response
     */
    public function create($id)
    {
        $cari = SalesOrderDetail::where('sales_id', $id)->join('item_variant', 'sales_order_details.item_id_variant', 'item_variant.id_variant')->select('item_variant.id_variant', 'sales_order_details.qty');
        foreach ($cari->get() as $key => $value) {
            $item[] = $value->id;
            $qty[] = $value->qty;
        }
        $data['items'] = $item;
        $data['qty'] = $qty;
        $data['id'] = $id;
        $data['kode'] = self::code();
        $data['shipment'] = (object) array('so_id' => $id);

        return view('shipments.create')->with($data);
    }

    public function code()
    {
         $reperence = Shipment::selectRaw('RIGHT(RTRIM(reference), 5) as kode')
                    ->orderBy('reference')
                    ->limit(1)
                    ->first();
        if($reperence){
            $kode = intval($reperence->kode) + 1;
        }else{
            $kode = 1;
        }
        return $kodemax = 'DO'.str_pad($kode, 5,"0", STR_PAD_LEFT);
    }

    /**
     * Store a newly created Shipment in storage.
     *
     * @param CreateShipmentRequest $request
     *
     * @return Response
     */
    public function store(CreateShipmentRequest $request)
    {
        $input = $request->all();

        if (!isset($input['selected_items'])) {
            return redirect()->back()->withErrors(array('message' => 'Please choose items'));
        }

        $shipment = $this->shipmentRepository->create($input);

        for ($i=0; $i <count($input['selected_items']) ; $i++) { 
            $detailShipment['shipment_id'] = $shipment->id;
            $detailShipment['item_id'] = $input['selected_items'][$i];
            $detailShipment['item_id_variant'] = $input['selected_variants'][$i];
            $detailShipment['qty'] = $input['qty'][$i];
            $detailShipment['warehouse'] = $input['warehouse'][$i];
            ShipmentDetail::create($detailShipment);
        }

        Flash::success('Shipment saved successfully.');

        return redirect(route('shipments.index'));
    }


    public function approve($value='')
    {
        Shipment::where('id', $value)->update(['status' => 1]);
        $so_id = Shipment::find($value)->so_id;
        if (SalesOrder::find($so_id)->status == 0) {
            Flash::error('Sales Order NOT APPROVED');

            return redirect(route('shipments.index'));
        }
        SalesOrder::whereId($so_id)->update(['status_invoice' => 1]);
        // $a = collect(ShipmentDetail::where('shipment_id', $value)->get())->toArray();
        // $b = SalesOrderDetail::where('sales_id', $so_id)->get();
        // $index =-1;
        // foreach ($b as $key => $value) {
        // $index++;
        //     if (isset($a[$index]['qty'])) {
        //         SalesOrderDetail::where('sales_id', $so_id)
        //             ->where('item_id_variant', $a[$index]['item_id_variant'])
        //             ->update(['qty' => $value->qty - $a[$index]['qty']]);
        //     }
        // }
        Flash::success('Shipment saved successfully.');

        return redirect(route('shipments.index'));
    }
    /**
     * Display the specified Shipment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $shipment = $this->shipmentRepository
            ->with('createdBy')
            ->with('salesOrder.buyer')
            ->with('salesOrder.detailOrders')
            ->with('salesOrder.detailAlamat')
            ->with('detailShipment.items')
            ->with('detailShipment.itemVariant')
            ->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }
        
        view()->share('shipment', $shipment);
        $pdf = PDF::loadView('shipments.shipment-print');
        return $pdf->stream('Shipment'.$shipment->reference.'.pdf');
        return view('shipments.shipment-print')->with('shipment', $shipment);
    }

    /**
     * Show the form for editing the specified Shipment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shipment = $this->shipmentRepository->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }

        $cari = ShipmentDetail::where('shipment_id', $id)->join('item_variant', 'shipment_detail.item_id_variant', 'item_variant.id_variant')->select('item_variant.id', 'shipment_detail.qty', 'shipment_detail.warehouse');
        foreach ($cari->get() as $key => $value) {
            $item[] = $value->id;
            $qty[] = $value->qty;
            $warehouse[] = $value->warehouse;
        }
        $data['items'] = $item;
        $data['kode'] = $shipment->reference;
        $data['qty'] = $qty;
        $data['warehouse'] = $warehouse;
        $data['id'] = $id;
        $data['shipment'] = $shipment;
        return view('shipments.edit')->with($data);
    }

    /**
     * Update the specified Shipment in storage.
     *
     * @param  int              $id
     * @param UpdateShipmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateShipmentRequest $request)
    {
        $input = $request->all();
        $shipment = $this->shipmentRepository->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }

        if (!isset($input['selected_items'])) {
            return redirect()->back()->withErrors(array('message' => 'Please choose item by clicking checkbox on the left of the table'));
        }
        
        ShipmentDetail::where('shipment_id', $id)->delete();
        $shipment = $this->shipmentRepository->update($request->all(), $id);
        for ($i=0; $i <count($input['selected_items']) ; $i++) { 
            $detailShipment['shipment_id'] = $id;
            $detailShipment['item_id'] = $input['selected_items'][$i];
            $detailShipment['item_id_variant'] = $input['selected_variants'][$i];
            $detailShipment['qty'] = $input['qty'][$i];
            $detailShipment['warehouse'] = $input['warehouse'][$i];
            ShipmentDetail::create($detailShipment);
        }
        Flash::success('Shipment updated successfully.');

        return redirect(route('shipments.index'));
    }

    /**
     * Remove the specified Shipment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shipment = $this->shipmentRepository->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }

        $this->shipmentRepository->delete($id);

        Flash::success('Shipment deleted successfully.');

        return redirect(route('shipments.index'));
    }
}
