<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSupllierRequest;
use App\Http\Requests\UpdateSupllierRequest;
use App\Repositories\SupllierRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SupllierController extends AppBaseController
{
    /** @var  SupllierRepository */
    private $supllierRepository;

    public function __construct(SupllierRepository $supllierRepo)
    {
        $this->supllierRepository = $supllierRepo;
    }

    /**
     * Display a listing of the Supllier.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->supllierRepository->pushCriteria(new RequestCriteria($request));
        $suplliers = $this->supllierRepository->all();

        return view('suplliers.index')
            ->with('suplliers', $suplliers);
    }

    /**
     * Show the form for creating a new Supllier.
     *
     * @return Response
     */
    public function create()
    {
        return view('suplliers.create');
    }

    /**
     * Store a newly created Supllier in storage.
     *
     * @param CreateSupllierRequest $request
     *
     * @return Response
     */
    public function store(CreateSupllierRequest $request)
    {
        $input = $request->all();

        $supllier = $this->supllierRepository->create($input);

        Flash::success('Supllier saved successfully.');

        return redirect(route('suplliers.index'));
    }

    /**
     * Display the specified Supllier.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $supllier = $this->supllierRepository->findWithoutFail($id);

        if (empty($supllier)) {
            Flash::error('Supllier not found');

            return redirect(route('suplliers.index'));
        }

        return view('suplliers.show')->with('supllier', $supllier);
    }

    /**
     * Show the form for editing the specified Supllier.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $supllier = $this->supllierRepository->findWithoutFail($id);

        if (empty($supllier)) {
            Flash::error('Supllier not found');

            return redirect(route('suplliers.index'));
        }

        return view('suplliers.edit')->with('supllier', $supllier);
    }

    /**
     * Update the specified Supllier in storage.
     *
     * @param  int              $id
     * @param UpdateSupllierRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSupllierRequest $request)
    {
        $supllier = $this->supllierRepository->findWithoutFail($id);

        if (empty($supllier)) {
            Flash::error('Supllier not found');

            return redirect(route('suplliers.index'));
        }

        $supllier = $this->supllierRepository->update($request->all(), $id);

        Flash::success('Supllier updated successfully.');

        return redirect(route('suplliers.index'));
    }

    /**
     * Remove the specified Supllier from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $supllier = $this->supllierRepository->findWithoutFail($id);

        if (empty($supllier)) {
            Flash::error('Supllier not found');

            return redirect(route('suplliers.index'));
        }

        $this->supllierRepository->delete($id);

        Flash::success('Supllier deleted successfully.');

        return redirect(route('suplliers.index'));
    }
}
