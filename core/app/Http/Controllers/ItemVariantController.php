<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateItemVariantRequest;
use App\Http\Requests\UpdateItemVariantRequest;
use App\Repositories\ItemVariantRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\ItemVariant;

class ItemVariantController extends AppBaseController
{
    /** @var  ItemVariantRepository */
    private $itemVariantRepository;

    public function __construct(ItemVariantRepository $itemVariantRepo)
    {
        $this->itemVariantRepository = $itemVariantRepo;
    }

    /**
     * Display a listing of the ItemVariant.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->itemVariantRepository->pushCriteria(new RequestCriteria($request));
        $itemVariants = $this->itemVariantRepository->all();

        return view('item_variants.index')
            ->with('itemVariants', $itemVariants);
    }

    /**
     * Show the form for creating a new ItemVariant.
     *
     * @return Response
     */
    public function create()
    {
        return view('item_variants.create');
    }

    /**
     * Store a newly created ItemVariant in storage.
     *
     * @param CreateItemVariantRequest $request
     *
     * @return Response
     */
    public function store(CreateItemVariantRequest $request)
    {
        $input = $request->all();

        $itemVariant = $this->itemVariantRepository->create($input);

        Flash::success('Item Variant saved successfully.');

        return redirect(route('itemVariants.index'));
    }

    /**
     * Display the specified ItemVariant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $itemVariant = \App\Models\ItemVariant::whereIdVariant($id)->first();

        if (empty($itemVariant)) {
            Flash::error('Item Variant not found');

            return redirect(route('itemVariants.index'));
        }

        return view('item_variants.show')->with('itemVariant', $itemVariant);
    }

    /**
     * Show the form for editing the specified ItemVariant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $itemVariant = \App\Models\ItemVariant::whereIdVariant($id)->first();

        if (empty($itemVariant)) {
            Flash::error('Item Variant not found');

            return redirect(route('itemVariants.index'));
        }
        ;
        return view('item_variants.edit')->with('itemVariant', $itemVariant)->with('stocks', ItemVariant::with('warehouses')->whereIdVariant($id)->get());
    }

    /**
     * Update the specified ItemVariant in storage.
     *
     * @param  int              $id
     * @param UpdateItemVariantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemVariantRequest $request)
    {
        $itemVariant = \App\Models\ItemVariant::whereIdVariant($id)->first();
        $input = $request->all();
        
        if (empty($itemVariant)) {
            Flash::error('Item Variant not found');

            return redirect(route('itemVariants.index'));
        }

        foreach ($input['picture'] as $key => $value) {
            $picture[] = $value;
        }
        
        foreach ($input['value'] as $key => $value) {
            \App\Models\ItemVariant::whereIdVariant($id)->whereWarehouse($key)->update(['value' => $value]);
        }
        unset($input['value']);
        $input['picture'] = json_encode($picture);
        $this->itemVariantRepository->update($input, $itemVariant->id);
        Flash::success('Item Variant updated successfully.');
        
        return redirect()->to('/private/items/'.$itemVariant->item_id.'/edit');
    }

    /**
     * Remove the specified ItemVariant from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $itemVariant = $this->itemVariantRepository->findWithoutFail($id);

        if (empty($itemVariant)) {
            Flash::error('Item Variant not found');

            return redirect(route('itemVariants.index'));
        }

        $this->itemVariantRepository->delete($id);

        Flash::success('Item Variant deleted successfully.');

        return redirect(route('itemVariants.index'));
    }
}
