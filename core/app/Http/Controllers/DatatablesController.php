<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;
use App\Models\Users;
use App\Models\Warehouse;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Sales;
use App\Models\Supllier;
use App\Models\Customer;
use App\Models\PurchaseOrder;
use App\Models\PaymentMethod;
use App\Models\ActivityLog;
use App\Models\WarehouseTransfer;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\SalesOrder;
use App\Models\SalesOrderDetail;
use App\Models\Shipment;
use App\Models\ShipmentDetail;
use App\Models\ReturnItem;
use App\Models\ReturnItemDetail;

class DatatablesController extends Controller
{
    public function getUsers()
    {
        $users = Users::with('createdBy')->select('id', 'name', 'email', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($users)
            ->addColumn('action', function($users){
            return '<form action="'.route('users.destroy', $users->id).'" method="POST"><a href="'.route('users.edit',$users->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }

    public function getWarehouses()
    {
        $warehouses = Warehouse::with('createdBy')->select('id', 'name', 'default', 'created_by', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($warehouses)
            ->addColumn('action', function($warehouses){
            return '<form action="'.route('warehouses.destroy', $warehouses->id).'" method="POST"><a href="'.route('warehouses.edit',$warehouses->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }

    public function getBrands()
    {
        $brand = Brand::with('createdBy')->select('id', 'brand_name', 'created_by', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($brand)
            ->addColumn('action', function($brand){
            return '<form action="'.route('brands.destroy', $brand->id).'" method="POST"><a href="'.route('brands.edit',$brand->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }

    public function getCategories()
    {
        $categories = Category::with('createdBy')->select('id', 'name', 'created_by', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($categories)
            ->addColumn('action', function($categories){
            return '<form action="'.route('categories.destroy', $categories->id).'" method="POST"><a href="'.route('categories.edit',$categories->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }

    public function getSales()
    {
        $sales = Sales::with('createdBy')->select('id', 'name', 'default', 'tax_included', 'created_by', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($sales)
            ->addColumn('action', function($sales){
            return '<form action="'.route('salesType.destroy', $sales->id).'" method="POST"><a href="'.route('salesType.edit',$sales->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }    

    public function getSuplliers()
    {
        $suplliers = Supllier::with('createdBy')->select('id', 'name', 'email', 'phone', 'status', 'created_by', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($suplliers)
            ->addColumn('action', function($suplliers){
            return '<form action="'.route('suplliers.destroy', $suplliers->id).'" method="POST"><a href="'.route('suplliers.edit',$suplliers->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }    

    public function getCustomers()
    {
        $customers = Customer::with('createdBy')->select('id', 'name', 'email', 'phone', 'status', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($customers)
            ->addColumn('action', function($customers){
            return '<form action="'.route('customers.destroy', $customers->id).'" method="POST"><a href="'.route('customers.edit',$customers->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }    

    public function getItems()
    {
        $items = Item::with('createdBy')->with('brands')->with('categories')->with('customVariant')->selectRaw('items.id, items.name, variant, brand, category, items.created_by')->orderBy('updated_at', 'desc');
        return Datatables::of($items)
            ->addColumn('action', function($items){
            return '<form action="'.route('items.destroy', $items->id).'" method="POST"><a href="'.route('items.edit',$items->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><a href="'.route('items.show',$items->id).'" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }    


    public function getPaymentMethods()
    {
        $paymentMethods = PaymentMethod::with('createdBy')->select('id','name', 'default', 'created_by', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($paymentMethods)
            ->addColumn('action', function($paymentMethods){
            return '<form action="'.route('paymentMethods.destroy', $paymentMethods->id).'" method="POST"><a href="'.route('paymentMethods.edit',$paymentMethods->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }

    public function counting($value, $from)
    {
        if ($from == "sal") {
            $b = ItemVariant::join('warehouses', 'item_variant.warehouse', 'warehouses.id')->select('item_variant.id', 'item_variant.value as stock', 'name', 'warehouse' ,'warehouses.default')->where('item_variant.id_variant', $value)->get();
            $a = '<select class="form-control wh" style="width:120px;" name="warehouse[]" disabled>';
            foreach ($b as $key => $value) {
                $a.= '<option value='.$value->warehouse.'>'.$value->name.' ('.$value->stock.')</option>';
            }
            $a.='</select>';   
            return $a;    
        }else if($from =="wt"){
            $b =ItemVariant::join('warehouses', 'item_variant.warehouse', 'warehouses.id')->select('item_variant.id', 'item_variant.value as stock', 'name', 'warehouse' ,'warehouses.default')->where('item_variant.id_variant', $value)->orderBy('item_variant.updated_at', 'desc')->get();
            $a = '<select required class="form-control wh hw1" style="width:120px;" name="warehouse_from[]" disabled>';
            $a.= '<option value="">-From-</option>';
            foreach ($b as $key => $value) {
                $a.= '<option value='.$value->warehouse.'>'.$value->name.' ('.$value->stock.')</option>';
            }
            $a.='</select>';   
            $c = '<select required class="form-control wh hw2" style="width:120px;" name="warehouse_to[]" disabled>';
            $c.= '<option value="">-To-</option>';
            foreach ($b as $key => $value) {
                $c.= '<option value='.$value->warehouse.'>'.$value->name.' ('.$value->stock.')</option>';
            }
            $c.='</select>';   
            return $a.$c;  
        }else if($from == "item"){
            $b = ItemVariant::leftJoin('warehouses', 'item_variant.warehouse', 'warehouses.id')->select('item_variant.id', 'item_variant.value as stock', 'name', 'warehouse' ,'warehouses.default')->where('item_variant.id_variant', $value)->orderBy('item_variant.updated_at', 'desc')->get();
            $a='<ul>';
            foreach ($b as $key => $value) {
                $a.= '<li>'.$value->name.' ('.$value->stock.')</li>';
            }   
            $a.='</ul>';
            return $a;
        }
    }

    public function getItemsSales()
    {
        $items = ItemVariant::join('items', 'item_id', 'items.id')->select('item_variant.id_variant','item_variant.picture','items.id as item_id','item_variant.id','items.name', 'item_variant.warna', 'item_variant.pattern', 'item_variant.size', 'item_variant.roll', 'item_variant.price')->groupBy('item_variant.id_variant');
        return Datatables::of($items)
            ->addColumn('action', function($items){
                return self::counting($items->id_variant, "sal").'<div id="input"><input name="qty[]" type="text" value="" class="form-control qty" disabled="disabled" placeholder="Qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/><input name="diskon_v[]" type="text" value="" class="form-control diskon" disabled="disabled" placeholder="Disc" price='.$items->price.' onkeypress="return event.charCode >= 48 && event.charCode <= 57"></div><br><a class="partial" style="cursor:pointer;">Partial</a>';
            })->make(true);
    }    

    public function getItemsPurchase()
    {
        $items = ItemVariant::join('items', 'item_id', 'items.id')->select('item_variant.id_variant','item_variant.picture','items.id as item_id','item_variant.id','items.name', 'item_variant.warna', 'item_variant.pattern', 'item_variant.size', 'item_variant.roll', 'item_variant.price')->groupBy('item_variant.id_variant');
        return Datatables::of($items)
            ->addColumn('action', function($items){
                return self::counting($items->id_variant, "sal").'<input name="qty[]" type="text" value="" class="form-control qty" disabled="disabled" placeholder="Qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>';
            })->make(true);
    }    

    public function getItemVariant($id, $jenis)
    {
        if ($jenis== "edit") {
            $items = ItemVariant::join('items', 'item_id', 'items.id')->select('item_variant.id_variant','item_variant.picture','items.id as item_id','item_variant.id','items.name', 'item_variant.warna', 'item_variant.pattern', 'item_variant.size', 'item_variant.roll', 'item_variant.price', 'item_variant.updated_at')->where('item_variant.item_id', $id)->groupBy('item_variant.id_variant');
            return Datatables::of($items)->addColumn('action', function ($items){
                return self::counting($items->id_variant, "item");
            })->editColumn('edit', function($items){
                return '<a href="'.route('itemVariants.edit',$items->id_variant).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a>';
            })->rawColumns(['action','edit'])->make(true);
        }elseif($jenis == "look"){
            $items = ItemVariant::join('items', 'item_id', 'items.id')->select('item_variant.id_variant','item_variant.picture','items.id as item_id','item_variant.id','items.name', 'item_variant.warna', 'item_variant.pattern', 'item_variant.size', 'item_variant.roll', 'item_variant.price', 'item_variant.updated_at')->where('item_variant.item_id', $id)->groupBy('item_variant.id_variant', 'item_variant.roll');
            return Datatables::of($items)->addColumn('action', function ($items){
                return self::counting($items->id_variant, "item");
            })->make(true);
        }
    }
    
    public function getItemsWT()
    {
        $items = ItemVariant::join('items', 'item_id', 'items.id')->select('item_variant.id_variant','item_variant.picture','items.id as item_id','item_variant.id','items.name', 'item_variant.warna', 'item_variant.pattern', 'item_variant.size', 'item_variant.roll', 'item_variant.price')->groupBy('item_variant.id_variant');
        return Datatables::of($items)
            ->addColumn('action', function($items){
                return self::counting($items->id_variant, "wt").'<input name="qty[]" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" value="" class="form-control qty" style="text-align:center;width:80px;" disabled="disabled" placeholder="Qty" required/><input type="hidden" name="selected_items[]" value="'.$items->item_id.'" disabled/><input disabled type="hidden" name="selected_variants[]" class="variant" value="'.$items->id.'"/>';
            })->make(true);
    }
    
    public function getActivityLog()
    {
        $activityLogs = ActivityLog::with('causer')->select('id','log_name', 'description', 'causer_id' ,'subject_id', 'subject_type' ,'properties', 'created_at', 'updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($activityLogs)
            ->addColumn('action', function($activityLogs){
            return '<a href="'.route('activityLogs.show',$activityLogs->id).'" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a>';
            })->make(true);
    }

    public function getWarehouseTransfer()
    {
        $wTs = WarehouseTransfer::with('createdBy')->with('item')->with('warehouseFrom')->with('warehouseTo')->with('variants')->selectRaw('warehouse_transfer.id,warehouse_transfer.warehouse_from, warehouse_transfer.warehouse_to,warehouse_transfer.item_variant, description, value, warehouse_transfer.created_at, warehouse_transfer.created_by, warehouse_transfer.item_id, upper(substr(warehouse_transfer.id, 1, 5)) as no_wto')->orderBy('warehouse_transfer.updated_at', 'desc')->get();
        return Datatables::of($wTs)
            ->addColumn('action', function($wTs){
            return '<a href="'.route('warehouseTransfers.show',$wTs->id).'" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a>';
            })->make(true);
    }

    public function getPurchaseOrders()
    {
        $purchaseOrders = PurchaseOrder::with('createdBy')->with('suppliernya')->select('purchase_order.id','date', 'supplier', 'reference', 'purchase_order.created_by','purchase_order.created_at', 'status', 'purchase_order.updated_at')->orderBy('updated_at', 'desc');
        return Datatables::of($purchaseOrders)
            ->addColumn('action', function($purchaseOrders){
                $a="";
                if ($purchaseOrders->status == 0) {
                    $a = '<a href="'.route('purchaseOrders.approve',$purchaseOrders->id).'" title="Approve?" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></a>';
                }
            return '<form action="'.route('purchaseOrders.destroy', $purchaseOrders->id).'" method="POST"><a href="'.route('purchaseOrders.edit',$purchaseOrders->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a><a href="'.route('purchaseOrders.show',$purchaseOrders->id).'" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a>'.$a.'<input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }

    public function getItemShipment($id)
    {
        $items = SalesOrderDetail::where('sales_id', $id)->join('item_variant', 'sales_order_details.item_id_variant', 'item_variant.id_variant')->join('items', 'item_variant.item_id', 'items.id')->join('warehouses', 'sales_order_details.warehouse', 'warehouses.id')->select('item_variant.id', 'items.id as item_id' ,'warna', 'pattern', 'size', 'roll', 'price', 'id_variant' ,'items.name' ,'sales_order_details.qty', 'warehouses.name as whName' ,'sales_order_details.warehouse', 'partial')->groupBy('item_variant.id_variant');
        return Datatables::of($items)
            ->addColumn('action', function($items){
                return '('.$items->whName.')'.$items->qty.'&nbsp; <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="qty[]" required disabled class="form-control qty"/>';
            })->make(true);
    }

    public function salesReport()
    {
        $sales = SalesOrder::join('sales_order_details', 'sales_orders.id', 'sales_order_details.sales_id')->select(DB::Raw('sales_orders.date, COUNT(DISTINCT sales_orders.id) as perDate, sum(sales_order_details.qty) as qtyPerDate, sum(DISTINCT sales_orders.amount) approxVal'))->groupBy('sales_orders.date')->whereNull('sales_order_details.deleted_at');
        return Datatables::of($sales)->make(true);
    }

    public function salesReportDetail($value='')
    {
        $sales = SalesOrder::with('buyer')->with('payment')->with('salesType')->join('sales_order_details', 'sales_orders.id', 'sales_order_details.sales_id')->join('item_variant', 'sales_order_details.item_id_variant', 'item_variant.id_variant')->select('sales_orders.reference', 'sales_orders.date', 'sales_orders.customer_id', 'sales_orders.payment_id', 'sales_orders.sales_type_id', 'sales_orders.id as soID', 'sales_orders.amount as amount')->where('sales_orders.date', $value)->groupBy('sales_orders.id');
        return Datatables::of($sales)->addColumn('action', function($items){
                return '<a href="'.route('salesOrders.show',$items->soID).'" >'.$items->reference.'</a>';
            })->make(true);
    }

    public function purchaseReport()
    {
        $sales = PurchaseOrder::join('purchase_order_details', 'purchase_order.id', 'purchase_order_details.purchase_order_id')->select(DB::Raw('purchase_order.date, purchase_order.updated_at, COUNT(DISTINCT purchase_order.id) as perDate, sum(purchase_order_details.qty) as qtyPerDate, sum(DISTINCT purchase_order.amount) approxVal'))->groupBy('purchase_order.date')->whereNull('purchase_order_details.deleted_at')->orderBy('updated_at', 'desc');
        return Datatables::of($sales)->make(true);
    }

    public function purchaseReportDetail($value='')
    {
        $sales = PurchaseOrder::with('suppliernya')->join('purchase_order_details', 'purchase_order.id', 'purchase_order_details.purchase_order_id')->join('item_variant', 'purchase_order_details.item_id_variant', 'item_variant.id_variant')->select('purchase_order.reference', 'purchase_order.date', 'purchase_order.supplier', 'purchase_order.id as soID', 'purchase_order.amount as amount', 'purchase_order.updated_at')->where('purchase_order.date', $value)->groupBy('purchase_order.id');
        return Datatables::of($sales)->addColumn('action', function($items){
                return '<a href="'.route('purchaseOrders.show',$items->soID).'" >'.$items->reference.'</a>';
            })->make(true);
    }    

    public function warehouseReport()
    {
        $sales = Item::join('item_variant', 'items.id', 'item_variant.item_id')->selectRaw('items.name as itemName, items.id as itemId, items.variant as variant, sum(item_variant.value) as value, items.updated_at')->groupBy('items.id')->orderBy('updated_at', 'desc');
        return Datatables::of($sales)->addColumn('action', function($items){
            return '<a href="'.route('items.show',$items->itemId).'" >'.$items->itemName.'</a>';
        })->make(true);
    }

    public function returnItems(){
        $returnItems = ReturnItem::with('createdBy:id,name')->with('sales:id,reference')->with('shipment:id,reference')->selectRaw('*, return_item.status as status_real, return_item.id as real_id')->orderBy('return_item.updated_at', 'desc');
        return Datatables::of($returnItems)
            ->addColumn('action', function($returnItems){
                $appr = $edit = "";
                if ($returnItems->status_real == 0) {
                    $edit = '<a href="'.route('returnItems.edit',$returnItems->real_id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a>';
                    $appr = '<a href="'.route('returnItems.approve',['id' => $returnItems->real_id]).'" title="Approve?" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></a>';
                }
            return '<form action="'.route('returnItems.destroy', $returnItems->real_id).'" method="POST">'.$edit.'<a href="'.route('returnItems.show',$returnItems->real_id).'" target="_blank" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a>'.$appr.'<input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }

    public function itemsReturn($id){
        $returnItems = SalesOrderDetail::where('sales_id', $id)->with('itemVariant')->with('stock')->with('items')->selectRaw('*, sales_order_details.id as real_id')->orderBy('sales_order_details.updated_at', 'desc');
        if (!$returnItems->first()) {
            $real = ShipmentDetail::where('shipment_id', $id)->with('itemVariant')->with('stock')->with('items')->selectRaw('*, shipment_detail.id as real_id')->orderBy('shipment_detail.updated_at', 'desc');
            return Datatables::of($real)->addColumn('action', function($real){
                return '<input type="hidden" name="item_id[]" class="item" value="'.$real->item_id.'" disabled>
                <input type="hidden" name="sales_order_id[]" class="so" value="'.$real->real_id.'" disabled>
                <input type="hidden" name="item_id_variant[]" class="variant" value="'.$real->item_id_variant.'" disabled>
                <input type="hidden" name="warehouse[]" class="wh" value="'.$real->warehouse.'" disabled><div id="input"><input name="qty[]" type="text" value="" class="form-control qty" disabled="disabled" placeholder="Qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/></div>';
                })->make(true);
        }else{
            return Datatables::of($returnItems)->addColumn('action', function($returnItems){
                return '<input type="hidden" name="item_id[]" class="item" value="'.$returnItems->item_id.'" disabled>
                <input type="hidden" name="sales_order_id[]" class="so" value="'.$returnItems->real_id.'" disabled>
                <input type="hidden" name="item_id_variant[]" class="variant" value="'.$returnItems->item_id_variant.'" disabled>
                <input type="hidden" name="warehouse[]" class="wh" value="'.$returnItems->warehouse.'" disabled><div id="input"><input name="qty[]" type="text" value="" class="form-control qty" disabled="disabled" placeholder="Qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/></div>';
                })->make(true);
            
        }
    }
}
