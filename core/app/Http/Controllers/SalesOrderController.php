<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSalesOrderRequest;
use App\Http\Requests\UpdateSalesOrderRequest;
use App\Repositories\SalesOrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Mail;
use App\Mail\SalesOrder as SO;
use App\Mail\ShipmentOrder;
use App\Mail\Invoice;
use App\Models\Customer;
use App\Models\CustomerDetail;
use App\Models\PaymentMethod;
use App\Models\Sales;
use App\Models\SalesOrder;
use App\Models\SalesOrderDetail;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\Shipment;
use App\Models\Tax;
use PDF;
use DB;

/**
 * Datatable
 */
use App\DataTables\SalesDataTable;
use App\DataTables\SalesUnapproveDataTable;

class SalesOrderController extends AppBaseController
{
    /** @var  SalesOrderRepository */
    private $salesOrderRepository;

    public function __construct(SalesOrderRepository $salesOrderRepo)
    {
        $this->salesOrderRepository = $salesOrderRepo;
    }

    /**
     * Display a listing of the SalesOrder.
     *
     * @param Request $request
     * @return Response
     */
    public function index(SalesUnapproveDataTable $salesUnapprovedDataTable)
    {
        return $salesUnapprovedDataTable->render('sales_orders.index');
    }    

    public function getApproved(SalesDataTable $dataTable)
    {
        return $dataTable->render('sales_orders.index');
    }

    public function kode($jenis)
    {
        $kodemax = "";
        if ($jenis == "ship") {
            $reperence = Shipment::selectRaw('RIGHT(RTRIM(reference), 5) as kode, SUBSTR(reference, 1, 2) as lain')
                    ->orderBy('lain')
                    ->where(DB::raw("SUBSTR(reference, 1, 2)"), 'DO')
                    ->limit(1)
                    ->first();
            if($reperence){
                $kode = intval($reperence->kode) + 1;
            }else{
                $kode = 1;
            }
            $kodemax = 'DO'.str_pad($kode, 5,"0", STR_PAD_LEFT);
        }else if ($jenis == "sales") {        
            $reperence = SalesOrder::selectRaw('RIGHT(RTRIM(reference), 5) as kode, SUBSTR(reference, 1, 2) as lain')
                    ->orderBy('lain')
                    ->where(DB::raw("SUBSTR(reference, 1, 2)"), 'SO')
                    ->limit(1)
                    ->first();
            if($reperence){
                $kode = intval($reperence->kode) + 1;
            }else{
                $kode = 1;
            }
            $kodemax = 'SO'.str_pad($kode, 5,"0", STR_PAD_LEFT);
        }

        return $kodemax;
    }

    /**
     * Show the form for creating a new SalesOrder.
     *
     * @return Response
     */
    public function create()
    {   
        $cus[""] = '-Pilih Customer-';
        foreach (Customer::select('id', 'name')->where('status', 0)->get() as $key => $value) {
            $cus[$value->id] = $value->name;
        }   
        foreach (PaymentMethod::select('id', 'name', 'default')->orderBy('default', 'desc')->get() as $key => $value) {
            $pay[$value->id] = $value->name;
        }
        foreach (Sales::select('id', 'name')->orderBy('default', 'desc')->get() as $key => $value) {
            $sal[$value->id] = $value->name;
        }
        $tax[null] = '-Choose Tax-';
        foreach (Tax::select('id', 'name')->orderBy('default_taxes', 'desc')->get() as $key => $value) {
            $tax[$value->id] = $value->name;
        }
       
        $data['customers'] = $cus;
        $data['payments'] = $pay;
        $data['sales'] = $sal;
        $data['tax'] = $tax;
        $data['code'] = self::kode("sales");
        return view('sales_orders.create', $data);
    }

    public function getAlamat($id, $type){
        if($type == "simpan"){
            $detail = "<option value=''>-Pilih Alamat-</option>";
            foreach(CustomerDetail::whereCustomerId($id)->get() as $alamat){
                $detail .= "<option value=".$alamat->id.">".$alamat->warehouse."</option>";
            }
            return $detail;
        }else{
            $detail = '';
            foreach(CustomerDetail::whereCustomerId($id)->get() as $alamat){
                if($alamat->id == $type){
                    $detail .= "<option value=".$alamat->id." selected=selected >".$alamat->warehouse."</option>";
                }else{
                    $detail .= "<option value=".$alamat->id." >".$alamat->warehouse."</option>";
                }
            }
            return $detail;
        }
    }

    /**
     * Store a newly created SalesOrder in storage.
     *
     * @param CreateSalesOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateSalesOrderRequest $request)
    {
        $input = $request->all();
        $amount = $diskon = $harga = $tax = $diskonInv = 0;

        if (!isset($input['selected_items'])) {
            return redirect()->back()->withErrors(array('message' => 'Please choose items'));
        }

        $salesOrder = $this->salesOrderRepository->create($input);

        for ($i=0; $i <count($input['selected_items']) ; $i++) { 
        
            if(isset($input['diskon_v'][$i])){
                if (strlen($input['diskon_v'][$i]) <= 3) {
                    $diskon = (($input['diskon_v'][$i]/100)*($input['qty'][$i]*$input['roll'][$i]*ItemVariant::where('id_variant', $input['selected_variants'][$i])->first()->price));
                }else{
                    $diskon = $input['diskon_v'][$i];
                }
                $harga += ((($input['qty'][$i]*$input['roll'][$i]*ItemVariant::where('id_variant', $input['selected_variants'][$i])->first()->price))-$diskon);
            }else{
                $harga += (($input['qty'][$i]*$input['roll'][$i]*ItemVariant::where('id_variant', $input['selected_variants'][$i])->first()->price));
            }
            

            if (isset($input['tax'])) {
                $rate = Tax::find($input['tax'])->rate;
                $tax = (($rate/100)*$harga);
            }

            if (isset($input['diskon'])) {
                if (strlen($input['diskon']) <= 3) {
                    $diskonInv = (($diskon/100)*$harga);
                }else{
                    $diskonInv = $harga-$diskon;
                }
            }

            $detailSales['sales_id'] = $salesOrder->id;
            $detailSales['item_id'] = $input['selected_items'][$i];
            $detailSales['item_id_variant'] = $input['selected_variants'][$i];
            $detailSales['warehouse'] = $input['warehouse'][$i];
            $detailSales['diskon'] = $input['diskon_v'][$i];
            $detailSales['partial'] = (isset($input['partial'][$i]) ? $input['partial'][$i] : null);
            $detailSales['qty'] = $input['qty'][$i];
            SalesOrderDetail::create($detailSales);
        }
        $amount =  $harga+$tax-$diskonInv;
        $this->salesOrderRepository->update(['amount' => $amount], $salesOrder->id);
        $status = (Customer::find($input['customer_id'])->amount+$amount > Customer::find($input['customer_id'])->limit_trans) ? 0 : 1;
        Customer::whereId($input['customer_id'])->update(['amount' => Customer::find($input['customer_id'])->amount + $amount, 'status' => $status]);
        
        Flash::success('Sales Order saved successfully.');

        return redirect(route('salesOrders.index'));
    }

    public function approve($id='', $jenis='')
    {
        $salesOrder = $this->salesOrderRepository->findWithoutFail($id);

        if (empty($salesOrder)) {
            Flash::error('Sales Order not found');
            return redirect(route('salesOrders.index'));
        }

        if ($jenis == 'inv') {
            $this->salesOrderRepository->update(['status_invoice' => 2], $id);
            $status = (Customer::find($salesOrder->customer_id)->amount+$salesOrder->amount > Customer::find($salesOrder->customer_id)->limit_trans) ? 0 : 1;
            Customer::whereId($salesOrder->customer_id)->update(['amount' => Customer::find($salesOrder->customer_id)->amount + $salesOrder->amount, 'status' => $status]);
            $message = 'Invoice <b>paid</b> successfully';
            $route = 'invoices.index';
            Mail::to(Customer::whereId($salesOrder->customer_id)->first()->email)->send(new Invoice($salesOrder));
        }elseif($jenis=="send"){
            $ship = new Shipment;
            $ship->so_id = $id;
            $ship->reference = self::kode("ship");
            $ship->save();
            $message = 'Shipment created successfully';
            $salesOrder = $this->salesOrderRepository->update(['status_invoice' => 1], $id);
            $route = 'invoices.index';
            Mail::to(Customer::whereId($salesOrder->customer_id)->first()->email)->send(new ShipmentOrder($salesOrder));
        }else{
            foreach (SalesOrderDetail::whereSalesId($id)->select('item_id_variant', 'item_id', 'warehouse', 'qty', 'partial')->get() as $key => $value) {
                ItemVariant::whereIdVariant($value->item_id_variant)->where('warehouse', $value->warehouse)
                ->update(['value' => ItemVariant::whereIdVariant($value->item_id_variant)->where('warehouse', $value->warehouse)->first()->value - $value->qty]);
                if($value->partial !== null){
                    $sv = ItemVariant::whereIdVariant($value->item_id_variant)->where('warehouse', $value->warehouse)->first();
                    $iv = new ItemVariant;
                    $iv->id_variant = $value->item_id_variant;
                    $iv->item_id = $value->item_id;
                    $iv->warehouse = $value->warehouse;
                    $iv->value = 1;
                    $iv->warna = $sv->warna;
                    $iv->pattern = $sv->pattern;
                    $iv->size = $sv->size;
                    $iv->roll = $sv->roll-$value->partial;
                    $iv->price = $sv->price;
                    $iv->picture = $sv->picture;
                    $iv->save();
                }
            }
            $this->salesOrderRepository->update(['status' => 1, 'reference' => str_replace('SO','INV',$salesOrder->reference)], $id);
            $so = $this->salesOrderRepository
                ->with('buyer')
                ->with('detailOrders.items')
                ->with('detailOrders.itemVariant')
                ->find($id);
            view()->share('salesOrder', $so);
	        $pdf = PDF::loadView('sales_orders.invoice.invoice-print');
            // Mail::to(Customer::whereId($salesOrder->customer_id)->first()->email)->send(new SO($so, $pdf));
            $message = 'Sales Order <b>approved</b> successfully</a>';
            $route = 'salesOrders.index';

        }

        Flash::success($message);

        return redirect(route($route));
    }

    /**
     * Display the specified SalesOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $salesOrder = $this->salesOrderRepository
            ->with('buyer')
            ->with('taxR')
            ->with('detailAlamat')
            ->with('detailOrders.items')
            ->with('detailOrders.itemVariant')
            ->findWithoutFail($id);
        
        if (empty($salesOrder)) {
            Flash::error('Sales Order not found');

            return redirect(route('salesOrders.index'));
        }

        return view('sales_orders.show')->with('salesOrder', $salesOrder);
    }

    // Menunjukkan Invoice Detail untuk di print out
    public function printInvoice($id='')
    {
        $salesOrder = $this->salesOrderRepository
            ->with('buyer')
            ->with('detailOrders.items')
            ->with('detailOrders.itemVariant')
            ->findWithoutFail($id);
        
        if (empty($salesOrder)) {
            Flash::error('Sales Order not found');

            return redirect(route('salesOrders.index'));
        }
        view()->share('salesOrder', $salesOrder);
        $pdf = PDF::loadView('sales_orders.invoice.invoice-print');
        return $pdf->stream('Invoice'.$salesOrder->reference.'.pdf');
        // return view('sales_orders.invoice.invoice-print')->with('salesOrder', $salesOrder);
    }

    /**
     * Show the form for editing the specified SalesOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $salesOrder = $this->salesOrderRepository->findWithoutFail($id);

        if (empty($salesOrder)) {
            Flash::error('Sales Order not found');

            return redirect(route('salesOrders.index'));
        }

        foreach (Customer::select('id', 'name')->get() as $key => $value) {
            $cus[$value->id] = $value->name;
        }   
        foreach (PaymentMethod::select('id', 'name', 'default')->orderBy('default', 'desc')->get() as $key => $value) {
            $pay[$value->id] = $value->name;
        }
        foreach (Sales::select('id', 'name')->orderBy('default', 'desc')->get() as $key => $value) {
            $sal[$value->id] = $value->name;
        }
        $tax[null] = '-Choose Tax-';
        foreach (Tax::select('id', 'name')->orderBy('default_taxes', 'desc')->get() as $key => $value) {
            $tax[$value->id] = $value->name;
        }

        $cari = SalesOrderDetail::where('sales_id', $id)->join('item_variant', 'sales_order_details.item_id_variant', 'item_variant.id_variant')->select('item_variant.id_variant', 'sales_order_details.diskon', 'sales_order_details.qty', 'sales_order_details.warehouse', 'sales_order_details.partial')->groupBy('item_variant.id_variant');

        foreach ($cari->get() as $key => $value) {
            $item[] = $value->id_variant;
            $qty[] = $value->qty;
            $diskon[] = $value->diskon;
            $warehouse[] = $value->warehouse;
            $partial[] = $value->partial;
        }

        $data['code'] = $salesOrder->reference;
        $data['salesOrder'] = $salesOrder;
        $data['warehouse'] = $warehouse;
        $data['diskon'] = $diskon;
        $data['partial'] = $partial;
        $data['customers'] = $cus;
        $data['payments'] = $pay;
        $data['sales'] = $sal;
        $data['tax'] = $tax;
        $data['items'] = $item;
        $data['qty'] = $qty;
        $data['id'] = $id;
        return view('sales_orders.edit')->with($data);
    }

    /**
     * Update the specified SalesOrder in storage.
     *
     * @param  int              $id
     * @param UpdateSalesOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSalesOrderRequest $request)
    {
        $input = $request->all();
        $amount = $diskon = $harga= $tax = $diskonInv = 0;
        $salesOrder = $this->salesOrderRepository->findWithoutFail($id);

        if (empty($salesOrder)) {
            Flash::error('Sales Order not found');

            return redirect(route('salesOrders.index'));
        }

        if (!isset($input['selected_items'])) {
            return redirect()->back()->withErrors(array('message' => 'Please choose items'));
        }
        
        $salesOrder = $this->salesOrderRepository->update($input, $id);
        SalesOrderDetail::where('sales_id',$input['sales_id'])->delete();
        for ($i=0; $i <count($input['selected_items']) ; $i++) { 
            if(isset($input['diskon_v'][$i])){
                if (strlen($input['diskon_v'][$i]) <= 3) {
                    $diskon = (($input['diskon_v'][$i]/100)*($input['qty'][$i]*ItemVariant::where('id_variant', $input['selected_variants'][$i])->first()->price));
                }else{
                    $diskon = $input['diskon_v'][$i];
                }
                $harga += ((($input['qty'][$i]*ItemVariant::where('id_variant', $input['selected_variants'][$i])->first()->price))-$diskon);
            }else{
                $harga += (($input['qty'][$i]*ItemVariant::where('id_variant', $input['selected_variants'][$i])->first()->price));
            }
            

            if (isset($input['tax'])) {
                $rate = Tax::find($input['tax'])->rate;
                $tax = (($rate/100)*$harga);
            }

            if (isset($input['diskon'])) {
                if (strlen($input['diskon']) <= 3) {
                    $diskonInv = (($diskon/100)*$harga);
                }else{
                    $diskonInv = $harga-$diskon;
                }
            }

            $detailSales['sales_id'] = $input['sales_id'];
            $detailSales['item_id'] = $input['selected_items'][$i];
            $detailSales['item_id_variant'] = $input['selected_variants'][$i];
            $detailSales['warehouse'] = $input['warehouse'][$i];
            $detailSales['qty'] = $input['qty'][$i];
            $detailSales['partial'] = (isset($input['partial'][$i]) ? $input['partial'][$i] : null);
            $detailSales['diskon'] = $input['diskon_v'][$i];
            SalesOrderDetail::create($detailSales);
        }
        $amount =  $harga+$tax-$diskonInv;
        $status = (Customer::find($input['customer_id'])->amount+$amount > Customer::find($input['customer_id'])->limit_trans) ? 0 : 1;
        Customer::whereId($input['customer_id'])->update(['amount' => Customer::find($input['customer_id'])->amount + $amount, 'status' => $status]);
        $salesOrder = $this->salesOrderRepository->update(['amount' => $amount], $id);

        Flash::success('Sales Order updated successfully.');

        return redirect(route('salesOrders.index'));
    }


    /**
     * Remove the specified SalesOrder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $salesOrder = $this->salesOrderRepository->findWithoutFail($id);

        if (empty($salesOrder)) {
            Flash::error('Sales Order not found');

            return redirect(route('salesOrders.index'));
        }

        SalesOrderDetail::whereSalesId($id)->delete();
        Shipment::whereSoId($id)->delete();

        $this->salesOrderRepository->delete($id);

        Flash::success('Sales Order deleted successfully.');

        return redirect(route('salesOrders.index'));
    }
}
