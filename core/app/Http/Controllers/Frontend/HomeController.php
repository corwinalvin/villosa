<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\Users;
use App\Models\Customer;
use App\Models\CustomerDetail;
use App\Models\SalesOrderDetail;
use App\Models\SalesOrder;
use App\Models\Category;
use DB;
use Cart;

class HomeController extends Controller
{
    public function index(){
        return view('welcome');
    }

    public function customers(){
        return Customer::get();
    }

    public function getAlamat($id){
        return CustomerDetail::where('customer_id', $id)->select('id', 'warehouse')->get();
    }

    public function saveCustomer(Request $request){
        $request->validate([
            'name' => 'required|min:3',
            'alamat' => 'required|min:10',
            'phone' => 'required|numeric|digits_between:11,13',
            'email' => 'required|email',
        ]);

        $cus = new Customer;
        $cus->name = $request->name;
        $cus->phone = $request->phone;
        $cus->email = $request->email;
        $cus->save();

        $cusD = new CustomerDetail;
        $cusD->customer_id = $cus->id;
        $cusD->warehouse = $request->alamat; 
        $cusD->created_by = Users::where('remember_token', '<>', '')->first()->id;
        $cusD->updated_by = Users::where('remember_token', '<>', '')->first()->id;
        $cusD->save();

        return response()->json(['id' => $cus->id], 200);
    }

    public function products(){
        return response(['products' => Item::with('brands:id,brand_name')->with('variants:item_id,picture')->with('categories:id,name')->with('createdBy:id,name')->get()
        , 'categories' => Category::select('id', 'slug','name')->get()]);
    }

    public function category($slug){
        if($slug == 'all'){
            return json_decode(self::products()->getContent())->products;
        }else{
            return response(Item::with('brands:id,brand_name')->where('category', Category::whereSlug($slug)->first()->id)->with('variants:item_id,picture')->with('categories:id,name')->with('createdBy:id,name')->get());
        }
    }

    public function productShow($id){
        $barang = Item::whereSlug($id)->first();

        if($id == "cart" || empty($barang)){
            return response()->json(['message' => 'Not Found'], 404);
        }
       
        return response(['hasil' => Item::whereSlug($id)->with(['variants' => function($query){
            $query->groupBy('id_variant');
        }])->with('brands:id,brand_name')->with('categories:id,name')->with('createdBy:id,name')->first(), 
        'total' => ItemVariant::filter(['item_id' => $barang->id])->select(DB::Raw('sum(value) as total'))->first()->total ]);
    }

    public function productSearch(Request $request){
        return Item::where('name', 'like', '%'.$request->search.'%')->with('brands:id,brand_name')->with('variants:item_id,warna,pattern,roll,picture')->with('categories:id,name')->get();
    }

    public function searchVariant(Request $request){
                
        if($request->warna != "" || $request->pattern != "" || $request->roll != ""){
            $cek = ItemVariant::filter($request->all());
            if($cek->get()->count() > 2){
                $hasil = ItemVariant::filter($request->all())->select(DB::Raw('item_id, id_variant,warna,pattern,roll,price,picture,warehouse'))->get();
                $total = ItemVariant::filter($request->all())->select(DB::Raw('sum(value) as total'))->first();
            }elseif($cek->get()->count() == 2 && $cek->get()[0]->warehouse != $cek->get()[1]->warehouse){
                if($cek->get()[0]->roll != $cek->get()[1]->roll || $cek->get()[0]->warna != $cek->get()[1]->warna || $cek->get()[0]->pattern != $cek->get()[1]->pattern){
                    $hasil = ItemVariant::filter($request->all())->select(DB::Raw('item_id, id_variant,warna,pattern,roll,price,picture,warehouse'))->get();
                    $total = ItemVariant::filter($request->all())->select(DB::Raw('sum(value) as total'))->first();
                }else{
                    $cek = $cek->select(DB::Raw('max(id) as real_id'))->first();
                    $hasil = ItemVariant::where('id', $cek->real_id)->select(DB::Raw('item_id, id_variant,warna,pattern,roll,price,picture,warehouse'))->get();
                    $total = ItemVariant::where('id', $cek->real_id)->with('warehouses:id,name')->select(DB::Raw('warehouse,sum(value) as total'))->first();
                }
            }elseif($cek->get()->count() == 1){
                $cek = $cek->select(DB::Raw('max(id) as real_id'))->first();
                $hasil = ItemVariant::where('id', $cek->real_id)->select(DB::Raw('item_id, id_variant,warna,pattern,roll,price,picture,warehouse'))->get();
                $total = ItemVariant::where('id', $cek->real_id)->with('warehouses:id,name')->select(DB::Raw('warehouse, sum(value) as total'))->first();
            }
        }else{
            $hasil = ItemVariant::filter($request->all())->select(DB::Raw('item_id, id_variant,warna,pattern,roll,price,picture,warehouse'))->get();
            $total = ItemVariant::filter($request->all())->select(DB::Raw('sum(value) as total'))->first();
        }
        return response(['total' => $total->total, 'hasil' =>$hasil, 'warehouse' => $total]);
    }
    
    public function postCart(Request $request){
        Cart::add(array(
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => (int) $request->qty,
            'attributes' => array(
                'warna' => $request->warna,
                'roll' => $request->roll,
                'pattern' => $request->pattern,
                'gambar' => $request->gambar,
                'brand' => $request->brand,
                'category' => $request->category,
                'id_variant' => $request->id_variant,
                'warehouse' => $request->warehouse
            )
        ));
        
    }

    public function getCart(){
        return response()->json(['content' => Cart::getContent(), 'total' => Cart::getTotal()]);
    }

    public function getTotalQty(){
        return Cart::getTotalQuantity();
    }

    public function cartRemove($id){
        Cart::remove($id);
    }

    private function kode(){
        $reperence = SalesOrder::selectRaw('RIGHT(RTRIM(reference), 5) as kode')
                ->orderBy('reference')
                ->limit(1)
                ->first();
        if($reperence){
            $kode = intval($reperence->kode) + 1;
        }else{
            $kode = 1;
        }
        return $kodemax = 'SO'.str_pad($kode, 5,"0", STR_PAD_LEFT);
    }
    public function order(Request $request){
        $request->validate([
            'alamat' => 'required',
            'customer' => 'required'
        ]);

        $so = new SalesOrder;
        $so->customer_id = $request->customer;
        $so->alamat_id = $request->alamat;
        $so->date = date('Y-m-d');
        $so->reference = $this->kode();
        $so->amount = Cart::getTotal();
        $so->save();

        foreach (Cart::getContent() as $key => $value) {
            $detailSales['sales_id'] = $so->id;
            $detailSales['item_id'] = $value->id;
            $detailSales['item_id_variant'] = $value->attributes->id_variant;
            $detailSales['warehouse'] = $value->attributes->warehouse;
            $detailSales['qty'] = $value->quantity;
            SalesOrderDetail::create($detailSales);    
        }
        Cart::clear();
        return response()->json(['message' => 'ok']);
    }
}