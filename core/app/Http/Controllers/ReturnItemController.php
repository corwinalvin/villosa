<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReturnItemRequest;
use App\Http\Requests\UpdateReturnItemRequest;
use App\Repositories\ReturnItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\SalesOrder;
use App\Models\Shipment;
use App\Models\ReturnItemDetail;
use App\Models\ReturnItem;
use App\Models\ItemVariant;
use Session;
use PDF;

class ReturnItemController extends AppBaseController
{
    /** @var  ReturnItemRepository */
    private $returnItemRepository;

    public function __construct(ReturnItemRepository $returnItemRepo)
    {
        $this->returnItemRepository = $returnItemRepo;
    }

    /**
     * Display a listing of the ReturnItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->returnItemRepository->pushCriteria(new RequestCriteria($request));
        $returnItems = $this->returnItemRepository->all();

        return view('return_items.index')
            ->with('returnItems', $returnItems);
    }

    private function kode()
    {
        $reperence = ReturnItem::selectRaw('RIGHT(RTRIM(reference), 5) as kode, SUBSTR(reference, 1, 2) as lain')
                ->orderBy('lain')
                ->where(DB::raw("SUBSTR(reference, 1, 2)"), 'DO')
                ->limit(1)
                ->first();
        if($reperence){
            $kode = intval($reperence->kode) + 1;
        }else{
            $kode = 1;
        }
        $kodemax = 'DO'.str_pad($kode, 5,"0", STR_PAD_LEFT);
        return $kodemax;
    }

    /**
     * Show the form for creating a new ReturnItem.
     *
     * @return Response
     */
    public function create()
    {
        $a = $b = [];
        foreach(ReturnItem::select('sales_id', 'shipment_id')->get() as $id){
            $a[]=$id->sales_id;
            $b[]=$id->shipment_id;
        }
        $data['sales'] = (isset($a[0]) !== null) ? SalesOrder::whereNotIn('id', $a)->get() : SalesOrder::get();
        $data['shipments'] = (isset($b[0]) !== null) ? Shipment::whereNotIn('id', $b)->get() : Shipment::get();
        
        return view('return_items.create', $data);
    }

    public function apiItems(Request $request){
        if($request->type == "sales"){
            $data['items'] = SalesOrder::where('id', $request->id)->with('detailOrders.items:name,id')->with('detailOrders.itemVariant')->with('detailOrders.stock:name,id')->get();
        }else if($request->type == "shipment"){
            $data['items'] = Shipment::where('id', $request->id)->with('detailShipment.items:name,id')->with('detailShipment.itemVariant')->with('detailShipment.stock:name,id')->get();
        }
        Session::put('items', $data['items']);
        return redirect('private/returnItems/create');
    }

    /**
     * Store a newly created ReturnItem in storage.
     *
     * @param CreateReturnItemRequest $request
     *
     * @return Response
     */
    public function store(CreateReturnItemRequest $request)
    {
        
        $input = $request->all();
        
        $returnItem = $this->returnItemRepository->create($input);
        for ($i=0; $i <count($input['item_id']) ; $i++) { 
            $returnItems['return_item_id'] = $returnItem->id;
            $returnItems['item_id'] = $input['item_id'][$i];
            $returnItems['sales_order_id'] = $input['sales_order_id'][$i];
            $returnItems['item_id_variant'] = $input['item_id_variant'][$i];
            $returnItems['warehouse'] = $input['warehouse'][$i];
            $returnItems['partial'] = (isset($input['partial'][$i]) != '') ? $input['partial'][$i] : null;
            $returnItems['qty'] = $input['qty'][$i];
            ReturnItemDetail::create($returnItems);
        }
        
        Flash::success('Return Item saved successfully.');
        
        Session::remove('items');
        return redirect(route('returnItems.index'));
    }

    public function approve($id){
        ReturnItem::where('id', $id)->update(['status' => 1]);
        foreach(ReturnItemDetail::where('return_item_id', $id)->get() as $item){
            ItemVariant::where('id_variant', $item->item_id_variant)->where('warehouse', $item->warehouse)->update(['value'=>ItemVariant::where('id_variant', $item->item_id_variant)->where('warehouse', $item->warehouse)->first()->value + $item->qty]);
            if($item->partial !== null){
                $sv = ItemVariant::whereIdVariant($item->item_id_variant)->where('warehouse', $item->warehouse)->first();
                $iv = new ItemVariant;
                $iv->id_variant = $item->item_id_variant;
                $iv->item_id = $item->item_id;
                $iv->warehouse = $item->warehouse;
                $iv->value = 1;
                $iv->warna = $sv->warna;
                $iv->pattern = $sv->pattern;
                $iv->size = $sv->size;
                $iv->roll = $item->partial;
                $iv->price = $sv->price;
                $iv->picture = $sv->picture;
                $iv->save();
            }
        }

        Flash::success('Return Item approved successfully.');

        return redirect(route('returnItems.index'));
    }

    /**
     * Display the specified ReturnItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $returnItemShipment = $this->returnItemRepository->with('returnItems.itemVariant:id_variant,warna,pattern,size,roll')->with('returnItems.stock:id,name')->with('returnItems.items:name,id')->with('shipment.detailShipment')->with('shipment.salesOrder')->findWithoutFail($id);
        $returnItemSales = $this->returnItemRepository->with('sales')->with('returnItems.itemVariant:id_variant,warna,pattern,size,roll')->findWithoutFail($id);
        
        
        if (!empty($returnItemShipment)) {
            view()->share('returnItem',$returnItemShipment);
            view()->share('returnItem2',$returnItemSales);

            $pdf = PDF::loadView('return_items.show_print_shipment');
            return $pdf->stream('Invoice'.$returnItemShipment->reference.'.pdf');
        }elseif(empty($returnItemShipment->shipment)){
            view()->share('returnItem', $returnItemSales);
            $pdf = PDF::loadView('return_items.show_print_sales');
            return $pdf->stream('Invoice'.$returnItem->reference.'.pdf');

        }else{
            Flash::error('Return Item not found');
            
            return redirect(route('returnItems.index'));
        }
    }

    /**
     * Show the form for editing the specified ReturnItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $returnItem = $this->returnItemRepository->findWithoutFail($id);

        if (empty($returnItem)) {
            Flash::error('Return Item not found');

            return redirect(route('returnItems.index'));
        }
        Session::remove('items');
        $cari = ReturnItemDetail::where('return_item_id', $id)->join('item_variant', 'return_item_details.item_id_variant', 'item_variant.id_variant')->select('return_item_details.sales_order_id','item_variant.id_variant', 'return_item_details.qty', 'return_item_details.warehouse', 'return_item_details.partial')->groupBy('item_variant.id_variant');
        
        foreach ($cari->get() as $key => $value) {
            $item[] = $value->id_variant;
            $qty[] = $value->qty;
            $warehouse[] = $value->warehouse;
            $sales_order[] = $value->sales_order_id;
            $partial [] = $value->partial;
        }
        
        $data['sales'] = SalesOrder::get();
        $data['shipments'] =Shipment::get();
        $data['returnItem'] = $returnItem;
        $data['items'] = $item;
        $data['qty'] = $qty;
        $data['partial'] = $partial;
        $data['warehouse'] = $warehouse;
        $data['sales_order'] = $sales_order;
        $data['id'] = $id;
        return view('return_items.edit', $data);
    }

    /**
     * Update the specified ReturnItem in storage.
     *
     * @param  int              $id
     * @param UpdateReturnItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReturnItemRequest $request)
    {
        $returnItem = $this->returnItemRepository->findWithoutFail($id);
        $input = $request->all();
   
        if (empty($returnItem)) {
            Flash::error('Return Item not found');

            return redirect(route('returnItems.index'));
        }
        $returnItem = $this->returnItemRepository->update($input, $id);
        ReturnItemDetail::where('return_item_id', $input['return_id'])->delete();
        for ($i=0; $i <count($input['item_id']) ; $i++) { 
            $returnItems['return_item_id'] = $input['return_id'];
            $returnItems['item_id'] = $input['item_id'][$i];
            $returnItems['sales_order_id'] = $input['sales_order_id'][$i];
            $returnItems['item_id_variant'] = $input['item_id_variant'][$i];
            $returnItems['warehouse'] = $input['warehouse'][$i];
            $returnItems['qty'] = $input['qty'][$i];
            ReturnItemDetail::create($returnItems);
        }
        
        Flash::success('Return Item updated successfully.');

        return redirect(route('returnItems.index'));
    }

    /**
     * Remove the specified ReturnItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $returnItem = $this->returnItemRepository->findWithoutFail($id);

        if (empty($returnItem)) {
            Flash::error('Return Item not found');

            return redirect(route('returnItems.index'));
        }

        $this->returnItemRepository->delete($id);
        ReturnItemDetail::where('return_item_id', $id)->delete();
        Flash::success('Return Item deleted successfully.');

        return redirect(route('returnItems.index'));
    }
}
