<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWarehouseTransferRequest;
use App\Http\Requests\UpdateWarehouseTransferRequest;
use App\Repositories\WarehouseTransferRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\ItemVariant;

class WarehouseTransferController extends AppBaseController
{
    /** @var  WarehouseTransferRepository */
    private $warehouseTransferRepository;

    public function __construct(WarehouseTransferRepository $warehouseTransferRepo)
    {
        $this->warehouseTransferRepository = $warehouseTransferRepo;
    }

    /**
     * Display a listing of the WarehouseTransfer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('warehouse_transfers.index');
    }

    /**
     * Show the form for creating a new WarehouseTransfer.
     *
     * @return Response
     */
    public function create()
    {
        return view('warehouse_transfers.create');
    }

    /**
     * Store a newly created WarehouseTransfer in storage.
     *
     * @param CreateWarehouseTransferRequest $request
     *
     * @return Response
     */
    public function store(CreateWarehouseTransferRequest $request)
    {
        $input = $request->all();

        if (!isset($input['selected_items'])) {
            return redirect()->back()->withErrors(array('message' => 'Please choose items'));
        }

        for ($i=0; $i <count($input['selected_items']) ; $i++) { 
            $wt['warehouse_from'] = $input['warehouse_from'][$i];
            $wt['warehouse_to'] = $input['warehouse_to'][$i];
            $wt['item_id'] = $input['selected_items'][$i];
            $wt['item_variant'] = $input['selected_variants'][$i];
            $wt['value'] = $input['qty'][$i];
            $wt['description'] = $input['description'];
            $warehouseTransfer = $this->warehouseTransferRepository->create($wt);
            ItemVariant::whereIdVariant($input['selected_variants'][$i])->where('warehouse', $input['warehouse_to'][$i])
                ->update(['value' => ItemVariant::whereIdVariant($input['selected_variants'][$i])->where('warehouse', $input['warehouse_to'][$i])->first()->value + $input['qty'][$i]]);
            ItemVariant::whereIdVariant($input['selected_variants'][$i])->where('warehouse', $input['warehouse_from'][$i])
                ->update(['value' => ItemVariant::whereIdVariant($input['selected_variants'][$i])->where('warehouse', $input['warehouse_from'][$i])->first()->value - $input['qty'][$i]]);
        }

        Flash::success('Warehouse Transfer saved successfully.');

        return redirect(route('warehouseTransfers.index'));
    }

    /**
     * Display the specified WarehouseTransfer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $warehouseTransfer = $this->warehouseTransferRepository->findWithoutFail($id);

        if (empty($warehouseTransfer)) {
            Flash::error('Warehouse Transfer not found');

            return redirect(route('warehouseTransfers.index'));
        }

        return view('warehouse_transfers.show')->with('warehouseTransfer', $warehouseTransfer);
    }

    /**
     * Show the form for editing the specified WarehouseTransfer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $warehouseTransfer = $this->warehouseTransferRepository->findWithoutFail($id);

        if (empty($warehouseTransfer)) {
            Flash::error('Warehouse Transfer not found');

            return redirect(route('warehouseTransfers.index'));
        }

        return view('warehouse_transfers.edit')->with('warehouseTransfer', $warehouseTransfer);
    }

    /**
     * Update the specified WarehouseTransfer in storage.
     *
     * @param  int              $id
     * @param UpdateWarehouseTransferRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWarehouseTransferRequest $request)
    {
        $warehouseTransfer = $this->warehouseTransferRepository->findWithoutFail($id);

        if (empty($warehouseTransfer)) {
            Flash::error('Warehouse Transfer not found');

            return redirect(route('warehouseTransfers.index'));
        }

        $warehouseTransfer = $this->warehouseTransferRepository->update($request->all(), $id);

        Flash::success('Warehouse Transfer updated successfully.');

        return redirect(route('warehouseTransfers.index'));
    }

    /**
     * Remove the specified WarehouseTransfer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $warehouseTransfer = $this->warehouseTransferRepository->findWithoutFail($id);

        if (empty($warehouseTransfer)) {
            Flash::error('Warehouse Transfer not found');

            return redirect(route('warehouseTransfers.index'));
        }

        $this->warehouseTransferRepository->delete($id);

        Flash::success('Warehouse Transfer deleted successfully.');

        return redirect(route('warehouseTransfers.index'));
    }
}
