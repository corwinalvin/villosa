<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Repositories\ItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Excel;
use Auth;
use Uuid;
use DB;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\SalesOrder;

class ItemController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->itemRepository->pushCriteria(new RequestCriteria($request));
        $items = $this->itemRepository->all();

        return view('items.index')
            ->with('items', $items);
    }

    /**
     *   Import Excel
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:csv,txt'
        ]);
        
        $data = Excel::load($request->file->getRealPath(), function($reader) {
            if (!empty($reader->get()) && $reader->get()->count()) {
                foreach ($reader->get() as $key => $value) {
                    $cek = $this->get_num_chars('/',$value->varian);
                    $var = self::validVariant($cek);
                    if ($value->harga != "") {
                        if (!empty($value->nama_barang) && $value->nama_barang != " ") {
                            $item = new Item;
                            $item->name = $value->nama_barang;
                            $item->brand = $this->findBrand($value->merek);
                            $item->category = $this->findCategory($value->tipe);
                            $item->desc = $value->deskripsi;
                            $item->variant = $var;
                            $item->department = $this->findDepartment($value->department);
                            $item->created_by = Auth::User()->id;
                            $item->save();
                            activity()->performedOn($item)->withProperties(['user' => Auth::user()->name])->log('Upload Item Excel');
                        }
                        $warehouse = $stok = $size = $warna = $pattern = $roll = null;
                        for ($i=0; $i < count(explode("|",$value->stok)); $i++) { 
                            
                            if (isset(explode("|",$value->warehouse)[$i])) {
                                $warehouse = self::findWarehouse(explode("|",$value->warehouse)[$i]);
                            }
                            if (isset(explode("|",$value->stok)[$i])) {
                                $stok = explode("|",$value->stok)[$i];
                            }
                            if (isset(explode("/", $value->varian)[0])) {
                                $size = explode("/", $value->varian)[0];
                            }
                            if (isset(explode("/", $value->varian)[1])) {
                                $warna = explode("/", $value->varian)[1];
                            }
                            if (isset(explode("/", $value->varian)[2])) {
                                $pattern = explode("/", $value->varian)[2];
                            }
                            if (isset(explode("/", $value->varian)[3])) {
                                $roll = explode("/", $value->varian)[3];
                            }
                            
                            $itemV = new ItemVariant;
                            $itemV->item_id = $item->id;
                            $itemV->id_variant = substr($size, 0,1).substr($warna,0, 1).substr($pattern,0, 1).$roll.substr($item->id,0, 3);
                            $itemV->warehouse = $warehouse;
                            $itemV->value = $stok;
                            $itemV->size = $size;
                            $itemV->warna = $warna;
                            $itemV->pattern = $pattern;
                            $itemV->roll = $roll;
                            $itemV->price = $value->harga;
                            $itemV->picture = json_encode([$value->url_gambar_1,$value->url_gambar_2,$value->url_gambar_3, $value->url_gambar_4,$value->url_gambar_5]);
                            $itemV->save();
                        }
                    }
                    
                }
            }
        });
        
        Flash::success('Item saved successfully.');
        
        return redirect(route('items.index'));
    }
    
    public function get_num_chars($char, $string) {
        return substr_count($string, $char);
    }

    public function validVariant($value='')
    {
        $string = array();
        if ($value == 1) {
            array_push($string, "c", "p");
        }elseif($value == 2){
            array_push($string, "c", "p", "s");
        }elseif ($value == 3) {
            array_push($string, "c", "p", "s", "r");
        }
        return json_encode(array_unique($string));
    }

    public function findBrand($value='')
    {
        return (\App\Models\Brand::where('brand_name', 'like', '%'.$value.'%')->first() ? \App\Models\Brand::where('brand_name', 'like', '%'.$value.'%')->first()->id  : 0);
    }

    public function findDepartment($value='')
    {
        return (\App\Models\Department::where('name', 'like', '%'.$value.'%')->first() ? \App\Models\Department::where('name', 'like', '%'.$value.'%')->first()->id  : 0);
    }

    public function findItemId($value='')
    {
        return (\App\Models\Item::where('sku', $value)->first() ? \App\Models\Item::where('sku', $value)->first()->id  : 1);
    }

    public function findCategory($value='')
    {
        return (\App\Models\Category::where('name', 'like', '%'.$value.'%')->first() ? \App\Models\Category::where('name', 'like', '%'.$value.'%')->first()->id  : 0);
    }

    public function findWarehouse($value='')
    {
        return (\App\Models\Warehouse::where('name', 'like', '%'.$value.'%')->first() ? \App\Models\Warehouse::where('name', 'like', '%'.$value.'%')->first()->id  : 0);
    }

    /**
     * Show the form for creating a new Item.
     *
     * @return Response
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Display the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }
        $data['item'] = $item;
        $data['orders'] = SalesOrder::with('buyer:id,name')->join('sales_order_details', 'sales_order_details.sales_id', 'sales_orders.id')->join('item_variant', 'item_variant.id_variant', 'sales_order_details.item_id_variant')->select('sales_orders.reference', 'sales_orders.date', 'sales_order_details.qty' , 'sales_orders.id as id', 'sales_orders.customer_id', 'item_variant.price', 'item_variant.warna', 'item_variant.pattern', 'item_variant.size', 'item_variant.roll', 'partial')->where('sales_order_details.item_id', $id)->groupBy('sales_order_details.item_id_variant')->get();
        $data['sa'] = DB::select('select * from activity_log where description=? and subject_type = ?', ['updated', 'App\Models\ItemVariant']);
        return view('items.show', $data);
    }

    /**
     * Show the form for editing the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        return view('items.edit')->with('item', $item);
    }

    /**
     * Update the specified Item in storage.
     *
     * @param  int              $id
     * @param UpdateItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemRequest $request)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        $item = $this->itemRepository->update($request->all(), $id);

        Flash::success('Item updated successfully.');

        return redirect(route('items.index'));
    }

    /**
     * Remove the specified Item from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }
        ItemVariant::where('item_id', $id)->delete();
        $this->itemRepository->delete($id);

        Flash::success('Item deleted successfully.');

        return redirect(route('items.index'));
    }

    public function getSku($id){
        $variants = ItemVariant::whereItemId($id)->get();
        $item = Item::select('name')->find($id);
        return view('reports.tj101', compact('variants', 'item'));
    }
}
