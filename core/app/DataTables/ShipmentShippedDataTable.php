<?php

namespace App\DataTables;

use App\Models\Shipment;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class ShipmentShippedDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return (new EloquentDataTable($query));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Shipment::with('createdBy')->with('salesOrder')->select('shipment.id', 'so_id', 'shipment.reference as sr', 'shipment.created_by','shipment.status')->where('shipment.status', 1)->orderBy('shipment.updated_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'sales_order.reference' => ['title' => 'Sales Order Ref.', 'name' => 'salesOrder.reference'],
            'sr' => ['title' => 'Reference'],
            'status' => ['title' => 'Status','render' => '(data == 1) ? "<div style=\'border:1px solid green !important;color: green !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;\';>SHIPPED</div>" : "<div style=\'border:1px solid red !important;color: red !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;\';>UNSHIPPED</div>"'],
            'created_by.name' => ['title' => 'Created By', 'name' => 'createdBy.name']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ShipmentShipped_' . date('YmdHis');
    }

    public function ajax(){
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action', function($query){
                return '<form action="'.route('shipments.destroy', $query->id).'" method="POST"><a target="_blank" href="'.route('shipments.show',$query->id).'" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a><input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }
}
