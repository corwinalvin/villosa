<?php

namespace App\DataTables;

use App\Models\SalesOrder;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class SalesUnapproveDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return (new EloquentDataTable($query));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return SalesOrder::with('createdBy')->with('buyer')->select('sales_orders.id', 'date','customer_id','reference', 'sales_orders.status' ,'sales_orders.created_by')->where('sales_orders.status', 0)->orderBy('sales_orders.updated_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '200px'])
                    ->parameters(['dom'=> 'Blfrtip','buttons' => ['pdf', 'excel']]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'buyer.name' => ['title' => 'Customer'],
            'date' => ['render' => '(new Date(data)).getDate() + "/" + (((new Date(data)).getMonth()+1).length > 1 ? ((new Date(data)).getMonth()+1) : "0" + ((new Date(data)).getMonth()+1)) + "/" + (new Date(data)).getFullYear()'],
            'reference',
            'status' => ['title' => 'Status','render' => '(data == 1) ? "<div style=\'border:1px solid green !important;color: green !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;\';>APPROVED</div>" : "<div style=\'border:1px solid red !important;color: red !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;\';>UNAPPROVED</div>"'],
            'created_by.name' => ['title' => 'Created By', 'name' => 'createdBy.name'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Sales Unapproved_' . date('YmdHis');
    }

    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action', function($query){
            $appr = $appr2 = "";
            if ($query->status == 0) {
                $appr = '<a href="'.route('salesOrders.approve',['id' => $query->id, 'jenis' => 'sal']).'" title="Approve!" class="btn btn-success"><i class="fa fa-check"></i></a>';
            }
            return '<form action="'.route('salesOrders.destroy', $query->id).'" method="POST"><a href="'.route('salesOrders.show',$query->id).'" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a><a href="'.route('salesOrders.edit',$query->id).'" class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></a>'.$appr.'<input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })->make(true);
    }
}
