<?php

namespace App\DataTables;

use App\Models\SalesOrder;
use App\Models\Shipment;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class SalesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return (new EloquentDataTable($query));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return SalesOrder::with('createdBy')->with('buyer')->select('sales_orders.id', 'date','customer_id','reference', 'sales_orders.status' ,'sales_orders.created_by')->where('sales_orders.status', 1)->orderBy('sales_orders.updated_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters(['dom'=> 'Blfrtip','buttons' => ['pdfHtml5', 'excelHtml5']]);
    }
    public function coba(){
        
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'buyer.name' => ['title' => 'Customer'],
            'date' => ['render' => '(new Date(data)).getDate() + "/" + (((new Date(data)).getMonth()+1).length > 1 ? ((new Date(data)).getMonth()+1) : "0" + ((new Date(data)).getMonth()+1)) + "/" + (new Date(data)).getFullYear()'],
            'reference',
            'status' => ['title' => 'Status','render' => '(data == 1) ? "<div style=\'border:1px solid green !important;color: green !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;\';>APPROVED</div>" : "<div style=\'border:1px solid green !important;color: green !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;\';>SHIPPED</div>"'],
            'created_by.name' => ['title' => 'Created By', 'name' => 'createdBy.name'],
            'action' => ['data' => 'action', 'name' => 'action','exportable' => false] 
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Sales_' . date('YmdHis');
    }

    public function ajax()
    {
        
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action', function($query){
            $appr2 = "";
            if ($query->status_invoice < 2 && !Shipment::where('so_id', $query->id)->first()) {
                $appr2 = '<a href="'.route('shipments.buat',$query->id).'" title="Send!" class="btn btn-warning"><i class="fa fa-truck"></i></a>';
            }
            return '<form action="'.route('salesOrders.destroy', $query->id).'" method="POST"><a href="'.route('salesOrders.show',$query->id).'" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a>'.$appr2.'<input type="hidden" name="_token" value="'.csrf_token().'"/><input type="hidden" name="_method" value="DELETE"/><button class="btn btn-danger" onclick="return confirm(&#39;Are you sure?&#39;)"><i class="glyphicon glyphicon-trash"></i></button></form>';
            })
        ->make(true);
    }
}
