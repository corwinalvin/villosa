<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Wildside\Userstamps\Userstamps;
use App\Traits\Uuids;
/**
 * Class WarehouseTransfer
 * @package App\Models
 * @version February 25, 2018, 1:49 pm UTC
 *
 * @property string id
 * @property string warehouse_from
 * @property string warehouse_to
 * @property integer item_variant
 * @property integer value
 * @property string created_by
 * @property string updated_by
 * @property string deleted_by
 */
class WarehouseTransfer extends Model
{
    use SoftDeletes;
    use Uuids;
    use Userstamps;
    use LogsActivity;

    public $table = 'warehouse_transfer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    
    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'warehouse_from',
        'warehouse_to',
        'item_variant',
        'item_id',
        'value',
        'description',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'warehouse_from' => 'string',
        'warehouse_to' => 'string',
        'item_id' => 'string',
        'item_variant' => 'string',
        'value' => 'integer',
        'description' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'warehouse_to' => 'required|array',        
        'warehouse_to*' => 'required',        
        'warehouse_from' => 'required|array',        
        'warehouse_from*' => 'required',        
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }

    public function warehouseFrom()
    {
        return $this->belongsTo('\App\Models\Warehouse','warehouse_from');
    }

    public function warehouseTo()
    {
        return $this->belongsTo('\App\Models\Warehouse', 'warehouse_to');
    }

    public function item()
    {
        return $this->belongsTo('\App\Models\Item', 'item_id','id');
    }

    public function variants()
    {
        return $this->belongsTo('\App\Models\ItemVariant', 'item_variant', 'id_variant');
    }
}
