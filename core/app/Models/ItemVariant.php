<?php

namespace App\Models;

use Eloquent as Model;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use EloquentFilter\Filterable;
/**
 * Class ItemVariant
 * @package App\Models
 * @version February 20, 2018, 9:14 am UTC
 *
 * @property integer item_id
 * @property string warna
 * @property string pattern
 * @property string size
 * @property string roll
 * @property string price
 */
class ItemVariant extends Model
{
    use Userstamps;
    use LogsActivity,Filterable;
    
    public $table = 'item_variant';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    
    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'item_id',
        'id_variant',
        'warehouse',
        'value',
        'warna',
        'pattern',
        'picture',
        'size',
        'roll',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'string',
        'id_variant' => 'string',
        'warehouse' => 'string',
        'value' => 'integer',
        'warna' => 'string',
        'price' => 'string',
        'pattern' => 'string',
        'picture' => 'string',
        'size' => 'string',
        'roll' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];    

    public function warehouses()
    {
        return $this->hasMany('\App\Models\Warehouse', 'id', 'warehouse');
    }

    // public function item()
    // {
    //     return $this->belongsTo('\App\Models\Item', 'item_id', 'id');
    // }

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
