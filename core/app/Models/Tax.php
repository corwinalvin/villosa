<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;

/**
 * Class Tax
 * @package App\Models
 * @version March 2, 2018, 7:29 am UTC
 *
 * @property string name
 * @property integer rate
 * @property boolean default_taxes
 * @property string created_by
 * @property string deleted_by
 * @property string updated_by
 */
class Tax extends Model
{
    use SoftDeletes;
    use Userstamps;
    use LogsActivity;
    use Uuids;

    public $table = 'taxes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'rate',
        'default_taxes',
        'created_by',
        'deleted_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'rate' => 'integer',
        'default_taxes' => 'boolean',
        'created_by' => 'string',
        'deleted_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }    
}
