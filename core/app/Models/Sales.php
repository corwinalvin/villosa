<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
/**
 * Class Sales
 * @package App\Models
 * @version February 17, 2018, 2:33 pm UTC
 *
 * @property string name
 * @property integer tax_included
 * @property integer default_sales_types
 * @property integer created_by
 */
class Sales extends Model
{
    use SoftDeletes;
    use Userstamps;
    use LogsActivity;
    use Uuids;


    public $table = 'sales_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'tax_included',
        'default',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'tax_included' => 'integer',
        'default' => 'integer',
        'created_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }    
}
