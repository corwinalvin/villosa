<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
use EloquentFilter\Filterable;
/**
 * Class SalesOrder
 * @package App\Models
 * @version February 21, 2018, 3:36 am UTC
 *
 * @property integer customer_id
 * @property integer sales_id
 * @property integer payment_id
 * @property date date
 * @property integer sales_type_id
 * @property string reference
 * @property integer status
 * @property string note
 * @property integer created_by
 */
class SalesOrder extends Model
{
    use SoftDeletes;
    use Userstamps;
    use LogsActivity, Filterable;
    use Uuids;

    public $table = 'sales_orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';
    
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    
    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_id',
        'payment_id',
        'alamat_id',
        'date',
        'sales_type_id',
        'tax',
        'diskon',
        'amount',
        'reference',
        'status',
        'status_invoice',
        'note',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'customer_id' => 'string',
        'alamat_id' => 'string',
        'payment_id' => 'string',
        'date' => 'date',
        'sales_type_id' => 'string',
        'tax' => 'string',
        'diskon' => 'integer',
        'amount' => 'integer',
        'reference' => 'string',
        'status' => 'integer',
        'status_invoice' => 'integer',
        'note' => 'string',
        'created_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function buyer()
    {
        return $this->belongsTo('\App\Models\Customer', 'customer_id', 'id');
    }

    public function detailOrders()
    {
        return $this->hasMany('\App\Models\SalesOrderDetail', 'sales_id', 'id');
    }    

    public function payment()
    {
        return $this->belongsTo('\App\Models\PaymentMethod', 'payment_id');
    }

    public function taxR()
    {
        return $this->belongsTo('\App\Models\Tax', 'tax');
    }

    public function salesType()
    {
        return $this->belongsTo('\App\Models\Sales', 'sales_type_id', 'id');
    }
    
    public function detailAlamat(){
        return $this->belongsTo('\App\Models\CustomerDetail', 'alamat_id', 'id');
    }
    
    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
