<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
use EloquentFilter\Filterable;

/**
 * Class PurchaseOrder
 * @package App\Models
 * @version February 28, 2018, 4:42 am UTC
 *
 * @property string supplier
 * @property date date
 * @property string reference
 * @property boolean status
 * @property string note
 * @property string created_by
 * @property string updated_by
 * @property string deleted_by
 */
class PurchaseOrder extends Model
{
    use SoftDeletes;
    use Uuids;
    use Userstamps;
    use LogsActivity, Filterable;

    public $table = 'purchase_order';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public $fillable = [
        'supplier',
        'date',
        'reference',
        'status',
        'amount',
        'note',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'supplier' => 'string',
        'date' => 'date',
        'reference' => 'string',
        'status' => 'boolean',
        'note' => 'string',
        'amount' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by');
    }

    public function suppliernya()
    {
        return $this->belongsTo('\App\Models\Supllier', 'supplier');
    }

    public function purchaseOrders()
    {
        return $this->hasMany('\App\Models\PurchaseOrderDetail', 'purchase_order_id', 'id');
    }   
}
