<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;

/**
 * Class ReturnItem
 * @package App\Models
 * @version March 11, 2018, 2:18 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property string sales_id
 * @property string shipment_id
 * @property string keterangan
 * @property string created_by
 * @property string updated_by
 * @property string deleted_by
 */
class ReturnItem extends Model
{
    use SoftDeletes, Userstamps, LogsActivity, Uuids;

    public $table = 'return_item';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public $fillable = [
        'sales_id',
        'shipment_id',
        'keterangan',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'sales_id' => 'string',
        'shipment_id' => 'string',
        'keterangan' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function createdBy(){
        return $this->belongsTo('\App\Models\Users', 'created_by');
    }

    public function sales(){
        return $this->belongsTo('\App\Models\SalesOrder', 'sales_id');
    }

    public function shipment(){
        return $this->belongsTo('\App\Models\Shipment', 'shipment_id');
    }

    public function returnItems(){
        return $this->hasMany('\App\Models\ReturnItemDetail','return_item_id');
    }
}
