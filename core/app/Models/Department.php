<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;

/**
 * Class Department
 * @package App\Models
 * @version April 2, 2018, 3:32 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property string name
 * @property string created_by
 * @property string updated_by
 * @property string deleted_by
 */
class Department extends Model
{
    use Userstamps, LogsActivity, SoftDeletes, Uuids;

    public $table = 'departments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function createdBy(){
        return $this->belongsTo('\App\Models\Users', 'created_by');
    }
}
