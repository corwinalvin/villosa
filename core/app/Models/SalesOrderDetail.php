<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;
class SalesOrderDetail extends Model
{
    use SoftDeletes;
    use Uuids;
    
    public $table = 'sales_order_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'sales_id',
        'item_id',
        'diskon',
        'item_id_variant',
        'warehouse',
        'qty',
        'partial'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'sales_id' => 'string',
        'item_id' => 'string',
        'diskon' => 'integer',
        'item_id_variant' => 'string',
        'warehouse' => 'string',
        'qty' => 'integer',
        'partial' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function salesOrder()
    {
        $this->belongsTo('\App\Models\SalesOrder', 'sales_id');
    }

    public function items()
    {
        return $this->belongsTo('\App\Models\Item', 'item_id','id');
    }

    public function itemVariant()
    {
        return $this->belongsTo('\App\Models\ItemVariant', 'item_id_variant', 'id_variant')->orderBy('created_at', 'desc');
    }

    public function stock()
    {
        return $this->belongsTo('\App\Models\Warehouse', 'warehouse', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
