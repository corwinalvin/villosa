<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;

class CustomerDetail extends Model
{
    use Uuids, Userstamps, LogsActivity;
    
    protected $table = "customers_detail";

    public $fillable = [
        'customer_id',
        'warehouse',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'customer_id' => 'string',
        'warehouse' => 'string',
    ];
}
