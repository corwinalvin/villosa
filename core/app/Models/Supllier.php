<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
/**
 * Class Supllier
 * @package App\Models
 * @version February 17, 2018, 2:34 pm UTC
 *
 * @property string name
 * @property string email
 * @property string phone
 * @property string street
 * @property string city
 * @property string state
 * @property integer zipcode
 * @property string status
 * @property integer created_by
 */
class Supllier extends Model
{
    use SoftDeletes;
    use Userstamps;
    use LogsActivity;
    use Uuids;

    public $table = 'suppliers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'phone',
        'street',
        'city',
        'state',
        'zipcode',
        'status',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'street' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zipcode' => 'integer',
        'status' => 'string',
        'created_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required|numeric',
        'street' => 'required',
        'state' => 'required',
        'zipcode' => 'required|numeric',
        'status' => 'required',
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
