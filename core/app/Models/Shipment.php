<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;

/**
 * Class Shipment
 * @package App\Models
 * @version February 28, 2018, 6:40 am UTC
 *
 * @property string so_id
 * @property string inv_id
 * @property date date
 * @property integer status
 * @property string reference
 * @property string note
 * @property integer created_by
 */
class Shipment extends Model
{
    use SoftDeletes;
    use Uuids;
    use Userstamps;
    use LogsActivity;

    public $table = 'shipment';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    public $fillable = [
        'so_id',
        'status',
        'reference',
        'date',
        'note',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'so_id' => 'string',
        'status' => 'integer',
        'date' => 'date',
        'reference' => 'string',
        'note' => 'string',
        'created_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function salesOrder()
    {
        return $this->belongsTo('\App\Models\SalesOrder', 'so_id', 'id');
    }
    
    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }

    public function detailShipment()
    {
        return $this->hasMany('\App\Models\ShipmentDetail', 'shipment_id', 'id');
    }       
}
