<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class PurchaseOrderDetail extends Model
{

    use SoftDeletes;
    use Uuids;
    
	protected $table = "purchase_order_details";

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'purchase_order_id',
        'item_id',
        'item_id_variant',
        'warehouse',
        'qty'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'purchase_order_id' => 'string',
        'item_id' => 'string',
        'item_id_variant' => 'string',
        'warehouse' => 'string',
        'qty' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function purchaseOrder()
    {
        $this->belongsTo('\App\Models\PurchaseOrder', 'purchase_order_id');
    }

    public function items()
    {
        return $this->belongsTo('\App\Models\Item', 'item_id','id');
    }

    public function itemVariant()
    {
        return $this->belongsTo('\App\Models\ItemVariant', 'item_id_variant', 'id_variant');
    }

    public function warehouse()
    {
        return $this->belongsTo('\App\Models\Warehouse', 'warehouse', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
