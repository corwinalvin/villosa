<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
/**
 * Class Category
 * @package App\Models
 * @version February 17, 2018, 2:30 pm UTC
 *
 * @property integer parent_id
 * @property string name
 * @property string slug
 * @property string picture
 * @property string description
 * @property integer created_by
 */
class Category extends Model
{
    use SoftDeletes;
    use Sluggable;
    use Userstamps;
    use LogsActivity;
    use Uuids;
    
    public $table = 'categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public $fillable = [
        'id',
        'name',
        'slug',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'created_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
