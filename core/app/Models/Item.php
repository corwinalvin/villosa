<?php

namespace App\Models;

use Eloquent as Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
use EloquentFilter\Filterable;
/**
 * Class Item
 * @package App\Models
 * @version February 20, 2018, 12:55 am UTC
 *
 * @property string sku
 * @property string name
 * @property integer category
 * @property integer brand
 * @property string slug
 * @property string desc
 * @property integer tax
 * @property string status
 * @property string picture
 * @property integer price
 * @property string variant
 * @property integer created_by
 */
class Item extends Model
{
    use Sluggable;
    use Userstamps;
    use LogsActivity;
    use Uuids, Filterable;
    
    public $table = 'items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public $fillable = [
        'name',
        'category',
        'brand',
        'slug',
        'desc',
        'tax',
        'status',
        'picture',
        'price',
        'variant',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'category' => 'string',
        'brand' => 'string',
        'slug' => 'string',
        'desc' => 'string',
        'tax' => 'integer',
        'status' => 'string',
        'picture' => 'string',
        'price' => 'integer',
        'variant' => 'string',
        'created_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function brands()
    {
        return $this->belongsTo('\App\Models\Brand', 'brand', 'id');
    }   

    public function categories()
    {
        return $this->belongsTo('\App\Models\Category', 'category', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }

    public function variants()
    {
        return $this->hasMany('\App\Models\ItemVariant', 'item_id', 'id');
    }

    public function customVariant()
    {
        return $this->hasMany('\App\Models\ItemVariant', 'item_id', 'id')->selectRaw('SUM(item_variant.value) as total, max(price) as maxPrice, min(price) as minPrice, item_variant.item_id')->groupBy('item_id');
    }

    // public function cheapestPrice()
    // {
    //     return $this->hasMany('\App\Models\ItemVariant', 'item_id', 'id')->max('price')->first();
    // }

    // public function highestPrice()
    // {
    //     return $this->hasMany('\App\Models\ItemVariant', 'item_id', 'id')->max('price')->first();
    // }

    public function departments(){
        return $this->belongsTo('\App\Models\Deparment', 'department', 'id');
    }
}
