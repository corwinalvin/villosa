<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
/**
 * Class Customer
 * @package App\Models
 * @version February 18, 2018, 2:35 am UTC
 *
 * @property string name
 * @property string email
 * @property string phone
 * @property string street
 * @property string city
 * @property string state
 * @property string zipcode
 * @property string country
 * @property int status
 * @property int created_by
 * @property timestamp created_at
 * @property timestamp updated_at
 * @property timestamp deleted_at
 */
class Customer extends Model
{
    use SoftDeletes;
    use Userstamps;
    use LogsActivity;
    use Uuids;
    
    public $table = 'customers';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    
    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'created_by','updated_at', 'deleted_at'
    ];


    public $fillable = [
        'name',
        'email',
        'phone',
        'street',
        'office1',
        'office2',
        'warehouse1',
        'warehouse2',
        'warehouse3',
        'status',
        'limit_trans',
        'created_by',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'street' => 'string',
        'office1' => 'string',
        'office2' => 'string',
        'warehouse1' => 'string',
        'warehouse2' => 'string',
        'warehouse3' => 'string',
        'limit_trans' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'street' => 'required',
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
