<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;

class ShipmentDetail extends Model
{
    use Uuids;
    use Userstamps;
    use LogsActivity;

    public $table = 'shipment_detail';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'shipment_id',
        'item_id',
        'item_id_variant',
        'warehouse',
        'qty'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'shipment_id' => 'string',
        'item_id' => 'string',
        'item_id_variant' => 'string',
        'warehouse' => 'string',
        'qty' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function shipment()
    {
        $this->belongsTo('\App\Models\Shipment', 'shipment_id', 'id');
    }

    public function items()
    {
        return $this->belongsTo('\App\Models\Item', 'item_id','id');
    }

    public function itemVariant()
    {
        return $this->belongsTo('\App\Models\ItemVariant', 'item_id_variant', 'id_variant')->orderBy('created_at', 'desc');
    }
    
    public function stock()
    {
        return $this->belongsTo('\App\Models\Warehouse', 'warehouse', 'id');
    }
    
}
