<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
/**
 * Class Warehouse
 * @package App\Models
 * @version February 17, 2018, 2:26 pm UTC
 *
 * @property integer id
 * @property string name
 * @property boolean default
 * @property integer created_by
 */
class Warehouse extends Model
{
    use SoftDeletes;
    use Userstamps;
    use LogsActivity;
    use Uuids;
    
    public $table = 'warehouses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'id',
        'name',
        'default',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'default' => 'boolean',
        'created_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
   
   public function detailVariant()
   {
       return $this->belongsTo('\App\Models\ItemVariant', 'warehouse');
   }

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
