<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;

class ReturnItemDetail extends Model
{
    use Userstamps, LogsActivity, Uuids;

    protected $table = "return_item_details";

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public $fillable = [
        'return_item_id',
        'item_id',
        'item_id_variant',
        'sales_order_id',
        'warehouse',
        'qty',
        'partial'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'return_item_id' => 'string',
        'sales_order_id' => 'string',
        'item_id' => 'string',
        'item_id_variant' => 'string',
        'warehouse' => 'string',
        'qty' => 'integer',
        'partial' => 'integer'
    ];

    public function returnItem()
    {
        $this->belongsTo('\App\Models\ReturnItem', 'return_item_id');
    }

    public function items()
    {
        return $this->belongsTo('\App\Models\Item', 'item_id','id');
    }

    public function itemVariant()
    {
        return $this->belongsTo('\App\Models\ItemVariant', 'item_id_variant', 'id_variant')->orderBy('created_at', 'desc');
    }

    public function stock()
    {
        return $this->belongsTo('\App\Models\Warehouse', 'warehouse', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
