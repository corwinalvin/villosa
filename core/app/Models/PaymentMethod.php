<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuids;
/**
 * Class PaymentMethod
 * @package App\Models
 * @version February 21, 2018, 4:16 am UTC
 *
 * @property string name
 * @property integer default
 * @property integer created_by
 * @property integer deleted_by
 * @property integer updated_by
 */
class PaymentMethod extends Model
{
    use SoftDeletes;
    use Userstamps;
    use LogsActivity;
    use Uuids;
    
    public $table = 'payment_methods';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static $logFillable = true;
    
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];
    

    public $fillable = [
        'name',
        'default',
        'created_by',
        'deleted_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'default' => 'integer',
        'created_by' => 'string',
        'deleted_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function createdBy()
    {
        return $this->belongsTo('\App\Models\Users', 'created_by', 'id');
    }
}
