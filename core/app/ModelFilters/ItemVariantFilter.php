<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;
use DB;

class ItemVariantFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function item($item_id){
        return $this->where('item_id', $item_id);
    }

    public function warna($warna){
        return $this->where('warna', $warna);
    }
    
    public function pattern($pattern){
        return $this->where('pattern', $pattern);
    }

    public function roll($roll){
        return $this->where('roll', $roll);
    }

    public function setup(){
        return $this->where('value', '>', 0);
    }
}
