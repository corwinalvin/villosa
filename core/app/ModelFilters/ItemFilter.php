<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class ItemFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [
        
    ];

    public function variant($variant){
        return $this->related('variants', 'id', $variant)->selectRaw('sum(item_variant.value) as stock');
    }

    public function name($name){
        return $this->whereLike('name', $name);
    }

    public function category($category){
        return $this->related('categories', 'id', $category);
    }

    public function brand($brand){
        return $this->related('brands', 'id', $brand);
    }

    public function department($department){
        return $this->related('departments', 'id', $department);
    }
}
