<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class SalesOrderFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];
    
    public function status($status){
        return $this->where('status',$status);
    }

    public function payment($payment){
        return $this->related('payment','id', $payment);
    }

    public function customer($customer){
        return $this->related('buyer','id', $customer);
    }

    public function type($type){
        return $this->related('salesType','id', $type);
    }

    public function date($date){
        return $this->whereBetween('date', [date('Y-m-d',strtotime($date['early_date'])), date('Y-m-d', strtotime($date['end_date']))]);
    }
}
