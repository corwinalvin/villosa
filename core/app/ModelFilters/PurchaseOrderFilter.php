<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class PurchaseOrderFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [
    ];

    public function status($status){
        return $this->where('status', $status);
    }

    public function supplier($supplier){
        return $this->related('suppliernya','id',$supplier);
    }
    
    public function date($date){
        return $this->whereBetween('date', [date('Y-m-d',strtotime($date['early_date'])), date('Y-m-d', strtotime($date['end_date']))]);
    }
}
