<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('products', 'Frontend\HomeController@products');
Route::get('product/{id}', 'Frontend\HomeController@productShow');
Route::get('category/{id}', 'Frontend\HomeController@category');
Route::post('productSearch', 'Frontend\HomeController@productSearch');
Route::post('searchVariant', 'Frontend\HomeController@searchVariant');
/**
 * Customer
 */
Route::post('customer', 'Frontend\HomeController@saveCustomer');
Route::get('customers', 'Frontend\HomeController@customers');
Route::get('customer/{id}', 'Frontend\HomeController@getAlamat');
/**
 * Get Data Carts
 * Post Data to Cart
 */
Route::post('cart', 'Frontend\HomeController@postCart');
Route::post('order', 'Frontend\HomeController@order');
Route::get('cart', 'Frontend\HomeController@getCart');
Route::get('cart/remove/{id}', 'Frontend\HomeController@cartRemove');
Route::get('getTotalQty', 'Frontend\HomeController@getTotalQty');