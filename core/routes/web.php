<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::group(['middleware' => 'auth'], function (){
	Route::get('/', 'Frontend\HomeController@index')->name('home');
	Route::get('/customer/cart', 'Frontend\HomeController@index');
	Route::get('/search/query={name}', 'Frontend\HomeController@index');
	Route::get('/{id}', 'Frontend\HomeController@index');
	/**
	 * BACKEND
	 */
	Route::group([ 'prefix' => 'private'], function(){
		Route::get('home', 'HomeController@index')->name('dashboard');
		Route::post('itemsBy', 'ReturnItemController@apiItems')->name('api.itemsBy');
		Route::get('getSku/{id}', 'ItemController@getSku')->name('getSku');
		Route::get('returnItem/approve/{id}', 'ReturnItemController@approve')->name('returnItems.approve');
		Route::get('getAlamat/{cus_id}/{type}', 'SalesOrderController@getAlamat')->name('getAlamat');
		Route::get('downloadCSVex', function(){
			return response()->download(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'/public/[CONTOH MARAVILLOSA].csv');
		})->name('downloadCSVex')->middleware('permission:item');
		Route::resource('warehouseTransfers', 'WarehouseTransferController')->middleware('permission:warehouse-transfer');
		Route::resource('items', 'ItemController')->middleware('permission:item');
		Route::resource('items-variant', 'ItemVariantController')->middleware('permission:item');
		Route::resource('activityLogs', 'ActivityLogController')->middleware('permission:activity-logs');
		Route::resource('returnItems', 'ReturnItemController')->middleware('permission:return-item');
		
		Route::group(['prefix' => 'sales', 'middleware' => 'permission:sales'], function (){
			Route::get('print-invoice/{id}', 'SalesOrderController@printInvoice')->name('print-invoice')->middleware('permission:sales-order');
			Route::get('sales/approve/{id}/{jenis}', 'SalesOrderController@approve')->name('salesOrders.approve')->middleware('permission:sales-order');
			Route::resource('salesOrders', 'SalesOrderController')->middleware('permission:sales-order');
			Route::resource('invoices', 'InvoicesController')->middleware('permission:invoice');
			Route::get('purchase/{id}', 'PurchaseOrderController@approve')->name('purchaseOrders.approve')->middleware('permission:purchase-order');
			Route::resource('purchaseOrders', 'PurchaseOrderController')->middleware('permission:purchase-order');
			Route::resource('shipments', 'ShipmentController')->middleware('permission:shipment');
			Route::get('shipments/create/{id}', 'ShipmentController@create')->name('shipments.buat')->middleware('permission:shipment');
			Route::get('shipments/approve/{id}', 'ShipmentController@approve')->name('shipments.approve')->middleware('permission:shipment');
		});
		
		Route::group(['prefix' => 'report', 'namespace' => 'Report', 'middleware' => 'permission:reports'], function (){
			Route::get('purchase', 'PurchaseController@index')->name('report.purchase')->middleware('permission:report-purchase');
			Route::get('purchase/detail/{date}', 'PurchaseController@detail')->name('report.purchase.detail')->middleware('permission:report-purchase');
			Route::get('sales', 'SalesController@index')->name('report.sales')->middleware('permission:report-sales');
			Route::get('sales/detail/{date}', 'SalesController@detail')->name('report.sales.detail')->middleware('permission:report-sales');
			Route::get('warehouses', 'WarehousesController@index')->name('report.warehouses')->middleware('permission:report-stock');
			/**	FILTER */
			Route::post('purchase/filter', 'PurchaseController@filter')->name('filter.purchase')->middleware('permission:report-purchase');
			Route::post('sales/filter', 'SalesController@filter')->name('filter.sales')->middleware('permission:report-sales');
			Route::post('warehouses/filter', 'WarehousesController@filter')->name('filter.item')->middleware('permission:report-stock');
			Route::get('item/find', 'WarehousesController@apiNamaBarang')->name('api.findNamaBarang')->middleware('permission:report-stock');
			Route::post('item/find/variant', 'WarehousesController@apiNamaVariant')->name('api.findNamaVariant')->middleware('permission:report-stock');
		});
		
		Route::group(['prefix' => 'setting', 'middleware'=> ['permission:settings']], function (){
			Route::resource('users', 'UsersController')->middleware('permission:users');
			Route::resource('warehouses', 'WarehouseController')->middleware('permission:warehouses');
			Route::resource('brands', 'BrandController')->middleware('permission:brands');
			Route::resource('categories', 'CategoryController')->middleware('permission:categories');
			Route::resource('salesType', 'SalesTypeController')->middleware('permission:sales-type');
			Route::resource('suplliers', 'SupllierController')->middleware('permission:suppliers');
			Route::resource('customers', 'CustomerController')->middleware('permission:customers');
			Route::resource('paymentMethods', 'PaymentMethodController')->middleware('permission:payment-methods');
			Route::resource('taxes', 'TaxController')->middleware('permission:taxes');
			Route::resource('permissions', 'PermissionController')->middleware('permission:permissions');
			Route::resource('roles', 'RolesController')->middleware('permission:roles');
			Route::resource('departments', 'DepartmentController');
			Route::resource('satuans', 'SatuanController');
			Route::resource('itemVariants', 'ItemVariantController');
		});
		
		Route::group(['prefix' => 'datatables'], function(){
			Route::get('user', 'DatatablesController@getUsers')->name('datatables.users');
			Route::get('warehouses', 'DatatablesController@getWarehouses')->name('datatables.warehouses');
			Route::get('brands', 'DatatablesController@getBrands')->name('datatables.brands');
			Route::get('categories', 'DatatablesController@getCategories')->name('datatables.categories');
			Route::get('sales', 'DatatablesController@getSales')->name('datatables.sales');
			Route::get('suplliers', 'DatatablesController@getSuplliers')->name('datatables.suplliers');
			Route::get('customers', 'DatatablesController@getCustomers')->name('datatables.customers');
			Route::get('items', 'DatatablesController@getItems')->name('datatables.items');
			Route::get('itemsSales', 'DatatablesController@getItemsSales')->name('datatables.itemsSales');
			Route::get('itemsPurchase', 'DatatablesController@getItemsPurchase')->name('datatables.itemsPurchase');
			Route::get('salesOrders', 'DatatablesController@getSalesOrders')->name('datatables.salesOrders');
			Route::get('paymentMethods', 'DatatablesController@getPaymentMethods')->name('datatables.paymentMethods');
			Route::get('warehouseTransfer', 'DatatablesController@getWarehouseTransfer')->name('datatables.warehouseTransfer');
			Route::get('itemsWT', 'DatatablesController@getItemsWT')->name('datatables.itemsWT');
			Route::get('logs', 'DatatablesController@getActivityLog')->name('datatables.logs');
			Route::get('purchaseOrder', 'DatatablesController@getPurchaseOrders')->name('datatables.purchaseOrder');
			Route::get('items/{id}/{jenis}', 'DatatablesController@getItemVariant')->name('datatables.itemVariant');
			Route::get('itemsSO/{id}', 'DatatablesController@getItemShipment')->name('datatables.itemSO');
			Route::get('salesReport', 'DatatablesController@salesReport')->name('datatables.salesReport');
			Route::get('sales/detail/{date}', 'DatatablesController@salesReportDetail')->name('datatables.salesReportDetail');
			Route::get('purchaseReport', 'DatatablesController@purchaseReport')->name('datatables.purchaseReport');
			Route::get('purchase/detail/{date}', 'DatatablesController@purchaseReportDetail')->name('datatables.purchaseReportDetail');
			Route::get('warehouseReport', 'DatatablesController@warehouseReport')->name('datatables.warehouseReport');
			Route::get('returnItems', 'DatatablesController@returnItems')->name('datatables.returnItems');
			Route::get('itemsReturns/{id}', 'DatatablesController@itemsReturn')->name('datatables.itemsReturn');
			/**
			 * Datatable as Service
			 */
			Route::get('invoices/paid', 'InvoicesController@index')->name('datatables.invoicesUnpaid');
			Route::get('invoices/unpaid', 'InvoicesController@getPaid')->name('datatables.invoicesPaid');
			Route::get('salesOrders/approve', 'SalesOrderController@getApproved')->name('datatables.soApproved');
			Route::get('salesOrders/unapprove', 'SalesOrderController@index')->name('datatables.soUnapproved');
			Route::get('shipments/shipped', 'ShipmentController@getApproved')->name('datatables.shipmentApproved');
			Route::get('shipments/unshipped', 'ShipmentController@index')->name('datatables.shipmentUnapproved');
		});
	});
});