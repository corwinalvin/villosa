<table class="table table-responsive" id="users-table">
    <thead>
        <tr>
        <th>Name</th>
        <th>Email</th>
        <th >Action</th>
        </tr>
    </thead>
</table>

@section('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.users') !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data : 'action' ,name:'action'}
        ]
    });
});
</script>
@stop