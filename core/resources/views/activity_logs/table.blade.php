<div class="table-responsive">
    <table class="table" id="activityLogs-table">
        <thead>
            <tr>
                <th>Log Name</th>
                <th>Description</th>
                <th>Subject Id</th>
                <th>Subject Type</th>
                <th>Causer</th>
                <th>Created At</th>
                <th >Action</th>
            </tr>
        </thead>
    </table>
</div>
@section('scripts')
<script>
$(function() {
    $('#activityLogs-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.logs') !!}',
        columns: [
            { data: 'log_name', name: 'log_name' },
            { data: 'description', name: 'description' },
            { data: 'subject_id', name: 'subject_id' },
            { data: 'subject_type', name: 'subject_type' },
            { data: 'causer.name', name: 'causer.name' },
            { data: 'created_at', name: 'created_at', render: function (data) {
                var date = new Date(data);
                var month = date.getMonth() + 1;
                return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
            }},
            { data : 'action' ,name:'action'}
        ],
        order: [[6, "asc" ]]
    });
});
</script>
@stop