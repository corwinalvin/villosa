@extends('layouts.app')
@section('title', 'Activity Logs')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Activity Logs</h1>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-danger">
            <div class="box-body">
                    @include('activity_logs.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

