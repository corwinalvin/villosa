@section('css')
<style>
    h4{text-transform: uppercase !important;font-weight: bolder;}
</style>
@stop
<!-- Log Name Field -->
<div class="form-group col-sm-4">
    {!! Form::label('log_name', 'Log Name:') !!}
    <p>{!! $activityLog->log_name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group col-sm-4">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $activityLog->description !!}</p>
</div>

<!-- Subject Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    <p>{!! $activityLog->subject_id !!}</p>
</div>

<!-- Subject Type Field -->
<div class="form-group col-sm-4">
    {!! Form::label('subject_type', 'Subject Type:') !!}
    <p>{!! $activityLog->subject_type !!}</p>
</div>

<!-- Causer Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('causer_id', 'Causer:') !!}
    <p>{!! $activityLog->causer->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-4">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $activityLog->created_at !!}</p>
</div>

<!-- Properties Field -->
<div class="form-group col-sm-12">
    {!! Form::label('properties', 'Data:') !!}
    <p></p>
    @if($activityLog->log_name == "updated")
    <h4>Sesudah :</h4> 
    <pre>{!! print_r(json_decode($activityLog->properties)->attributes) !!}</pre>
    <h4>Sebelumnya :</h4> 
    <pre>{!! print_r(json_decode($activityLog->properties)->old) !!}</pre>
    @else
    <pre>{!! print_r(json_decode($activityLog->properties)) !!}</pre>
    @endif
</div>


