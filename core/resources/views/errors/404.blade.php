@extends('layouts.app')
@section('content')
<section class="content">

      <div class="error-page">
        <h2 class="headline text-red">404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Oops! Not Found.</h3>

          <p>
            {{$exception->getMessage()}}
            Meanwhile, you may <a href='{{route("dashboard")}}'>return to dashboard</a>.
          </p>
        </div>
      </div>
      <!-- /.error-page -->

    <div>
</div>
</section>
@stop