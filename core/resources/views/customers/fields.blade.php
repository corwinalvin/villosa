@section('css')
<style>
.switch{position:relative;display:inline-block;width:60px;height:34px}.switch input{display:none}.slider{position:absolute;cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:#ccc;-webkit-transition:.4s;transition:.4s}.slider:before{position:absolute;content:"";height:26px;width:26px;left:4px;bottom:4px;background-color:#fff;-webkit-transition:.4s;transition:.4s}input:checked+.slider{background-color:#2196F3}input:focus+.slider{box-shadow:0 0 1px #2196F3}input:checked+.slider:before{-webkit-transform:translateX(26px);-ms-transform:translateX(26px);transform:translateX(26px)}.slider.round{border-radius:34px}.slider.round:before{border-radius:50%}
</style>
@stop
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Street Field -->
<div class="form-group col-sm-6">
    {!! Form::label('street', 'Street:') !!}
    {!! Form::text('street', null, ['class' => 'form-control']) !!}
</div>
<!-- Zipcode Field -->
@if(strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
<div class="form-group col-sm-6">
    {!! Form::label('city', 'Warehouse 1:') !!}
    {!! Form::textarea('warehouse[]', null, ['class' => 'form-control']) !!}
</div>
<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'Warehouse 2:') !!}
    {!! Form::textarea('warehouse[]', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('city', 'Warehouse 3:') !!}
    {!! Form::textarea('warehouse[]', null, ['class' => 'form-control']) !!}
</div>
@else
    @foreach($detail as $detail)
        <div class="form-group col-sm-6">
            {!! Form::label('city', 'Warehouse 1:') !!}
            {!! Form::textarea('warehouse[]', $detail->warehouse, ['class' => 'form-control']) !!}
        </div>
    @endforeach
@endif

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <br>
    <!-- Rounded switch -->
    Tidak Banned
    <label class="switch">  
    {!! Form::hidden('status', false) !!}
    {!! Form::checkbox('status', '1', null) !!}
    <span class="slider round"></span>
    </label>
    Banned
</div>

<div class="form-group col-sm-6">
    {!! Form::label('limit', 'Limit:') !!}
    {!! Form::text('limit_trans', null, ['class' => 'form-control' , 'onkeypress'=>'return event.charCode >= 48 && event.charCode <= 57']) !!}

</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
</div>
