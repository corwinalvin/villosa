
<div class="col-lg-6">
    <!-- Name Field -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        <p>{!! $customer->name !!}</p>
    </div>

    <!-- Email Field -->
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        <p>{!! $customer->email !!}</p>
    </div>

    <!-- Phone Field -->
    <div class="form-group">
        {!! Form::label('phone', 'Phone:') !!}
        <p>{!! $customer->phone !!}</p>
    </div>

    <!-- Created At Field -->
    <div class="form-group">
        {!! Form::label('created_at', 'Created At:') !!}
        <p>{!! $customer->created_at !!}</p>
    </div>
</div>
<div class="col-lg-6">
    {!! Form::label('created_at', 'Warehouses:') !!}
    <br>
    @foreach($warehouses as $warehouse)
        <p>{{$warehouse->warehouse}} </p>
    @endforeach
</div>