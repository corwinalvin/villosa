<table class="table table-responsive" id="customers-table">
    <thead>
        <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Status</th>
        <th>Action</th>
        </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#customers-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.customers') !!}',
        columns: [
            { data: 'name', name: 'name', render : function (data, index, metaData){
                // console.log(metaData)
                return '<a href=customers/'+metaData.id+'>'+data+'</a>'
            }},
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'status', name: 'status', render : function (data) {
                if(data == 1){
                    return 'Banned';
                }else{
                    return 'Tidak Banned';
                }
            }},
            { data : 'action' ,name:'action'}
        ]
    });
});
</script>
@stop