
@section('css')
<style>
#items-table tfoot tr th input {
    width: 100px;
}
#input{
    display : inline-flex;
}
.qty,.diskon,.partialInput {
    width: 60px;
}
#modal-default {
  position: relative;
}
.modal-dialog {
  position: fixed;
  overflow-y: initial !important;
  width: 23%;
  margin: 0;
  padding: 10px;
}
.modal{
  background: transparent !important;
}
.modal-backdrop{
  display: none !important
}
.modal-backdrop{
  display: none !important
}
body.modal-open{
  overflow: visible;
   /*this line allowed form to be scrollable */
}
.modal-body{ 
 /*this section allowed form input-fields to be clickable */
  max-height: calc(70vh - 210px);
  overflow-y: auto;
  padding: 5px !important;
}
button .fa{
  font-size: 12px;
  margin-left: 10px;
}
</style>
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal"> <i class='fa fa-times'></i> </button>
        <h4 class="modal-title">Item Dipilih</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Kosong</li>
        </ul>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="minmaxCon"></div>
@stop
<!-- Sales Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sales_id', 'Invoices:') !!}
    <select name="sales_id" class="form-control find-sales">
        <option value="">-Pilih Sales/Invoice-</option>
        @foreach($sales as $sales)
        <option value="{{$sales->id}}" {{(isset(Session::get('items')[0]) && Session::get('items')[0]->id == $sales->id) ? 'selected=selected' : ''}} {{(isset($returnItem) && $returnItem->sales_id == $sales->id) ? 'selected=selected':''}}>{{$sales->reference}}</option>
        @endforeach
    </select>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('shipment_id', 'Shipment:') !!}
    <select name="shipment_id" class="form-control find-shipment">
        <option value="">-Pilih Shipment-</option>
        @foreach($shipments as $shipment)
            <option value="{{$shipment->id}}" {{(isset(Session::get('items')[0]) && Session::get('items')[0]->id == $shipment->id) ? 'selected=selected' : ''}} {{(isset($returnItem) && $returnItem->shipment_id == $shipment->id) ? 'selected=selected':''}}>{{$shipment->reference}}</option>
        @endforeach
    </select>
</div>

<!-- Shipment Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <div class="radio">
        <label class="radio-inline">
            {!! Form::radio('status', 1, ['class' => 'form-control']) !!} Approved
        </label>
        <label class="radio-inline">
            {!! Form::radio('status', 0, ['class' => 'form-control']) !!} Not Approved
        </label>
    </div>
</div>
<div class="col-lg-12 ">
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-hover" id="table-data">
                <thead>
                    <tr>
                        <th>Barang</th>
                        <th>Variant</th>
                        <th>Qty-Warehouse</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @if(!empty(Session::get('items')))
                    @if(isset(Session::get('items')[0]->detailOrders))
                        @foreach(Session::get('items')[0]->detailOrders as $item)
                            <tr>
                                <input type="hidden" class="item" name="item_id[]" value="{{$item->item_id}}" disabled>
                                <input type="hidden" class="variant" name="item_id_variant[]" value="{{$item->itemVariant->id_variant}}" disabled>
                                <input type="hidden" class="wh" name="warehouse[]" value="{{$item->warehouse}}" disabled>
                                <input type="hidden" class="so" name="sales_order_id[]" value="{{$item->id}}" disabled>
                                <td>{{$item->items->name}}</td>
                                <td>{{$item->itemVariant->warna}} {{$item->itemVariant->pattern}} {{$item->itemVariant->size}} {{$item->itemVariant->roll}}</td>
                                <td>Qty : {{$item->qty}} - {{$item->stock->name}}</td>
                                <td><div id="input"><input type="text" name="qty[]" qty="{{$item->qty}}" disabled required class="form-control qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Qty"></div><br><a class="partial" style="cursor:pointer">Partial</a></td>
                            </tr>
                        @endforeach
                    @elseif(isset(Session::get('items')[0]->detailShipment))
                        @foreach(Session::get('items')[0]->detailShipment as $index => $item)
                        <tr>
                            <input type="hidden" class="so" name="sales_order_id[]" value="{{$item->id}}" disabled>
                            <input type="hidden" class="item" name="item_id[]" value="{{$item->item_id}}" disabled>
                            <input type="hidden" class="variant" name="item_id_variant[]" value="{{$item->itemVariant->id_variant}}" disabled>
                            <input type="hidden" class="wh" name="warehouse[]" value="{{$item->warehouse}}" disabled>
                            <td>{{$item->items->name}}</td>
                            <td>{{$item->itemVariant->warna}} {{$item->itemVariant->pattern}} {{$item->itemVariant->size}} {{$item->itemVariant->roll}} {{(Session::get('items')[0]->salesOrder->detailOrders[$index]->partial !== null) ? '- ('.Session::get('items')[0]->salesOrder->detailOrders[$index]->partial.')' : ''}}</td>
                            <td>Qty : {{$item->qty}} - {{$item->stock->name}}</td>
                            <td><div id="input"><input type="text" name="qty[]" qty="{{$item->qty}}" disabled required class="form-control qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Qty"></div><br>
                            @if(Session::get('items')[0]->salesOrder->detailOrders[$index]->partial == null)<a class="partial" style="cursor:pointer">Partial</a>@endif</td>
                        </tr>
                        @endforeach
                    @endif
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('returnItems.index') !!}" class="btn btn-default">Cancel</a>
</div>
@section('scripts')
<script type="text/javascript" src="{{asset('js/passive.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script>
$(function() {
    $(".find-sales").change(function(){
        var id = $(this).find('option:selected').val();
        $('#id').val(id);
        $('#type').val('sales');
        $("#sales-form").submit();
    });
    $(".find-shipment").change(function(){
        var id = $(this).find('option:selected').val();
        $('#id').val(id);
        $('#type').val('shipment');
        $("#sales-form").submit();
    });
    var rows_selected = [];
    var items_selected = [];
    var variant_selected = [];
    var lain = [];
    @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
        var ids = <?php echo json_encode($items)?>;
        var qty = <?php echo json_encode($qty)?>;
        var warehouse = <?php echo json_encode($warehouse)?>;
        var so = <?php echo json_encode($sales_order)?>;
        var sales_id = <?php echo json_encode($id)?>;
        var partial = <?php echo json_encode($partial)?>;
    @endif
    var table = $("#table-data").DataTable({
        @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
        processing: true,
        serverSide: true,
        <?php
            $id = ($returnItem->sales_id===null) ? $returnItem->shipment_id : $returnItem->sales_id;
        ?>
        ajax: '{!! route('datatables.itemsReturn', $id) !!}',
        columns: [
            { data: 'name', name: 'items.name' },
            { data: 'warna', name: '', searchable : false, render:function (data,row,metaData) {
                return metaData.item_variant.warna + ' ' + metaData.item_variant.size + ' ' + metaData.item_variant.pattern + ' ' + metaData.item_variant.roll;
            }},
            { data: 'pattern', name: 'item_variant.pattern',searchable : false, render:function (data, row, metaData) {
                return metaData.qty+'-'+metaData.stock.name;
            }},
            { data : 'action', name:'qty', orderable:false, searchable:false}
        ],
        @endif
        rowCallback: function(row, data, index){
            // Get row ID
            @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
            var rowId = data.item_id_variant;
            @else
            var rowId = data[1];
            @endif
            // If row ID is in the list of selected row IDs
            if($.inArray(rowId, rows_selected) !== -1){
                $(row).addClass('selected');
                if ($(row).hasClass('selected')) {
                    $(row).find('.qty').attr('disabled', false);
                    $(row).find('.variant').attr('disabled', false);
                    $(row).find('.item').attr('disabled', false);
                    $(row).find('.so').attr('disabled', false);
                    $(row).find('.wh').attr('disabled', false);
                }
                @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
                    for (var i = 0; i < ids.length; i++) {
                        $(row).find(".qty").val(qty[i]);
                        $(row).find(".so").val(so[i]);
                        $(row).find(".wh").val(warehouse[i]);
                    }
                    $(row).append("<input type='hidden' name='return_id' value="+sales_id+">");
                @endif
            }
        },fnInitComplete: function () {
            @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
            for (var i = 0; i < ids.length; i++) {
                $("#table-data tr").each(function(row, data) {
                if ($(this).find('.variant').val() ==ids[i]) {
                    $(this).find("td").trigger('click');
                    $(this).find(".qty").val(qty[i]);
                    $(this).find(".qty").click();
                    $(this).find(".wh").val(warehouse[i]);
                    $(this).find(".so").val(so[i]);
                    if(partial[i]){
                      $(this).find("#input").append("<input type='text' onkeypress='return event.charCode >= 48 && event.charCode <= 57' name='partial[]' placeholder='C. Roll' value="+partial[i]+" class='form-control partialInput' required>");
                    }
                    $(this).append("<input type='hidden' name='return_id' value="+sales_id+">");
                }
                }); 
            }
            @endif
        }
    });

    $('#table-data tbody').on('keyup', '.qty,.partialInput', function(e){
        addItem();
    });

    $('#table-data tbody').on('click', '.partial', function(e){
        var $row = $(this).closest('tr');
        var html = '<input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="partial[]" placeholder="C. Roll" class="form-control partialInput" required/>'
        if($row.find('.partialInput').length == 1){
            $(".partialInput").remove();
        }else{
            $row.find('#input').append(html);
        }
    });

    $('#table-data tbody').on('click', 'td', function(e){
        var gr = this.cellIndex;
        var $row = $(this).closest('tr');
        $('.modal-body').html('');
        // Get row data
        var data = table.row($row).data();
        // Get row ID
        @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
        var rowId = data.item_id_variant;
        @else
        var rowId = data[1];
        @endif
        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);
        // If checkbox is checked and row ID is not in list of selected row IDs
        if(!$row.hasClass('selected') && index === -1){
            rows_selected.push(rowId);
            $('.modal-title').html('Item Dipilih (Total : '+rows_selected.length + ')');
        } else if (gr < 3 && index !== -1){
            rows_selected.splice(index,1);
            $('.modal-title').html('Item Dipilih (Total : '+rows_selected.length + ')');
        }

        if(!$row.hasClass('selected')){
            $row.find('.qty').attr('disabled', false);
            $row.find('.qty').focus();
            $row.find('.wh').attr('disabled', false);
            $row.find('.variant').attr('disabled', false);
            $row.find('.item').attr('disabled', false);
            $row.find('.so').attr('disabled', false);
            $row.addClass('selected');
            $('#modal-default').modal({keyboard : false, backdrop : 'static', show :true }).draggable({ cursor :'move', handle: ".modal-header"});
            $('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');
        }else if (gr < 3) {
            $row.find('.qty').attr('disabled', true);
            $row.find('.qty').focus();
            $row.find('.wh').attr('disabled', true);
            $row.find('.variant').attr('disabled', true);
            $row.find('.so').attr('disabled', true);
            $row.find('.item').attr('disabled', true);
            $row.find('.partialInput').remove();
            $row.removeClass('selected');
        } 
        addItem();
        e.stopPropagation();
    });
    function addItem(argument) {
        lain.length = 0;
        localStorage.removeItem('item');
        $('.modal-body').html('');
        $("#table-data tr").each(function(row, data) {
        if ($(this).hasClass('selected')) {
            var isi = table.row($(this)).data();
            @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
            var variant = isi.item_variant.warna + ' ' + isi.item_variant.size + ' ' + isi.item_variant.pattern + ' ' + isi.item_variant.roll;
            var nama = isi.items.name;
            @else
            var variant = (isi[1] !=null) ? isi[1] : '';
            var nama = isi[0];
            @endif
            var roll = parseFloat($(this).find('td').eq(1).text().replace(/\D/g,''));
            var partial = ($(this).find('.partialInput').val() >= roll ? $(this).find('.partialInput').val(''): $(this).find('.partialInput').val());
            var qty = (parseInt($(this).find('.qty').val()) > parseInt($(this).find('.qty').attr('qty')) || parseInt($(this).find('.qty').val()) == 0 ? $(this).find('.qty').val(0) : $(this).find('.qty').val());
            localStorage.setItem('item', nama + ' (' + variant + ' '  + ') <br> Qty : ' +qty);
            lain.push(localStorage.getItem('item'));
        }
        });
        $('.modal-body').append(makeUL(lain, variant_selected));
    }

    function makeUL(array, array2) {
        var list = document.createElement('ul');
        for(var i = 0; i < array.length; i++) {
            var item = document.createElement('li');
            item.innerHTML = array[i];
            item.setAttribute('id', array2[i]);
            list.appendChild(item);
        }
        return list;
    }
});
</script>
@stop