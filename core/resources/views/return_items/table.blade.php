<table class="table table-responsive" id="returnItems-table">
    <thead>
        <tr>
            <th>Sales</th>
            <th>Shipment</th>
            <th>Keterangan</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script>
$(function() {
    $('#returnItems-table').DataTable({
        processing: true,
        serverSide: true,
        dom : 'Blfrtip',
        buttons : ['pdfHtml5', 'excelHtml5'],
        ajax: '{!! route('datatables.returnItems') !!}',
        columns: [
            { data: 'sales.reference', name: 'sales.reference' , render:function(data, row, metaData){
                if (data) {
                    return '<a href="{{url('private/sales/salesOrders')}}/'+metaData.sales_id+'">'+metaData.sales.reference+'</a>';
                }
                return '-';
            }},
            { data: 'shipment.reference', name: 'shipment.reference', render:function(data, row, metaData){
                if (data) {
                    return '<a href="{{url('private/sales/shipments')}}/'+metaData.shipment_id+'">'+metaData.shipment.reference+'</a>';
                }
                return '-';
            }},
            { data: 'keterangan',name: 'keterangan', render:function(data){
                if(data){
                    return data;
                }
                return '-';
            }},
            { data : 'action' ,name:'action'}
        ]
    });
});
</script>
@stop