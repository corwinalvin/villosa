@php
  $item_count = ceil(count($returnItem->returnItems) / 13);
  $temp_item  = 10;
  $curent     = 0;
  $item_data  = array_chunk($returnItem->returnItems->toArray(), 13);
@endphp
<!DOCTYPE html>
<html>
<head>
  <title>Retur {{strtoupper(substr($returnItem->id, 0, 5))}}</title>
</head>
<style>
html, body {
    /*margin: 0;*/
    font-size: 11px;
  line-height: 150%;
}

@page {
  size: 21.59cm 13.97cm;
}

.logo {
  width: 30%;
}

.borderless td, .borderless th {
    border: none !important;
}
.s {
   border: none !important;
   background:white !important;
}
.bordertable{
  border-left: 1px solid #000000;
  border-bottom: 1px solid #000000;
  border-top: 1px solid #000000;
  border-right: 1px solid #000000;
}

.borderth{
  border-bottom: 1px solid #000000;
  /*border-top: 1px solid #000000;
  bord*/er-right: 1px solid #000000;
}
.bordertd{
  border-right: 1px solid #000000;
}
.classtr{
  size: 21.59cm 13.97cm;
}
tr.spaceUnder > td
{
  padding-bottom: -0.5em;
}

.footer {
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  text-align: center;
}


.break {
  page-break-after: always
}

</style>
<body style="margin: -30px -30px -30px -30px; font-family: Tahoma,Arial, sans-serif;" >
  @for($i = 0; $i < count($item_data); $i++)
  <!-- For untuk masing2 page -->
    <div id="wrapper">  
      <table >
        <tr>
          <td style="width: 557px; font-size: 20px blod;" colspan="7">RETURN ITEMS</td>
          <td colspan="2">No Return Item :</td>
          <td align="left">#{{strtoupper(substr($returnItem->id, 0, 5))}}</td>
        </tr>
        <tr>
          <td valign="top" rowspan="4">Bill To</td>
          <td colspan="5" style="padding: 5px;" class="bordertable"  width="60">{{(isset($returnItem->shipment)) ? $returnItem->shipment->salesOrder->buyer->name : $returnItem2->sales->buyer->name}}</td>
          <td></td>
          <td colspan="2">Invoice Date </td>
          <td></td>
        </tr>
        <tr>
          <td valign="top" rowspan="3" colspan="5" style="border-left: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;padding: 5px " width="60">
            {{(isset($returnItem->shipment)) ? $returnItem->shipment->salesOrder->detailAlamat->warehouse : $returnItem2->sales->detailAlamat->warehouse}} <br>Phone: {{(isset($returnItem->shipment)) ? $returnItem->shipment->salesOrder->buyer->phone : $returnItem2->sales->buyer->phone}} Email: {{(isset($returnItem->shipment)) ? $returnItem->shipment->salesOrder->buyer->email : $returnItem2->sales->buyer->email}}
          </td>
          <td></td>
          <td></td>
          <td colspan="2" align="center">{{ date('d M Y', strtotime($returnItem->created_at)) }}</td>
        </tr>
        <tr>
          <td></td>
          <td>Term</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>Salesman</td>
          <td colspan="2" class="bordertable" align="center"> {{$returnItem->createdBy->name}}</td>
        </tr>
      </table>
      <table id="salesInvoice">
        <tr>
          <td align="left" style="width: 200px" class="borderth">Item Description</td>
          <td align="left" style="width: 80px;" class="borderth">Qty</td>
          <td align="left" style="width: 100px;" class="borderth">Warehouse</td>
        </tr>
        @php
          (float)$tot = $dis = $rp = $discP = $discPt = 0;
        @endphp
        @foreach($item_data[$i] as $k => $items)
            @php
              $itemF = (object)$items;
              $item = (object)$itemF->items;
              $itemV = (object)$itemF->item_variant;
            @endphp
            <tr class="spaceUnder">
            <td>{{ $item->name }} ({{$itemV->warna}} {{(isset($itemV->pattern)) ? ',' : '' }} {{$itemV->pattern}}{{(isset($itemV->size)) ? ',' : '' }} {{$itemV->size}}{{(isset($itemV->roll)) ? ',' : '' }} {{$itemV->roll}}) {{(isset($itemF->partial)) ? '- '.$itemF->partial : '' }}</td>
            <td align="left">{{$itemF->qty}}</td>
            <td align="left">{{ $itemF->stock['name'] }}</td>
            </td>
          </tr>
        @endforeach
      </table>
      <br>
      <div class="footer">
        <table>
        <tr>
          <td  style="width: 379px; border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black" colspan="7"></td>
          <td style="width: 100px;">&nbsp;</td>
          <td class="" style="width: 134px;"></td>
          <td class="" colspan="2" align="right" style="width: 100px"></td>
        </tr>
        <tr>
          <td colspan="7" style="border-bottom: 1px solid black; border-right: 1px solid black;border-left: 1px solid black"><p></p></td>
          <td style="width: 50px;"> </td>
          <td class="" style="width: 134px;"></td>
          <td class="" colspan="2" align="right" style="width: 100px"></td>
        </tr>
        <tr>
          <td colspan="2" align="center">Prepared By</td>
          <td></td>
          <td colspan="2" align="center">Approved By</td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2" style="border-bottom: 1px solid #000000; height: 10px"></td>
          <td style="height: 25px"></td>
          <td colspan="2" style="border-bottom: 1px solid #000000"></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
      </div>
    </div>
    <div class="break"></div>
  @endfor

  
</body>
</html>