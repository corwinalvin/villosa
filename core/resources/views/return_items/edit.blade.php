@extends('layouts.app')
@section('title', 'Return Item')
@section('content')
    <section class="content-header">
        <h1>
            Return Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($returnItem, ['route' => ['returnItems.update', $returnItem->id], 'method' => 'patch']) !!}

                        @include('return_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection