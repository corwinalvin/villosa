@extends('layouts.app')
@section('title', 'Return Item')
@section('content')
    <section class="content-header">
        <h1>
            Return Item
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-danger">

            <div class="box-body">
                <div class="row">
                    <form action="{{route('api.itemsBy')}}" method="POST" id="sales-form">{{csrf_field()}}<input type="hidden" name="type" value="sales" id="type"><input type="hidden" name="id" id="id"></form>
                    {!! Form::open(['route' => 'returnItems.store']) !!}

                        @include('return_items.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
