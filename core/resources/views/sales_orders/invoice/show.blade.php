@extends('layouts.app')
@section('content')
<div class="content">
  <div class="clearfix"></div>
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
        <i class="fa fa-globe"></i> {{config('app.name')}}
        <small class="pull-right">Date: {{date_format($salesOrder->date, 'd/m/Y')}}</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        From
        <address>
          <strong>{{config('app.name')}}.</strong><br>
          Jakarta<br>
          Indonesia<br>
          Phone: (804) 123-5432<br>
          Email: info@maravillosa.com
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        To
        <address>
          <strong>{{$salesOrder->buyer->name}}</strong><br>
          {{$salesOrder->buyer->street}} <br>
          {{$salesOrder->buyer->city}}, {{$salesOrder->buyer->state}} {{$salesOrder->buyer->zipcode}}<br>
          Phone: {{$salesOrder->buyer->phone}}<br>
          Email: {{$salesOrder->buyer->email}}
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Invoice #{{$salesOrder->reference}}</b><br>
        <br>
        <b>Order ID:</b> {{strtoupper(substr($salesOrder->id, 0, 5))}}<br>
        <!-- <b>Payment Due:</b> 2/22/2014<br> -->
        <b>Account:</b> {{strtoupper(substr($salesOrder->buyer->id, 0, 5))}}
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Qty</th>
              <th>Product</th>
              <th>Rate</th>
              <th>Disc.</th>
              <th>Description</th>
              <th>Subtotal</th>
            </tr>
          </thead>
          <tbody>
            @php
            (float)$tot = $dis = $rp = $discP = $discPt = 0;
            @endphp
            @foreach($salesOrder->detailOrders as $detailOrder)
            <tr>
              <td>{{$salesOrder->detailOrders[$loop->index]->qty}}</td>
              <td>{{$salesOrder->detailOrders[$loop->index]->items->name}} ({{$salesOrder->detailOrders[$loop->index]->itemVariant->warna}} {{(isset($salesOrder->detailOrders[$loop->index]->itemVariant->pattern)) ? ',' : '' }} {{$salesOrder->detailOrders[$loop->index]->itemVariant->pattern}}{{(isset($salesOrder->detailOrders[$loop->index]->itemVariant->size)) ? ',' : '' }} {{$salesOrder->detailOrders[$loop->index]->itemVariant->size}}{{(isset($salesOrder->detailOrders[$loop->index]->itemVariant->roll)) ? ',' : '' }} {{$salesOrder->detailOrders[$loop->index]->itemVariant->roll}})
              </td>
              <td>{{rupiah($salesOrder->detailOrders[$loop->index]->itemVariant->price)}}</td>
              <td>
                @if(strlen($salesOrder->detailOrders[$loop->index]->diskon) <= 3)
                <?php $discP = $salesOrder->detailOrders[$loop->index]->diskon; ?>
                {{$salesOrder->detailOrders[$loop->index]->diskon}}% -- {{rupiah($diskon =($salesOrder->detailOrders[$loop->index]->diskon/100)*$salesOrder->detailOrders[$loop->index]->itemVariant->price)}}
                @else
                {{$diskon = $salesOrder->detailOrders[$loop->index]->diskon}}
                @endif
              </td>
              <td>{{$salesOrder->detailOrders[$loop->index]->items->desc}}</td>
              <td>
                @if(isset($diskon))
                {{rupiah($rp = ($salesOrder->detailOrders[$loop->index]->itemVariant->price * $salesOrder->detailOrders[$loop->index]->itemVariant->roll * $salesOrder->detailOrders[$loop->index]->qty)-$diskon)}}
                @else
                {{rupiah($rp = $salesOrder->detailOrders[$loop->index]->itemVariant->price * $salesOrder->detailOrders[$loop->index]->itemVariant->roll * $salesOrder->detailOrders[$loop->index]->qty)}}
                @endif
              </td>
              @php
              $tot +=$rp;
              $dis +=$diskon;
              $discPt +=$discP;
              @endphp
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">
        <p class="lead" style="font-weight: 100;">Payment Methods:
          <br><small class="help-block">{{$salesOrder->payment->name}}</small>
        </p>
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          {{($salesOrder->note != "" ? $salesOrder->note : 'Terimakasih sudah membeli di Maravillosa')}}
        </p>
      </div>
      <!-- /.col -->
      <div class="col-xs-6">
        <!-- <p class="lead">Amount Due 2/22/2014</p> -->
        <div class="table-responsive">
          <table class="table">
            <tbody><tr>
              <th style="width:50%">Subtotal:</th>
              <td>{{rupiah($tot)}}</td>
            </tr>
            <tr>
              <th>Disc. Invoice ({{$diP = $discPt + $salesOrder->diskon}}%):</th>
              <td>
                @if(strlen($salesOrder->diskon) <= 3)
                <?php
                $disInv = ($salesOrder->diskon/100)*$tot;
                ?>
                @else
                $disInv = $salesOrder->diskon;
                @endif
                -{{rupiah($diPt=$disInv+$dis)}}
              </td>
            </tr>
            <tr>
              <th>Tax ({{(isset($salesOrder->taxR->rate)) ? $rate = $salesOrder->taxR->rate : $rate = 0}}%)</th>
              <td>{{rupiah($tax = ($rate/100)*$tot)}}</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>{{rupiah(($tot+$tax)-$diPt)}}</td>
            </tr>
          </tbody></table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="{{route('print-invoice', $salesOrder->id)}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
        @if($salesOrder->status == 1)
          @if($salesOrder->status_invoice != 1)
        <a href="{{route('salesOrders.approve',['id' => $salesOrder->id, 'jenis' => 'inv'])}}" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Approve</a>
          @else
         <a href="{{route('shipments.buat',['id' => $salesOrder->id])}}" class="btn btn-warning pull-right"><i class="fa fa-truck"></i> Ship</a>
          @endif
        @endif
        <!--           <a href="{{route('salesOrders.edit', $salesOrder->id)}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
          <i class="fa fa-edit"></i> Edit
        </a> -->
      </div>
    </div>
  </section>
</div>
@stop