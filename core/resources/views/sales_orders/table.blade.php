<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">UNAPPROVED SALES ORDERS</a></li>
      <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" onclick="paid();">APPROVED SALES ORDERS</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="table-responsive">
          <?php echo $dataTable->table(['class' => 'table']); ?>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
          <div class="table-responsive">
             <table class="table" id="salesOrders-table-approved">
                  <thead>
                      <tr>
                      <th>Customer</th>
                      <th>Date</th>
                      <th>Reference</th>
                      <th>Status</th>
                      <th>Created By</th>
                      <th>Action</th>
                      </tr>
                  </thead>
              </table>            
          </div>
      </div>
      <!-- /.tab-pane -->
      <!-- /.tab-pane -->
    </div>
<!-- /.tab-content -->
</div>
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
{!! $dataTable->scripts() !!}
<script>
function paid() {
  if (!$.fn.dataTable.isDataTable('#salesOrders-table-approved')) {
    $('#salesOrders-table-approved').DataTable({
        processing: true,
        serverSide: true,
        dom : 'Blfrtip',
        buttons: [
            'pdfHtml5', 'excelHtml5'
        ],
        ajax: '{!! route('datatables.soApproved') !!}',
        columns: [
            { data: 'buyer.name', name: 'buyer.name' },
            { data: 'date', name: 'date', render: function (data) {
                var date = new Date(data);
                var month = date.getMonth() + 1;
                return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
            }},
            { data: 'reference',name: 'reference' },
            { data: 'status_invoice', name: 'status_invoice', render: function (data) {
                if(data==0){
                    return '<div style="border: 1px solid red !important;color: red !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;">UNPAID</div>';
                }else{
                     return '<div style="border: 1px solid green !important;color: green !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;">PAID</div>';
                }
            } },
            { data : 'created_by.name' ,name:'createdBy.name'},            
            { data : 'action', name : 'action', orderable:false, searchable :false, exportable : false}
        ],
        order : [[1, "asc"]]
    });
  }
}
</script>
@stop