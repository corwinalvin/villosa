@section('css')
<style>
#items-table tfoot tr th input {
    width: 100px;
}
#input{
  display:inline-flex;
}
.qty,.diskon,.partialInput {
    width: 43px;
}
#modal-default {
  position: relative;
}
.modal-dialog {
  position: fixed;
  overflow-y: initial !important;
  width: 23%;
  margin: 0;
  padding: 10px;
}
.modal{
  background: transparent !important;
}
.modal-backdrop{
  display: none !important
}
.modal-backdrop{
  display: none !important
}
body.modal-open{
  overflow: visible;
   /*this line allowed form to be scrollable */
}
.modal-body{ 
 /*this section allowed form input-fields to be clickable */
  max-height: calc(70vh - 210px);
  overflow-y: auto;
  padding: 5px !important;
}
button .fa{
  font-size: 12px;
  margin-left: 10px;
}
</style>
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal"> <i class='fa fa-times'></i> </button>
        <h4 class="modal-title">Item Dipilih</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Kosong</li>
        </ul>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="minmaxCon"></div>
@stop
<!-- Reference Field -->
<div class="form-group col-sm-4">
    {!! Form::label('reference', 'Reference:') !!}
    {!! Form::text('reference', $code, ['class' => 'form-control', 'readonly' => true]) !!}
</div>
<!-- Customer Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('customer_id', 'Customer:') !!}
    {!! Form::select('customer_id', $customers , null,['class' => 'form-control customer', 'required'=> 'required']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('customer_id', 'Alamat:') !!}
    @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
    <select name="alamat_id" class="form-control alamat" required="required"></select>
    @else
    <select name="alamat_id" class="form-control alamat" required="required"></select>
    @endif
</div>
<!-- Sales Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('sales_id', 'Sales Type:') !!}
    {!! Form::select('sales_type_id', $sales, null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('payment_id', 'Payment:') !!}
    {!! Form::select('payment_id', $payments, null, ['class' => 'form-control']) !!}
</div>
<!-- Date Field -->
<div class="form-group col-sm-4">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::text('date', date('Y-m-d'), ['class' => 'form-control', 'id' => 'datepicker']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('tax', 'Tax:') !!}
    {!! Form::select('tax', $tax, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('disc', 'Discount:') !!}
    {!! Form::number('diskon', null, ['class' => 'form-control', 'onkeypress' => "return event.charCode >= 48 && event.charCode <= 57"]) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-4">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>
<div class="col-lg-12 ">
    <p class="help-block">*) Mohon isi kuantiti <strong>TIDAK LEBIH</strong> dari stok <i><u>warehouse</u></i> yang dipilih</p>
    <p class="help-block">*) Diskon dapat berupa persen atau nominal - <strong> Jangan Pakai Koma atau Lambang Persen</strong></p>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table " id="items-table">
                <thead>
                    <tr>
                        <th width="1"></th>
                        <th width="1">SKU</th>
                        <th>Name</th>
                        <th>Warna</th>
                        <th>Pattern</th>
                        <th>Roll</th>
                        <th>Harga</th>
                        <th>Warehouse-Qty</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th ></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('salesOrders.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
<script type="text/javascript" src="{{asset('js/passive.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script>
    $(".customer").on('change', function(){
      $.get('{{url("private/getAlamat")}}/'+$(this).val()+'/simpan', function(data){
        $('.alamat').html(data);
      });
    });
    @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
      $.get('{{url("private/getAlamat")}}/'+'{{$salesOrder->customer_id}}/{{$salesOrder->alamat_id}}', function(data){
        $('.alamat').html(data);
      });
    @endif
    $(function() {
      var rows_selected = [];
      var items_selected = [];
      var variant_selected = [];
      var lain = [];
      @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
        var ids = <?php echo json_encode($items)?>;
        var qty = <?php echo json_encode($qty)?>;
        var warehouse = <?php echo json_encode($warehouse)?>;
        var diskon = <?php echo json_encode($diskon)?>;
        var partial = <?php echo json_encode($partial)?>;
        var sales_id = <?php echo json_encode($id)?>;
      @endif
      var table = $('#items-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{!! route('datatables.itemsSales') !!}',
          // dom: '<ltip>',
          columns: [
              { data: function(data){
                  return '<input type="hidden" name="selected_items[]" class="item" value="'+data.item_id+'" disabled/><input disabled type="hidden" name="selected_variants[]" class="variant" value="'+data.id_variant+'"/><input type="hidden" name="roll[]" class="item" value="'+data.roll+'" class="roll" disabled/>';
              }, name: 'id'},
              { data: 'size', name: 'item_variant.size' , render:function (data) {
                if (data) {
                  return data;
                }else{
                  return '-';
                }
              }},
              { data: 'name', name: 'items.name' },
              { data: 'warna', name: 'item_variant.warna', render:function (data) {
                if (data) {
                  return data;
                }else{
                  return '-';
                }
              }},
              { data: 'pattern', name: 'item_variant.pattern', render:function (data) {
                if (data) {
                  return data;
                }else{
                  return '-';
                }
              }},
              { data: 'roll', name: 'item_variant.roll', render:function (data) {
                if (data) {
                  return data;
                }else{
                  return '-';
                }
              }},
              { data: 'price', name: 'item_variant.price', render : function (data){
                  return convertToRupiah(data);
              }},
              { data : 'action', name:'qty', orderable:false, searchable:false}
          ],rowCallback: function(row, data, index){
                // Get row ID
                var rowId = data.id;
                // If row ID is in the list of selected row IDs
                if($.inArray(rowId, rows_selected) !== -1){
                  $(row).find('input[type="checkbox"]').prop('checked', true);
                  $(row).addClass('selected');
                  if ($(row).hasClass('selected')) {
                    $(row).find('.qty').attr('disabled', false);
                    $(row).find('.diskon').attr('disabled', false);
                    $(row).find('.wh').attr('disabled', false);
                    $(row).find('.variant').attr('disabled', false);
                    $(row).find('.item').attr('disabled', false);
                    $(row).find('.roll').attr('disabled', false);
                  }
                  @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
                    for (var i = 0; i < ids.length; i++) {
                      $(row).find(".qty").val(qty[i]);
                      $(row).find(".wh").val(warehouse[i]);
                      $(row).find(".diskon").val(diskon[i]);
                    }
                    $(row).append("<input type='hidden' name='sales_id' value="+sales_id+">");
                  @endif
                }
            },fnInitComplete: function () {
              this.api().columns([1,2,3,4,5,6]).every(function () {
                  var column = this;
                  var input = document.createElement("input");
                  $(input).appendTo($(column.footer()).empty())
                  .on('change', function () {
                      column.search($(this).val(), false, false, true).draw();
                  });
              });
              @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
              for (var i = 0; i < ids.length; i++) {
                $("#items-table tr").each(function(row, data) {
                  if ($(this).find('.variant').val() ==ids[i]) {
                    $(this).find("td").trigger('click');
                    $(this).find(".qty").val(qty[i]);
                    $(this).find(".qty").click();
                    $(this).find(".diskon").val(diskon[i]);
                    $(this).find(".wh").val(warehouse[i]);
                    if(partial[i]){
                      $(this).find("#input").append("<input type='text' onkeypress='return event.charCode >= 48 && event.charCode <= 57' name='partial[]' placeholder='C. Roll' value="+partial[i]+" class='form-control partialInput' required>");
                    }
                    $(this).append("<input type='hidden' name='sales_id' value="+sales_id+">");
                  }
                }); 
              }
              @endif
          }
      });

      $('#items-table tbody').on('keyup', '.diskon', function(e){
        var $row = $(this).closest('tr');
        var price = parseInt($(this).attr('price'));
        addItem();
        if (this.value) {
          if (this.value.toString().length <= 3 && parseInt(this.value) != 0) {
            price = price - ((parseInt(this.value)/100) * price);
          }else{
            price = price - parseInt(this.value);
          }
          $row.find('td').eq(6).html(convertToRupiah(price));
        }
          $row.find('td').eq(6).html(convertToRupiah(price));
      });
      
      $('#items-table tbody').on('keyup', '.qty,.partialInput', function(e){
          addItem();
      });

      $('#items-table tbody').on('click', '.partial', function(e){
          var $row = $(this).closest('tr');
          var html = '<input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="partial[]" placeholder="C. Roll" class="form-control partialInput" required/>'
          if($row.find('.partialInput').length == 1){
            $row.find(".partialInput").remove();
          }else{
            $row.find('#input').append(html);
          }
      });

      $('#items-table tbody').on('click', 'td', function(e){
            var gr = this.cellIndex;
            var $row = $(this).closest('tr');
            $('.modal-body').html('');
            // Get row data
            var data = table.row($row).data();
            // Get row ID
            var rowId = data.id;
            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_selected);
            // If checkbox is checked and row ID is not in list of selected row IDs
            if(!$row.hasClass('selected') && index === -1){
              var pattern = (data.pattern !=null) ? data.pattern : '';
              var size = (data.size !=null) ? data.size : '';
              var warna = (data.warna !=null) ? data.warna : '';
              var roll = (data.roll !=null) ? data.roll : '';
              items_selected.push(data.name + '(' + pattern + ' ' + size + ' ' + warna + ' '+ roll + ')');
              rows_selected.push(rowId);
              variant_selected.push(data.id_variant);
              $('.modal-title').html('Item Dipilih (Total : '+rows_selected.length + ')');
            } else if (gr < 7 && index !== -1){
              rows_selected.splice(index,1);
              items_selected.splice(index,1);
              variant_selected.splice(index,1);
                $('.modal-title').html('Item Dipilih (Total : '+rows_selected.length + ')');
            }

            if(!$row.hasClass('selected')){
                $row.find('.qty').attr('disabled', false);
                $row.find('.wh').attr('disabled', false);
                $row.find('.variant').attr('disabled', false);
                $row.find('.diskon').attr('disabled', false);
                $row.find('.item').attr('disabled', false);
                $row.find('.roll').attr('disabled', false);
                $row.addClass('selected');
                $row.find('.qty').focus();
                $('#modal-default').modal({keyboard : false, backdrop : 'static', show :true }).draggable({ cursor :'move', handle: ".modal-header"});
                $('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');
            }else if (gr < 7) {
              $row.find('.qty').attr('disabled', true);
              $row.find('.partialInput').remove();
              $row.find('.qty').focus();
              $row.find('.wh').attr('disabled', true);
              $row.find('.variant').attr('disabled', true);
              $row.find('.roll').attr('disabled', true);
              $row.find('.item').attr('disabled', true);
              $row.find('.diskon').attr('disabled', true);
              $row.removeClass('selected');
            } 
            addItem();
            e.stopPropagation();
      });

      function addItem(argument) {
        lain.length = 0;
        localStorage.removeItem('item');
        $('.modal-body').html('');
        $("#items-table tr").each(function(row, data) {
          if ($(this).hasClass('selected')) {
            var isi = table.row($(this)).data();
            var pattern = (isi.pattern !=null) ? isi.pattern : '';
            var size = (isi.size !=null) ? isi.size : '';
            var warna = (isi.warna !=null) ? isi.warna : '';
            var roll = (isi.roll !=null) ? isi.roll : '';
            var cek = parseFloat($(this).find('option:selected').text().replace(/\D/g,''));
            var qty = ($(this).find('.qty').val() > cek ? $(this).find('.qty').val(''): $(this).find('.qty').val());
            var roll = parseFloat($(this).find('td').eq(5).text().replace(/\D/g,''));
            var partial = ($(this).find('.partialInput').val() >= roll ? $(this).find('.partialInput').val(''): $(this).find('.partialInput').val());
            var diskon = $(this).find('.diskon').val();
            var wh = $(this).find('option:selected').text().replace(/[^A-Za-z]/g, '');
            localStorage.setItem('item', isi.name + ' (' + pattern + ' ' + size + ' ' + warna + ' '+ roll + ')'  + '<br> Qty : ' +qty + ' - '+wh + ' | Diskon : '+ diskon + ' | Partial ' + ((typeof partial == 'undefined') ? '' : partial));
            lain.push(localStorage.getItem('item'));
          }
        });
        $('.modal-body').append(makeUL(lain, variant_selected));
      }

      function makeUL(array, array2) {
          var list = document.createElement('ul');

          for(var i = 0; i < array.length; i++) {
              var item = document.createElement('li');
              item.innerHTML = array[i];
              item.setAttribute('id', array2[i]);
              list.appendChild(item);
          }
          return list;
      }
  });
</script>
@stop