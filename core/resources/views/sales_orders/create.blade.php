@extends('layouts.app')
@section('title', 'Create Sales Order')
@section('content')
    <section class="content-header">
        <h1>
            Sales Order
             <div class="pull-right">
                <a type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-default">
                Munculkan Keranjang
                </a>
            </div>
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-danger">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'salesOrders.store']) !!}

                        @include('sales_orders.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
