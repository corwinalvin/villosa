<li class="{{ Request::is('private/home') ? 'active' : '' }}">
	<a href="{!!route('dashboard')!!}"><i class="ion ion-speedometer"></i> <span>Dashboard</span></a>
</li>
@permission('item')
<li class="{{ Request::is('private/items*') ? 'active' : '' }}">
    <a href="{!! route('items.index') !!}"><i class="fa fa-edit"></i><span>Items</span></a>
</li>
@endpermission
@permission('warehouse-transfer')
<li class="{{ Request::is('private/warehouseTransfers*') ? 'active' : '' }}">
    <a href="{!! route('warehouseTransfers.index') !!}"><i class="fa fa-edit"></i><span>Warehouse Transfers</span></a>
</li>
@endpermission
@permission('return-item')
<li class="{{ Request::is('private/returnItems*') ? 'active' : '' }}">
    <a href="{!! route('returnItems.index') !!}"><i class="fa fa-edit"></i><span>Return Items</span></a>
</li>
@endpermission
@permission('activity-logs')
<li class="{{ Request::is('private/activityLogs*') ? 'active' : '' }}">
    <a href="{!! route('activityLogs.index') !!}"><i class="fa fa-edit"></i><span>Activity Logs</span></a>
</li>
@endpermission
@permission('sales')
<li class="treeview {{ Request::is('private/sales*') ? 'active' : '' }}">
    <a role="button">
        <i class="ion ion-ios-cart"></i><span>Sales</span>
        <i class="ion ion-arrow-down-b pull-right"></i>
    </a>
    <ul class="treeview-menu">
		@permission('sales-order')
		<li class="{{ Request::is('private/sales/salesOrders*') ? 'active' : '' }}">
		    <a href="{!! route('salesOrders.index') !!}"><i class="fa fa-edit"></i><span>Sales Orders</span></a>
		</li>
		@endpermission
		<!-- @permission('purchase-order')
		<li class="{{ Request::is('private/sales/purchaseOrders*') ? 'active' : '' }}">
		    <a href="{!! route('purchaseOrders.index') !!}"><i class="fa fa-edit"></i><span>Purchase Orders</span></a>
		</li>
		@endpermission -->
		@permission('invoice')
		<li class="{{ Request::is('private/sales/invoices*') ? 'active' : '' }}">
		    <a href="{!! route('invoices.index') !!}"><i class="fa fa-edit"></i><span>Invoices</span></a>
		</li>
		@endpermission
		@permission('shipment')
		<li class="{{ Request::is('private/sales/shipments*') ? 'active' : '' }}">
		    <a href="{!! route('shipments.index') !!}"><i class="fa fa-edit"></i><span>Shipments</span></a>
		</li>
		@endpermission
	</ul>
</li>
@endpermission
@permission('reports')
<li class="treeview {{ Request::is('private/report*') ? 'active' : '' }}">
    <a role="button">
        <i class="glyphicon glyphicon-stats"></i><span>Report</span>
        <i class="ion ion-arrow-down-b pull-right"></i>
    </a>
    <ul class="treeview-menu">
		<!-- @permission('report-purchase')
        <li class="{{ Request::is('private/report/purchase*') ? 'active' : '' }}">
            <a href="{!! route('report.purchase') !!}"><i class="ion ion-ios-circle-outline"></i><span>Purchase</span></a>
        </li>
		@endpermission -->
		@permission('report-sales')
        <li class="{{ Request::is('private/report/sales*') ? 'active' : '' }}">
            <a href="{!! route('report.sales') !!}"><i class="ion ion-ios-circle-outline"></i><span>Sales</span></a>
        </li>
		@endpermission
		@permission('report-stock')
        <li class="{{ Request::is('private/report/warehouses*') ? 'active' : '' }}">
            <a href="{!! route('report.warehouses') !!}"><i class="ion ion-ios-circle-outline"></i><span>Stock</span></a>
        </li>        
		@endpermission
    </ul>
</li>
@endpermission
@permission('settings')
<li class="treeview {{ Request::is('private/setting*') ? 'active' : '' }}" >
    <a role="button">
        <i class="ion ion-gear-b"></i><span>Settings</span>
        <i class="ion ion-arrow-down-b pull-right"></i>
    </a>
    <ul class="treeview-menu">
		@permission('users')
		<li class="{{ Request::is('private/setting/users*') ? 'active' : '' }}">
		    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
		</li>
		@endpermission
		@permission('warehouses')
		<li class="{{ Request::is('private/setting/warehouses*') ? 'active' : '' }}">
		    <a href="{!! route('warehouses.index') !!}"><i class="fa fa-edit"></i><span>Warehouses</span></a>
		</li>
		@endpermission
		@permission('brands')
		<li class="{{ Request::is('private/setting/brands*') ? 'active' : '' }}">
		    <a href="{!! route('brands.index') !!}"><i class="fa fa-edit"></i><span>Brands</span></a>
		</li>
		@endpermission
		@permission('categories')
		<li class="{{ Request::is('private/setting/categories*') ? 'active' : '' }}">
		    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
		</li>
		@endpermission
		@permission('sales-type')
		<li class="{{ Request::is('private/setting/salesType*') ? 'active' : '' }}">
		    <a href="{!! route('salesType.index') !!}"><i class="fa fa-edit"></i><span>Sales</span></a>
		</li>
		@endpermission
		@permission('suppliers')
		<li class="{{ Request::is('private/setting/suplliers*') ? 'active' : '' }}">
		    <a href="{!! route('suplliers.index') !!}"><i class="fa fa-edit"></i><span>Suppliers</span></a>
		</li>
		@endpermission
		@permission('customers')
		<li class="{{ Request::is('private/setting/customers*') ? 'active' : '' }}">
		    <a href="{!! route('customers.index') !!}"><i class="fa fa-edit"></i><span>Customers</span></a>
		</li>
		@endpermission
		@permission('payment-methods')
		<li class="{{ Request::is('private/setting/paymentMethods*') ? 'active' : '' }}">
		    <a href="{!! route('paymentMethods.index') !!}"><i class="fa fa-edit"></i><span>Payment Methods</span></a>
		</li>
		@endpermission
		@permission('taxes')
		<li class="{{ Request::is('private/setting/taxes*') ? 'active' : '' }}">
		    <a href="{!! route('taxes.index') !!}"><i class="fa fa-edit"></i><span>Taxes</span></a>
		</li>
		@endpermission
		@permission('permissions')
		<li class="{{ Request::is('private/setting/permissions*') ? 'active' : '' }}">
		    <a href="{!! route('permissions.index') !!}"><i class="fa fa-edit"></i><span>Permissions</span></a>
		</li>
		@endpermission
		@permission('roles')
		<li class="{{ Request::is('private/setting/roles*') ? 'active' : '' }}">
		    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
		</li>
		@endpermission
		@permission('departments')
		<li class="{{ Request::is('private/setting/departments*') ? 'active' : '' }}">
			<a href="{!! route('departments.index') !!}"><i class="fa fa-edit"></i><span>Departments</span></a>
		</li>
		@endrole
	</ul>
<!-- 		<li class="{{ Request::is('private/setting/satuans*') ? 'active' : '' }}">
		    <a href="{!! route('satuans.index') !!}"><i class="fa fa-edit"></i><span>Satuans</span></a>
		</li> -->
</li>
@endpermission