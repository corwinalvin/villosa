@section('css')
<style>
#items-table tfoot tr th input, .qty {
    width: 80px;
}
#modal-default {
  position: relative;
}
.modal-dialog {
  position: fixed;
  overflow-y: initial !important;
  width: 23%;
  margin: 0;
  padding: 10px;
}
.modal{
  background: transparent !important;
}
.modal-backdrop{
  display: none !important
}
.modal-backdrop{
  display: none !important
}
body.modal-open{
  overflow: visible;
   /*this line allowed form to be scrollable */
}
.modal-body{ 
 /*this section allowed form input-fields to be clickable */
  max-height: calc(70vh - 210px);
  overflow-y: auto;
  padding: 5px !important;
}
button .fa{
  font-size: 12px;
  margin-left: 10px;
}
</style>
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal"> <i class='fa fa-times'></i> </button>
        <h4 class="modal-title">Item Dipilih</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Kosong</li>
        </ul>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@stop
<!-- Reference Field -->
<div class="form-group col-sm-6">
 {!! Form::hidden('so_id', $shipment->so_id) !!}
    {!! Form::label('reference', 'Reference:') !!}
    {!! Form::text('reference', $kode, ['class' => 'form-control', 'readonly' => true]) !!}
</div>
<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    <input type="text" value="{{Auth::User()->name}}" class="form-control" readonly="true">
    {!! Form::hidden('created_by', Auth::user()->id) !!}
</div>
<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Shipment Date:') !!}
    {!! Form::text('date', date('Y-m-d'), ['class' => 'form-control', 'id' => 'datepicker']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', [0 => 'Kirim Nanti', 1 => 'Kirim Sekarang'], null, ['class' => 'form-control']) !!}
</div>
<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>
<div class="col-lg-12 ">
    <p class="help-block">*) Mohon isi kuantiti <strong>TIDAK LEBIH</strong> dari stok <i><u>QUANTITY</u></i> yang dipilih</p>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table " id="items-table">
                <thead>
                    <tr>
                        <th width="1"></th>
                        <th>Name</th>
                        <th>Warna</th>
                        <th>Pattern</th>
                        <th width="1">Size</th>
                        <th>Roll</th>
                        <th>Harga</th>
                        <th>Warehouse-Qty</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th ></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>

        @section('scripts')
        <script>

        $(function() {
            var rows_selected = [];
            var items_selected = [];
            var variant_selected = [];
            var lain = [];
             @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
              var ids = <?php echo json_encode($items)?>;
              var qty = <?php echo json_encode($qty)?>;
              var warehouse = <?php echo json_encode($warehouse)?>;
            @endif
            var table = $('#items-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatables.itemSO', $shipment->so_id) !!}',
                // dom: '<ltip>',
                columns: [
                    { data: function(data){
                       return '<input type="hidden" name="selected_items[]" class="item" value="'+data.item_id+'" disabled/><input type="hidden" name="warehouse[]" class="wh" value="'+data.warehouse+'" disabled/><input disabled type="hidden" name="selected_variants[]" class="variant" value="'+data.id_variant+'"/>';
                    }, name: 'id'},
                    { data: 'name', name: 'items.name' },
                    { data: 'warna', name: 'item_variant.warna', render:function (data) {
                      if (data) {
                        return data;
                      }else{
                        return '-';
                      }
                    }},
                    { data: 'pattern', name: 'item_variant.pattern', render:function (data) {
                      if (data) {
                        return data;
                      }else{
                        return '-';
                      }
                    }},
                    { data: 'size', name: 'item_variant.size' , render:function (data) {
                      if (data) {
                        return data;
                      }else{
                        return '-';
                      }
                    }},
                    { data: 'roll', name: 'item_variant.roll', render:function (data, row, metaData) {
                      if (data) {
                        var partial = ''
                        if(metaData.partial){
                          partial = '- ('+metaData.partial+')'
                        }
                        return data + ' ' + partial;
                      }else{
                        return '-';
                      }
                    }},
                    { data: 'price', name: 'item_variant.price', render : function (data){
                        return convertToRupiah(data);
                    }},
                    { data : 'action', name:'qty', orderable:false, searchable:false}
                ],rowCallback: function(row, data, index){
                     // Get row ID
                     var rowId = data.id;
                     // If row ID is in the list of selected row IDs
                     if($.inArray(rowId, rows_selected) !== -1){
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                        @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
                          $(row).find('.qty').attr('disabled', false);
                          $(row).find('.variant').attr('disabled', false);
                          for (var i = 0; i < ids.length; i++) {
                            $(row).find(".qty").val(qty[i]);
                            $(row).find(".wh").val(warehouse[i]);
                          }
                        @endif
                     }
                  },fnInitComplete: function () {
                    this.api().columns([1,2,3,4,5,6]).every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                    });
                    $("#items-table tr").each(function(row, data) {
                      $(this).find(".qty").val($(this).find('td').eq(7).text().replace(/\D/g,''));
                      $(this).find("td").trigger('click');
                      $(this).find(".qty").click();
                    }); 
                    @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
                    for (var i = 0; i < ids.length; i++) {
                      $("#items-table tr").each(function(row, data) {
                        if ($(this).find('.variant').val()==ids[i]) {
                          $(this).find(".qty").click();
                          $(this).find("td").trigger('click');
                          $(this).find(".qty").val(qty[i]);
                          $(this).find(".wh").val(warehouse[i]);
                        }
                      }); 
                    }
                    @endif
                }
            });

            $('#items-table tbody').on('keyup', '.qty', function(e){
               addItem();
            });

              $('#items-table tbody').on('click', 'td', function(e){
                  var gr = this.cellIndex;
                  var $row = $(this).closest('tr');
                  $('.modal-body').html('');
                  // Get row data
                  var data = table.row($row).data();
                  // Get row ID
                  var rowId = data.id;
                  // Determine whether row ID is in the list of selected row IDs
                  var index = $.inArray(rowId, rows_selected);
                  // If checkbox is checked and row ID is not in list of selected row IDs
                  if(!$row.hasClass('selected') && index === -1){
                    var pattern = (data.pattern !=null) ? data.pattern : '';
                    var size = (data.size !=null) ? data.size : '';
                    var warna = (data.warna !=null) ? data.warna : '';
                    var roll = (data.roll !=null) ? data.roll : '';
                    items_selected.push(data.name + '(' + pattern + ' ' + size + ' ' + warna + ' '+ roll + ')');
                    rows_selected.push(rowId);
                    variant_selected.push(data.id_variant);
                    $('.modal-title').html('Item Dipilih (Total : '+rows_selected.length + ')');
                  } else if (gr < 7 && index !== -1){
                    rows_selected.splice(index,1);
                    items_selected.splice(index,1);
                    variant_selected.splice(index,1);
                     $('.modal-title').html('Item Dipilih (Total : '+rows_selected.length + ')');
                  }

                  if(!$row.hasClass('selected')){
                     $row.find('.qty').attr('disabled', false);
                     $row.find('.variant').attr('disabled', false);
                     $row.find('.item').attr('disabled', false);
                     $row.find('.wh').attr('disabled', false);
                     $row.addClass('selected');
                     $row.find('.qty').focus();
                     $('#modal-default').modal({keyboard : false, backdrop : 'static', show :true }).draggable({ cursor :'move', handle: ".modal-header"});
                     $('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');
                  }else if (gr < 7) {
                    $row.find('.qty').attr('disabled', true);
                    $row.find('.qty').focus();
                    $row.find('.variant').attr('disabled', true);
                    $row.find('.item').attr('disabled', true);
                    $row.find('.wh').attr('disabled', true);
                    $row.removeClass('selected');
                 } 
                     // localStorage.setItem('wh', 'Warehouse:'+ $row.find('option:selected').text().replace(/[^A-Za-z]/g, ''));
                    addItem();
                  // Prevent click event from propagating to parent
                  e.stopPropagation();
            });
            function addItem(argument) {
              lain.length = 0;
              localStorage.removeItem('item');
              $('.modal-body').html('');
              $("#items-table tr").each(function(row, data) {
                if ($(this).hasClass('selected')) {
                  var isi = table.row($(this)).data();
                  var pattern = (isi.pattern !=null) ? isi.pattern : '';
                  var size = (isi.size !=null) ? isi.size : '';
                  var warna = (isi.warna !=null) ? isi.warna : '';
                  var roll = (isi.roll !=null) ? isi.roll : '';
                  var cek = parseFloat($(this).find('td').eq(7).text().replace(/\D/g,''));
                  var qty = ($(this).find('.qty').val() > cek ? $(this).find('.qty').val(''): cek);
                  var qty = $(this).find('.qty').val();
                  var wh = $(this).find('td').eq(7).text().replace(/[^A-Za-z]/g, '');
                  localStorage.setItem('item', isi.name + ' (' + warna + ' ' + pattern + ' ' + size + ' '+ roll + ')'  + '<br> Qty : ' +qty + ' - '+wh );
                  lain.push(localStorage.getItem('item'));
                }
              });
              $('.modal-body').append(makeUL(lain));
            }

           function makeUL(array) {
                // Create the list element:
                var list = document.createElement('ul');

                for(var i = 0; i < array.length; i++) {
                    // Create the list item:
                    var item = document.createElement('li');

                    // Set its contents:
                    item.innerHTML = array[i];
                    // Add it to the list:
                    list.appendChild(item);
                }

                // Finally, return the constructed list:
                return list;
            }
        });
        </script>
        @stop
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('shipments.index') !!}" class="btn btn-default">Cancel</a>
</div>
