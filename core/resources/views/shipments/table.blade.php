<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">READY TO SEND</a></li>
      <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" onclick="shipped();">SHIPPED SHIPMENT</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="table-responsive">
          <?php echo $dataTable->table(['class' => 'table']); ?>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
          <div class="table-responsive">
             <table class="table" id="shipments-approved-table">
                  <thead>
                      <tr>
                      <th>Sales Order Ref.</th>
                      <th>Reference</th>
                      <th>Status</th>
                      <th>Created By</th>
                      <th>Action</th>
                      </tr>
                  </thead>
              </table>            
          </div>
      </div>
      <!-- /.tab-pane -->
      <!-- /.tab-pane -->
    </div>
<!-- /.tab-content -->
</div>
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
{!! $dataTable->scripts() !!}
<script>
function shipped() {
    if (!$.fn.dataTable.isDataTable('#shipments-approved-table')) {
        $('#shipments-approved-table').DataTable({
            processing: true,
            serverSide: true,
            dom : 'Blfrtip',
            buttons: [
                'pdfHtml5', 'excelHtml5'
            ],
            ajax: '{!! route('datatables.shipmentApproved') !!}',
            columns: [
                { data: 'sales_order.reference', name: 'salesOrder.reference' },
                { data: 'sr',name: 'sr' },
                { data: 'status', name: 'status', render: function (data) {
                    if(data==0){
                        return '<div style="cursor:pointer !important;border: 1px solid red !important;color: red !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;">UNAPPROVED</div>';
                    }else{
                            return '<div style="border: 1px solid #3c8dbc !important;color: #3c8dbc !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;">APPROVED</div>';
                    }
                } },
                { data : 'created_by.name' ,name:'createdBy.name'},            
                { data : 'action' ,name:'action'}
            ]
        });
    }
}
</script>
@stop