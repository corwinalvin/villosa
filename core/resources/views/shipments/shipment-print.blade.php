@php
	$data_item = array_chunk($shipment->detailShipment->toArray(), 13);
	$BAB = $shipment->salesOrder->detailOrders;
@endphp

<!DOCTYPE html>
<html>
<head>
	<title>Shipment {{$shipment->reference}}</title>
</head>
<style>

html, body {
    font-size: 11px;
	line-height: 150%;
}

@page {
  size: 21.59cm 19.97cm;
}

.logo {
  width: 30%;
}

.borderless td, .borderless th {
    border: none !important;
}
.s {
	 border: none !important;
	 background:white !important;
}
.bordertable{
	border-left: 1px solid #000000;
	border-bottom: 1px solid #000000;
	border-top: 1px solid #000000;
	border-right: 1px solid #000000;
}

.borderth{
	border-bottom: 1px solid #000000;
	/*border-right: 1px solid #000000;*/
}
.bordertd{
	border-right: 1px solid #000000;
}
.classtr{
	size: 21.59cm 13.97cm;
}
tr.spaceUnder > td
{
  padding-bottom: -0.5em;
}
.break {
	page-break-after: always
}

</style>
<body onload="" style="font-family: Tahoma,Arial, sans-serif;" >
	
	@for($i = 0; $i < count($data_item); $i++) 
		<div>
			<table>
				<tr>
					<td valign="top" class="" align="center">Kepada</td>
					<td class="" style=" width: 290px;" valign="top" colspan="8">
						<strong>{{ strtoupper($shipment->salesOrder->buyer->name) }}</strong><br>
						{{ $shipment->salesOrder->detailAlamat->warehouse }} <br>
						{{$shipment->salesOrder->buyer->city}} {{$shipment->salesOrder->buyer->country}}
					</td>
					<td align="right" style="font-size: 27px;border-bottom: 1px solid #303030;" colspan="8">DELIVERY ORDER</td>
				</tr>
				<tr>
					<td colspan="16"></td>
				</tr>
				<tr>
					<td colspan="10"></td>
					<td align="right" colspan="5">Nomor Surat Jalan :</td>
					<td align="left">{{ $shipment->reference }}</td>
				</tr>
				<tr>
					<td colspan="10"></td>
					<td align="right" colspan="5">Tanggal Surat Jalan :</td>
					<td align="left">{{ date('d M Y', strtotime($shipment->date)) }}</td>
				</tr>
				<tr>
					<td colspan="10"></td>
					<td align="right" colspan="5">Kirim Via : </td>
					<td align="left"></td>
				</tr>
			</table>
		</div>

		<div style="height: 280px;">
			<table id="salesInvoice">
				<tr>
					<td align="left" style="width: 12px;" class="borderth">No.</td>
					<td align="left" style="width: 120px;" class="borderth">Kode</td>
					<td align="left" style="width: 450px;" class="borderth">Nama Barang</td>
					<td align="left" style="width: 100px;" class="borderth">Qty</td>
				</tr>
				<?php $a = 0; $b = -1;?>
				@foreach($data_item[$i] as $item)
				@php 
					$itemR = (object) $item['items'];
					$itemV = (object) $item['item_variant'];
				@endphp
				<?php $a++;$b++; ?>
				<tr class="spaceUnder">
					<td>{{$a}}.</td>
					<td align="left"> {{$itemV->size}}</td>
					<td style="width: 450px;">{{ $itemR->name }} (
					{{$itemV->warna}} {{(isset($itemV->pattern)) ? ',' : '' }} {{$itemV->pattern}}{{(isset($itemV->size) && $itemV->size != "") ? ',' : '' }} {{$itemV->size}}{{(isset($itemV->roll)) ? ',' : '' }} {{$itemV->roll}})  {{($BAB[$b]->partial != null) ? '- '.$BAB[$b]->partial : ''}}</td>
					<td align="left">{{$item['qty']}} PCS</td>
				</tr>
				@endforeach
			</table>
			<br> 
		</div>
		<div style="page-break-inside: avoid;">
			<table >
				<tr>
					<td style="width: 50px;" align="center">Supir</td>
					<td style="width: 100px;" align="center">Pengirim</td>
					<td style="width: 100px;" align="center">Penerima</td>
					<td style="width: 100px;" align="center">Hormat Kami,</td>
					<td>Personal Gudang</td>
					<td></td>
					<td></td>
					<td valign="top" style="border-radius: 2px; border-left:1px solid black; border-top:1px solid black">Catatan</td>
					<td colspan="2" class="bordertable" style="border-radius: 3px">Sales : {{$shipment->createdBy->name}}</td>
				</tr>
				<tr>
					<td colspan="7"><p></p></td>
					<td valign="top" rowspan="2" colspan="3" style="border-right: 1px solid black;border-left: 1px solid black;border-top: 1px solid black;border-bottom: 1px solid black;">
						<p>{{ $shipment->note }}</p>
					</td>
				</tr>
				<tr>
					<td style="height: 55px"> </td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><hr></td>
					<td align="center"><hr></td>
					<td align="center"><hr></td>
					<td align="center"><hr></td>
					<td align="center"><hr></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td style="height: 25px"></td>
					<td colspan="2"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="break"></div>
		<br>

	@endfor

</body>
</html>
