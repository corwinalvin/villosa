@extends('layouts.app')
@section('title', 'Shipment Create')
@section('content')
    <section class="content-header">
        <h1>
            Shipment
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-danger">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'shipments.store']) !!}

                        @include('shipments.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
