<table class="table table-responsive" id="departments-table">
    <thead>
        <tr>
        <th>Name</th>
        <th>Created By</th>
        <th>Created At</th>
        <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($departments as $department)
        <tr>
            <td>{!! $department->name !!}</td>
            <td>{!! $department->createdBy->name !!}</td>
            <td>{!! $department->created_at !!}</td>
            <td>
                {!! Form::open(['route' => ['departments.destroy', $department->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('departments.show', [$department->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('departments.edit', [$department->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>