@extends('layouts.app')
@section('title')
{{$item->name}}
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Item
            <div class="pull-right">
                <a href="{{route('getSku', ['id' => $item->id])}}" class="btn btn-warning" >
                    Download SKU Label
                </a>
            </div>
        </h1>
    </section>
    <div class="content">
        <div class="box box-danger">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('items.show_fields')
                </div>
            </div>
        </div>
    </div>
@endsection
