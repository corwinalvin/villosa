
<!-- Sku Field -->
@if(strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
<div class="form-group col-sm-12">
    {!! Form::label('sku', 'File:') !!}
    {!! Form::file('file', ['class' => 'form-control', 'style' => 'width:100% !important;', 'required' => true]) !!}
</div>
@else

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('desc', 'Desc:') !!}
    {!! Form::textarea('desc', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12">
<h4>Variant Items</h4>
    <table class="table table-hover" id="items-table">
        <thead>
            <tr>
                <th>Harga</th>
                <th>Warna</th>
                <th>Pattern</th>
                <th>Size</th>
                <th>Roll</th>
                <th>Pictures</th>
                <th>Warehouse(Qty)</th>
                <th>Edit</th>
            </tr>
        </thead>
    </table>
</div>
@section('scripts')
<script>
$(function() {
    $('#orders').DataTable();
    $('#items-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.itemVariant', ["id" => $item->id, "jenis"=>"edit"]) !!}',
        columnDefs : [
            {
                targets : 5, 
                render : function(data, row, index){
                    var a = "";
                    for (i in jQuery.parseJSON(data.replace(/&quot;/g,'"'))) {
                        a +='<a href='+jQuery.parseJSON(data.replace(/&quot;/g,'"'))[i]+' data-lightbox="image-1"><img onerror=this.src="https://goo.gl/7DScCF"; src='+jQuery.parseJSON(data.replace(/&quot;/g,'"'))[i]+' width=50 /></a>';
                    }
                    return a;
                },
            }
        ],
        columns: [
            { data: 'price', name: 'item_variant.price', render : function(data){
                return convertToRupiah(data);
            }},
            { data: 'warna', name: 'item_variant.warna', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            }},
            { data: 'pattern', name: 'item_variant.pattern', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            }},
            { data: 'size',name: 'item_variant.size', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            } },
            { data: 'roll', name: 'item_variant.roll', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            } },
            { data: 'picture', name: 'item_variant.picture' },
            { data: 'action', name: 'action' },
            { data: 'edit', name: 'edit' }
        ]
    });
});
</script>
@stop
@endif
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('items.index') !!}" class="btn btn-default">Cancel</a>
</div>

