<table class="table table-responsive" id="items-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Category</th>
            <th>Brand</th>
            <th>Variant(s)</th>
            <th>Total Stock</th>
            <th>Harga Tertinggi</th>
            <th>Harga Terendah</th>
            <th>Created By</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script>
$(function() {
    $('#items-table').DataTable({
        processing: true,
        serverSide: true,
        dom : 'Blfrtip',
        buttons : ['pdfHtml5', 'excelHtml5'],
        ajax: '{!! route('datatables.items') !!}',
        columns: [
            { data: 'name', name: 'items.name'},
            { data: 'categories.name',name: 'categories.name' },
            { data: 'brands.brand_name', name: 'brands.brand_name' },
            { data: 'variant', render:function(data, type, row){
                var good = decodeHTMLEntities(data.replace(/[0-9]/g, '')).replace(/\W/g, '');
                
                if(good == 'cpsr'){
                    return 'Warna, Pattern, Size, Roll';
                }else if(good = 'cps'){
                    return 'Warna, Pattern, Size';
                }else if(good = 'cp'){
                    return 'Warna, Pattern';
                }else{
                    return 'Warna';
                }
            } , name: 'items.variant' },
            { data: 'custom_variant[0].total', name: 'customVariant[0].total', render : function(data){
                return humanNumber(data)
            }  },
            { data: 'custom_variant[0].maxPrice', name: 'customVariant[0].maxPrice', render : function(data){
                return convertToRupiah(data)
            } },
            { data: 'custom_variant[0].minPrice', name: 'customVariant[0].minPrice', render : function(data){
                return convertToRupiah(data)
            } },
            { data: 'created_by.name', name: 'createdBy.name' },
            { data : 'action' ,name:'action'}
        ]
    });
});
</script>
@stop