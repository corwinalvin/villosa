<!-- Name Field -->
<div class="form-group  col-sm-3">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $item->name !!}</p>
</div>

<!-- Category Field -->
<div class="form-group col-sm-3">
    {!! Form::label('category', 'Category:') !!}
    <p>{!! $item->categories->name !!}</p>
</div>

<!-- Brand Field -->
<div class="form-group col-sm-3">
    {!! Form::label('brand', 'Brand:') !!}
    <p>{!! $item->brands->brand_name !!}</p>
</div>

<!-- Desc Field -->
<div class="form-group col-sm-3">
    {!! Form::label('desc', 'Desc:') !!}
    <p>{!! $item->desc !!}</p>
</div>
<div class="form-group col-sm-3">
    {!! Form::label('desc', 'Status:') !!}
    <p>{!! getStatusItem($item->status) !!}</p>
</div>

<!-- Variant Field -->
<div class="form-group  col-sm-3">
    {!! Form::label('variant', 'Variant:') !!}
    <p>{!! getVariant(preg_replace("/[^A-Za-z0-9 ]/", '',preg_replace("/[0-9]+/","",$item->variant))) !!}</p>
</div>
<div class="form-group  col-sm-3">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $item->createdBy->name !!}</p>
</div>
<div class="form-group col-sm-12" >
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Order Item</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Histori Stok</a></li>
            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">Variant Items</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <h4>Histori Penjualan</h4>
                <table class="table" id="orders">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Variant</th>
                            <th>Reference</th>
                            <th>Base Harga</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td width="10">{{date('d/m/Y', strtotime($order->date))}}</td>
                            <td>{{$order->buyer->name}} </td>
                            <td>{{$order->warna}} {{$order->pattern}} {{$order->size}} {{$order->roll}}  {{($order->partial !== null) ? '-'.$order->partial : ''}}</td>
                            <td><a href="{{route('salesOrders.show', $order->id)}}">{{$order->reference}}</a></td>
                            <td>{{rupiah($order->price)}}</td>
                            <td>{{$order->qty}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab_2">
                <h4>Histori Stok</h4>
                <table class="table" id="stock-adjuments">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Item Variant</th>
                            <th>Updates</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sa as $sas)
                        <tr>
                            <?php $a = json_decode($sas->properties); ?>
                        <td>{{ date('d-m-Y', strtotime($sas->created_at))}}</td>
                        <td>{{$a->attributes->warna}} {{$a->attributes->size}} {{$a->attributes->pattern}} {{$a->attributes->roll}}</td>
                        <td>Updated :  {{$a->attributes->value}} | Sebelumnya : {{$a->old->value}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab_3">
                <h4>Variant Items</h4>
                <table class="table table-hover" id="items-table" style="width:100% !important;">
                    <thead>
                        <tr>
                            <th>SKU</th>
                            <th>Harga</th>
                            <th>Warna</th>
                            <th>Pattern</th>
                            <th>Roll</th>
                            <th>Pictures</th>
                            <th>Warehouse(Qty)</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    <!-- /.tab-content -->
    </div>
</div>

<!-- Created By Field -->

@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script>
$(function() {
    $('#orders').DataTable({
        dom : 'Blfrtip',
        buttons: ['pdfHtml5', 'excelHtml5']
    });
    $('#stock-adjuments').DataTable({
        dom : 'Blfrtip',
        buttons: ['pdfHtml5', 'excelHtml5']
    });
    $('#items-table').DataTable({
        processing: true,
        serverSide: true,
        dom : 'Blfrtip',
        buttons: ['pdfHtml5', 'excelHtml5'],
        ajax: '{!! route('datatables.itemVariant', ["id" => $item->id, "jenis"=>"look"]) !!}',
        columnDefs : [
            {
                targets : 5, 
                render : function(data, row, index){
                    var a = "";
                    for (i in jQuery.parseJSON(data.replace(/&quot;/g,'"'))) {
                        a +='<a href='+jQuery.parseJSON(data.replace(/&quot;/g,'"'))[i]+' data-lightbox="image-1"><img onerror=this.src="https://goo.gl/7DScCF"; src='+jQuery.parseJSON(data.replace(/&quot;/g,'"'))[i]+' width=50 /></a>';
                    }
                    return a;
                },
            }
        ],
        columns: [
            { data: 'size',name: 'item_variant.size', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            } },
            { data: 'price', name: 'item_variant.price', render : function(data){
                return convertToRupiah(data);
            }},
            { data: 'warna', name: 'item_variant.warna', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            }},
            { data: 'pattern', name: 'item_variant.pattern', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            }},
            { data: 'roll', name: 'item_variant.roll', render: function (data) {
                if (data) {
                    return data;
                }else{
                    return '-';
                }
            } },
            { data: 'picture', name: 'item_variant.picture' },
            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@stop