@extends('layouts.app')
@section('title', 'Items')
@section('content')
    <section class="content-header">
        <h1>
            Item
            <div class="pull-right">
                <a type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-info">
                Panduan Pengisian Template
                </a>
                <a class="btn btn-success" href="{{route('downloadCSVex')}}" >Download CSV</a>
            </div>
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-danger">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'items.store', 'files' => 'true']) !!}

                        @include('items.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-default fade" id="modal-info">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Panduan Pengisian Template</h4>
              </div>
              <div class="modal-body">
                <p class="help-block">*) Tidak ada penambahan stok terjadi jika memiliki barang yang sama</p>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>NAMA KOLOM</th>
                            <th align="center">CARA PENGISIAN KOLOM</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <tr>
                            <td>SKU</td>
                            <td>
                                <ul>
                                    <li>Pastikan SKU <strong>berbeda</strong> dengan barang lainnya di lapakmu.</li>
                                    <li>JANGAN PERNAH MERUBAH NOMOR SKU</li>
                                </ul>
                            </td>     
                        </tr>     -->    
                        <tr>
                            <td>Nama Barang</td>
                            <td>
                                Pastikan nama barang <strong>berbeda</strong> dengan barang lainnya di lapakmu.
                            </td>
                        </tr>
                        <tr>
                            <td>Merek</td>
                            <td>Pastikan Anda menulis 1 Merek yang <strong>tersedia</strong> di <u>Menu Setting > Brand</u></td>
                        </tr>
                         <tr>
                            <td>Tipe</td>
                            <td>Pastikan Anda menulis 1 Kategori yang <strong>tersedia</strong> di <u>Menu Setting > Categories</u></td>
                        </tr>
                        <tr>
                            <td>Varian - (Size/Warna/Pattern/Roll)</td>
                            <td>
                                <ul>
                                    <li>Isi Variant sesuai dengan urutannya yaitu Size/Warna/Pattern/Roll </li>
                                    <li>Jika memiliki variant yang kosong, kosongkan bagian varian tersebut <p>Ex : XL/Merah//190 (Tidak Memiliki Pattern) atau //Batik/140 (Tidak memiliki Size dan Warna)</p></li>
                                    <li>Hilangkan garis miring jika tidak ada lagi variant lain <p>Ex : M/Biru (Jangan : M/Biru/)</p> </li>
                                    <li>Jika <strong>hanya</strong> memiliki varian Size tambahkan garis miring diakhir <p>Ex : XL/ (Jangan : XL) </p> </li>
                                    <li>Hilangkan varian data yang sudah pernah dimasukkan jika ingin menambah Varian Baru</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Warehouse</td>
                            <td>
                                <ul>
                                    <li>Tulis nama warehouse dimana barang berada dengan <u>lengkap</u>, tidak masalah besar atau kecil </li>
                                    <li>Pemisah antar nama warehouse adalah <strong>|</strong></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Stok</td>
                            <td>
                                <ul>
                                    <li>Sesuaikan jumlah stok dengan urutan warehouse di kolom Warehouse</li>
                                    <li>Pemisah antar jumlah stok adalah <strong>|</strong></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td>Masukkan harga tanpa koma dan tanpa Rp - <strong>HANYA ANGKA SAJA</strong></td>
                        </tr>
                        <tr>
                            <td>URL Gambar 1-5</td>
                            <td>Isi dengan url gambar yang sudah disediakan</td>
                        </tr>
                    </tbody>
                </table>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
@endsection
