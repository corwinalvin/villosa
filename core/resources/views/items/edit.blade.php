@extends('layouts.app')
@section('title', 'Item')
@section('content')
    <section class="content-header">
        <h1>
            Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-danger">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($item, ['route' => ['items.update', $item->id], 'method' => 'patch']) !!}

                        @include('items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection