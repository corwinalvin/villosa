@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1>
    Sales
    <small>Report Sales Order</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sales Order Detail</li>
    </ol>
</section>
<div class="content">
    <div class="clearfix"></div>
    <div class="box box-danger">
        <div class="box-body">
            <table class="table table-responsive" id="sales-table">
                <thead>
                    <tr>
                        <th>Reference</th>
                        <th>Customer</th>
                        <th>Payment</th>
                        <th>Sales Type</th>
                        <th>Amount</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@stop
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript">
$(function() {
    $('#sales-table').DataTable({
        processing: true,
        serverSide: true,
        dom : 'Blfrtip',
        buttons: [
            'pdfHtml5', 'excelHtml5'
        ],
        ajax: '{!! route('datatables.salesReportDetail', $value) !!}',
        columns: [
            { data: 'action', name: 'action'},
            { data: 'buyer.name',name: 'buyer.name' },
            { data: 'payment.name',name: 'payment.name' },
            { data: 'sales_type.name',name: 'sales_type.name'},
            { data: 'amount',name: 'amount',render : function(data){
                return convertToRupiah(data);
            }}
        ]
    });
});
</script>
@stop