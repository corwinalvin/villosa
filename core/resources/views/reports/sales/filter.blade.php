@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1>
    Sales
    <small>Report Sales Order</small>
    <div class="pull-right">
            <a href="#" class="btn btn-success filter" data-toggle="control-sidebar">Filter</a>
        </div>
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>
    <div class="box box-danger">
        <div class="box-body">
            <table class="table table-responsive" id="sales-table">
                <thead>
                    <tr>
                        <th>Reference</th>
                        <th>Customer</th>
                        <th>Payment</th>
                        <th>Sales Type</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hasil as $value)
                    <tr>
                        <td><a href="{{$value->id}}">{{$value->reference}}</a></td>
                        <td>{{$value->buyer->name}}</td>
                        <td>{{$value->payment->name}}</td>
                        <td>{{$value->salesType->name}}</td>
                        <td>{{rupiah($value->amount)}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('reports.sales.addFilter')
@stop
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript">
$(function() {
    $('#sales-table').DataTable({
        dom : 'Blfrtip',
        buttons: [
            'pdfHtml5', 'excelHtml5'
        ],
    });
});
</script>
@stop