@extends('layouts.app')
@section('title', 'Report Warehouse '.date("Y-m-d"))
@section('content')
<section class="content-header">
 <h1>
    Warehouse
    <small>Report Warehouse </small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Warehouse</li>
  </ol>
</section>
<div class="content">
	<section class="connectedSortable ui-sortable">
		<div class="box box-solid bg-info" style="position: relative;left: 0px;top: 0px;">
			<div class="box-header ui-sortable-handle" style="cursor: move;">
				<h3 class="box-title" style="color: #303030">Stock Warehouse</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn bg-re btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body border-radius-none">
				<div class="chart" id="bar-chart" style="height: 250px;">
					
				</div>
			</div>
		</div>
	</section>
	<div class="row">
      <div class="col-lg-3 col-xs-6" >
        <!-- small box -->
        <div class="small-box bg-green"  style="min-height: 128px;">
          <div class="inner">
            <h3>{{$item_stock}}</h3>

            <p>Items Stock</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow" style="min-height: 128px;">
            <div class="inner">
              <h3 style="font-size: 24px;">{{$cost_value}}</h3>
              <p>Cost Value</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
          <!-- <a href="#" class="small-box-footer" style="margin-top: auto;">More info <i class="fa fa-arrow-circle-right"></i></a> -->
        	</div>
      </div>
	</div>
	<div class="box box-solid">
		<div class="box-header ui-sortable-handle" style="cursor: move;">
			<h3 class="box-title">Warehouse</h3>
			<div class="box-tools pull-right">
			<a href="#" class="btn btn-success filter" data-toggle="control-sidebar">Filter</a>
			</div>
		</div>
		<div class="box-body border-radius-none">
			<table class="table table-responsive" id="salesOrders-table">
			    <thead>
			        <tr>
			        <th>Item</th>
			        <th>Variant</th>
			        <th>Stock</th>
			        </tr>
			    </thead>
			</table>
		</div>
	</div>
</div>
@include('reports.warehouse.addFilter')
@stop
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript">
$(function() {
    $('#salesOrders-table').DataTable({
        processing: true,
        serverSide: true,
        dom : 'Blfrtip',
        buttons: [
          'excelHtml5', 'pdfHtml5'
        ],
        ajax: '{!! route('datatables.warehouseReport') !!}',
        columns: [
            { data: 'action', name: 'action'},
            { data: 'variant', render:function(data, type, row){
                var good = decodeHTMLEntities(data.replace(/[0-9]/g, '')).replace(/\W/g, '');
                
                if(good == 'cpsr'){
                    return 'Warna, Pattern, Size, Roll';
                }else if(good = 'cps'){
                    return 'Warna, Pattern, Size';
                }else if(good = 'cp'){
                    return 'Warna, Pattern';
                }else{
                    return 'Warna';
                }
            } , name: 'variant' },
            { data: 'value',name: 'value' }
        ]
    });
});
var line = new Morris.Bar({
    element          : 'bar-chart',
    resize           : true,
    data             : [
    @foreach($report_graph as $val)
      { item : '{{$val->name}}', a : {{$val->qtyItem}} },
    @endforeach
    ],
    xkey             : 'item',
    ykeys            : ['a'],
    labels           : ['Total Stock(s)'],
    lineColors       : ['#303030'],
    lineWidth        : 2,
    hideHover        : 'auto',
    gridTextColor    : '#303030',
    gridStrokeWidth  : 0.4,
    pointSize        : 4,
    pointStrokeColors: ['#00a65a'],
    gridLineColor    : '#00a65a',
    gridTextFamily   : ['Source Sans Pro','Helvetica Neue','Helvetica,Arial,sans-serif'],
    gridTextSize     : 13
  });
</script>
@stop