<aside class="control-sidebar control-sidebar-dark" id="sidebar-filter">
    <div class="tab-content">
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <form class="form-inline" action="{{route('filter.item')}}" method="POST">
        {{csrf_field()}}
          <div class="row">
            <div class="form-group col-lg-12">
              <h4>Item Filter</h4>
            </div>
            <div class="form-group  col-lg-12">
                <label>Nama Barang</label>
                <select name="name" class="cari form-control"><select>
            </div>
            <div class="form-group col-lg-12">
                <label>Variant</label>
                <select name="variant" id="variant" class="form-control"></select>
            </div>
            <div class="form-group col-lg-12">
              <label>Brand</label>
              <select class="select2 form-control" name="brand">
                <option value="">-Pilih Brand-</option>
                @foreach($brands as $brand)
                <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-lg-12">
              <label>Category</label>
              <select class="select2 form-control" name="category">
                <option value="">-Pilih Category-</option>
                @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-lg-12">
              <label>Warehouse</label>
              <select class="select2 form-control" name="warehouse">
                <option value="">-Pilih Warehouse-</option>
                 @foreach($warehouses as $warehouse)
                <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-lg-12">
              <label>Department</label>
              <select class="select2 form-control" name="department">
                <option value="">-Pilih Deparment-</option>
                 @foreach($departments as $department)
                <option value="{{$department->id}}">{{$department->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-lg-12">
              <input type="submit" class="btn btn-info" value="Submit"/>
              <a href="#" class="btn btn-default filter" data-toggle="control-sidebar">Batal</a>
            </div>
          </div>
       </form>
      </div>
    </div>
</aside> 
@push('scripts')
<script>
$(".cari").select2({
  placeholder : 'Mencari...',
  ajax: {
    url: "{{route('api.findNamaBarang')}}",
    dataType: 'json',
    delay: 250,
    processResults: function (data) {
      return {
        results:  $.map(data, function (item) {
          return {
            text: item.name,
            id: item.name
          }
        })
      };
    },
    cache: true
  }
});
$('.cari').on('change', function() {
  var data = $(".cari option:selected").val();
  $.post("{{route('api.findNamaVariant')}}", {_token : $('meta[name="csrf-token"]').attr('content'), id:data}, function(data){
    $("#variant").html(data);
    $("#variant").select2();
  });
});
$(".content-wrapper").click(function(e) 
{
    var container = $("#sidebar-filter");
    if(!container.is(e.target) && container.has(e.target).length === 0 && !$(e.target).hasClass('filter'))
    {
        $(container).attr('class', 'control-sidebar control-sidebar-dark');
    }
});
</script>
@endpush