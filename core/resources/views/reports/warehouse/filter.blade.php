@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1>
    Warehouse
    <small>Report Warehouse</small>
    <div class="pull-right">
        <a href="#" class="btn btn-success filter" data-toggle="control-sidebar">Filter</a>
    </div>
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>
    <div class="box box-danger">
        <div class="box-body">
            <table class="table table-responsive" id="sales-table">
                <thead>
                    <tr>
                        <th>Reference</th>
                        <th>Variant</th>
                        <th>Brand</th>
                        <th>Category</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $all = 0 ?>
                    @foreach($data as $index => $data)
                    <tr>
                        <td><a href="{{route('items.show', $data->id)}}">{{$data->name}}</a></td>
                        <td>{{getVariant($data->variant)}}</td>
                        <td>{{$data->brands->brand_name}}</td>
                        <td>{{$data->categories->name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('reports.warehouse.addFilter')
@stop
@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript">
$(function() {
    $('#sales-table').DataTable({
        dom : 'Blfrtip',
        buttons: [
            'pdfHtml5', 'excelHtml5'
        ]
    });
});
</script>
@stop