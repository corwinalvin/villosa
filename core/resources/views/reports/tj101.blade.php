@php
  $item_data  = array_chunk($variants->toArray(), 2);
@endphp
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE>PRINT</TITLE>
	<META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
	<META NAME="AUTHOR" CONTENT="X45U">
	<META NAME="CREATED" CONTENT="20131008;192100000000000">
	<META NAME="CHANGED" CONTENT="20180627;93032159193900">
	<STYLE TYPE="text/css">
	<!--
		@page { size: 8.47in 11.69in; margin-right: 0.1in; margin-top: 0.1in }
		P { margin-bottom: 0.1in; direction: ltr; color: #000000; line-height: 120%; widows: 2; orphans: 2 }
		P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		P.cjk { font-family: "Times New Roman", serif; font-size: 12pt; so-language: zh-CN }
		P.ctl { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ar-SA }
		TD P { margin-bottom: 0in; direction: ltr; color: #000000; widows: 2; orphans: 2 }
		TD P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		TD P.cjk { font-family: "Times New Roman", serif; font-size: 12pt; so-language: zh-CN }
		TD P.ctl { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ar-SA }
	-->

.break {
  page-break-after: always
}
	</STYLE>
</HEAD>
<BODY LANG="en-US" TEXT="#000000" DIR="LTR">
@for($i = 0; $i < count($item_data); $i++)
<TABLE WIDTH=756 CELLPADDING=1 CELLSPACING=0>
	<COL WIDTH=361>
	<COL WIDTH=28>
	<COL WIDTH=361>
	<TR>
		@foreach($item_data[$i] as $data)
		<TD WIDTH=361 STYLE="border: none; padding: 0in">
			<TABLE WIDTH=362 CELLPADDING=4 CELLSPACING=0>
				<COL WIDTH=63>
				<COL WIDTH=10>
				<COL WIDTH=208>
				<COL WIDTH=47>
				<TR>
					<TD COLSPAN=4 WIDTH=352 VALIGN=TOP STYLE="border: 1px solid #000000; padding: 0.04in">
						<P CLASS="western" ALIGN=CENTER>
							<FONT COLOR="#330066">
								<FONT SIZE=2 STYLE="font-size: 11pt">
									<B>PT. Maravillosa Indah Pratama</B>
								</FONT>
							</FONT>
						</P>
					</TD>
				</TR>
				<TR VALIGN=TOP>
					<TD WIDTH=63 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Article</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=10 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>:</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=208 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>{{$item->name}}
								</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=47 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0.04in">
						<P CLASS="western">
							<BR>
						</P>
					</TD>
				</TR>
				<TR VALIGN=TOP>
					<TD WIDTH=63 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Design</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=10 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>:</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=208 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>{{$data["pattern"]}}
								</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=47 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0.04in">
						<P CLASS="western">
							<BR>
						</P>
					</TD>
				</TR>
				<TR VALIGN=TOP>
					<TD WIDTH=63 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Warna</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=10 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>:</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=208 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>{{$data["warna"]}}</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=47 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0.04in">
						<P CLASS="western">
							<BR>
						</P>
					</TD>
				</TR>
				<TR VALIGN=TOP>
					<TD WIDTH=63 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Length</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=10 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>:</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=208 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>-</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=47 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0.04in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Cm</B>
							</FONT>
						</P>
					</TD>
				</TR>
				<TR VALIGN=TOP>
					<TD WIDTH=63 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Roll No.
								</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=10 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>:</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=208 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>{{$data["size"]}}</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=47 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0.04in">
						<P CLASS="western">
							<BR>
						</P>
					</TD>
				</TR>
				<TR VALIGN=TOP>
					<TD WIDTH=63 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Qty</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=10 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>:</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=208 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>-</B>
							</FONT>
						</P>
					</TD>
					<TD WIDTH=47 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0.04in; padding-left: 0.04in; padding-right: 0.04in">
						<P CLASS="western">
							<FONT SIZE=2 STYLE="font-size: 11pt">
								<B>Yard</B>
							</FONT>
						</P>
					</TD>
				</TR>
			</TABLE>
		</TD>
		<TD WIDTH=28 STYLE="border: none; padding: 0in">
			
		</TD>
		@endforeach
	</TR>
	<TR>
		<TD WIDTH=361 HEIGHT=28 STYLE="border: none; padding: 0in">
			<P CLASS="western" STYLE="margin-left: 0.1in; margin-right: 0.1in; margin-bottom: 0in; line-height: 100%">
			<BR>
			</P>
		</TD>
		<TD WIDTH=28 STYLE="border: none; padding: 0in">
			<P CLASS="western" STYLE="margin-left: 0.1in; margin-right: 0.1in; margin-bottom: 0in; line-height: 100%">
			<BR>
			</P>
		</TD>
		<TD WIDTH=361 STYLE="border: none; padding: 0in">
			<P CLASS="western" STYLE="margin-left: 0.1in; margin-right: 0.1in; margin-bottom: 0in; line-height: 100%">
			<BR>
			</P>
		</TD>
	</TR>
</TABLE>
@if($i == 4)
<div class="break"></div>
@endif
@endfor
</BODY>
</HTML>