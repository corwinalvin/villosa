@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1>
    Purchase
    <small>Report Purchase Order</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Purchase Order Detail</li>
    </ol>
</section>
<div class="content">
    <div class="clearfix"></div>
    <div class="box box-danger">
        <div class="box-body">
            <table class="table table-responsive" id="sales-table">
                <thead>
                    <tr>
                        <th>Reference</th>
                        <th>Supplier</th>
                        <th>Amount</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script type="text/javascript">
$(function() {
    $('#sales-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.purchaseReportDetail', $value) !!}',
        columns: [
            { data: 'action', name: 'action'},
            { data: 'suppliernya.name',name: 'suppliernya.name' },
            { data: 'amount',name: 'amount',render : function(data){
                return convertToRupiah(data);
            }}
        ]
    });
});
</script>
@stop