@extends('layouts.app')
@section('title', 'Report Purchase')
@section('content')
<section class="content-header">
 <h1>
    Purchase
    <small>Report Purchase Order </small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Purchase Order</li>
  </ol>
</section>
<div class="content">
	<section class="connectedSortable ui-sortable">
		<div class="box box-solid bg-red-gradient" style="position: relative;left: 0px;top: 0px;">
			<div class="box-header ui-sortable-handle" style="cursor: move;">
				<h3 class="box-title">Purchase Graph Per Quarter/Kwartal</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn bg-re btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body border-radius-none">
				<div class="chart" id="line-chart" style="height: 250px;">
					
				</div>
			</div>
		</div>
	</section>
	<div class="row">
		<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$total_sales}}</h3>

              <p>Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-cart"></i>
            </div>
            <a href="{{route('salesOrders.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6" >
          <!-- small box -->
          <div class="small-box bg-green"  style="min-height: 128px;">
            <div class="inner">
              <h3>{{$item_sold}}</h3>

              <p>Items Bought</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
	        <div class="small-box bg-yellow" style="min-height: 128px;">
	            <div class="inner">
	              <h3 style="font-size: 24px;">{{$cost_value}}</h3>
	              <p>Cost Value</p>
	            </div>
	            <div class="icon">
	              <i class="ion ion-cash"></i>
	            </div>
            <!-- <a href="#" class="small-box-footer" style="margin-top: auto;">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          	</div>
        </div>
        <div class="col-lg-3 col-xs-6">
	        <div class="small-box bg-red" style="min-height: 128px;">
	            <div class="inner">
	              <h3>{{$percentage}}<sup style="font-size: 20px">%</sup></h3>
	              <p>Percentage Compare A Month Ago</p>
	            </div>
	            <div class="icon">
	              <i class="ion ion-stats-bars"></i>
	            </div>
            <!-- <a href="#" class="small-box-footer" style="margin-top: auto;">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          	</div>
        </div>
	</div>
	<div class="box box-solid">
		<div class="box-header ui-sortable-handle" style="cursor: move;">
			<h3 class="box-title">Purchase</h3>
			<div class="box-tools pull-right">
				<a href="#" class="btn btn-success filter" data-toggle="control-sidebar">Filter</a>
			</div>
		</div>
		<div class="box-body border-radius-none">
			<table class="table table-responsive" id="salesOrders-table">
			    <thead>
			        <tr>
              <th>#</th>
			        <th>Date</th>
			        <th>Numbers of Orders</th>
			        <th>Number of Quantity</th>
			        <th>Approx. Harga</th>
			        </tr>
			    </thead>
			</table>
		</div>
	</div>
</div>
@include('reports.purchase.addFilter')
@stop
@section('scripts')
<script type="text/javascript">
$(function() {
    var table = $('#salesOrders-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.purchaseReport') !!}',
        columns: [
            {render : function(data, type, row, meta){
                return meta.row + meta.settings._iDisplayStart+1;
            }, searchable : false, orderable: false},
            { data: 'date', name: 'date', render: function (data) {
                var date = new Date(data);
                var month = date.getMonth() + 1;
                var full = (date.length > 1 ? date : "0" + date.getDate() +"-" + (month.length > 1 ? month : "0" + month)  + "-"  + date.getFullYear()) ;
                var url = "purchase/detail/"+full;
                return '<a href="'+url+'">'+full+'</a>';
            }},
            { data: 'perDate',name: 'perDate' },
            { data: 'qtyPerDate',name: 'qtyPerDate' },
            { data: 'approxVal',name: 'approxVal', render: function(data){
            	return convertToRupiah(data);
            }}
        ]
    });
});
var line = new Morris.Line({
    element          : 'line-chart',
    resize           : true,
    data             : [
     @foreach($report_graph as $report)
      { y: '{{$report->year}} Q{{$report->quarter}}', item1: {{$report->idcount}} },
     @endforeach
    ],
    xkey             : 'y',
    ykeys            : ['item1'],
    labels           : ['Total SO'],
    lineColors       : ['#303030'],
    lineWidth        : 2,
    hideHover        : 'auto',
    gridTextColor    : '#fff',
    gridStrokeWidth  : 0.4,
    pointSize        : 4,
    pointStrokeColors: ['#efefef'],
    gridLineColor    : '#efefef',
    gridTextFamily   : ['Source Sans Pro','Helvetica Neue','Helvetica,Arial,sans-serif'],
    gridTextSize     : 13
  });
</script>
@stop