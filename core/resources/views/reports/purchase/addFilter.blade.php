<aside class="control-sidebar control-sidebar-dark" id="sidebar-filter">
    <div class="tab-content">
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <form class="form-inline" action="{{route('filter.purchase')}}" method="POST">
        {{csrf_field()}}
          <div class="row">
            <div class="form-group col-lg-12">
              <h4>Purchase Order Filter</h4>
            </div>
            <div class="form-group  col-lg-6">
                <label>Tanggal Awal</label>
                <input type="text" name="early_date" class="form-control" id="datepicker">
            </div>
            <div class="form-group col-lg-6">
                <label>Tanggal Akhir</label>
                <input type="text" name="end_date" class="form-control" id="datepicker2">
            </div>
            <div class="form-group col-lg-12">
              <label>Supplier</label>
              <select class="select2 form-control" name="supplier">
                <option value="">-Pilih Supplier-</option>
                @foreach($suppliers as $supplier)
                  <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-lg-12">
              <label>Status</label>
              <select class="select2 form-control" name="status">
                <option value="0">Not Approved</option>
                <option value="1">Approved</option>
              </select>
            </div>
            <div class="form-group col-lg-12">
              <input type="submit" class="btn btn-info" value="Submit"/>
              <a href="#" class="btn btn-default filter" data-toggle="control-sidebar">Batal</a>
            </div>
          </div>
       </form>
      </div>
    </div>
</aside> 
@push('scripts')
<script>
$(".content-wrapper").click(function(e) 
{
    var container = $("#sidebar-filter");
    if(!container.is(e.target) && container.has(e.target).length === 0 && !$(e.target).hasClass('filter'))
    {
        $(container).attr('class', 'control-sidebar control-sidebar-dark');
    }
});
</script>
@endpush