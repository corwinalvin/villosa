@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1>
    Purchase
    <small>Report Purchase Order</small>
        <div class="pull-right">
            <a href="#" class="btn btn-success filter" data-toggle="control-sidebar">Filter</a>
        </div>
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>
    <div class="box box-danger">
        <div class="box-body">
            <table class="table table-responsive" id="sales-table">
                <thead>
                    <tr>
                        <th>Reference</th>
                        <th>Date</th>
                        <th>Supplier</th>
                        <th>Status</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($hasil as $hasil)
                    <tr>
                        <td><a href="{{route('purchaseOrders.show', $hasil->id)}}">{{$hasil->reference}}</a></td>
                        <td>{{$hasil->suppliernya->name}}</td>
                        <td>{{date('Y-m-d', strtotime($hasil->date))}}</td>
                        <td>{{getStatusItem($hasil->status)}}</td>
                        <td>{{rupiah($hasil->amount)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('reports.purchase.addFilter')
@stop
@section('scripts')
<script type="text/javascript">
$(function() {
    $('#sales-table').DataTable();
});
</script>
@stop