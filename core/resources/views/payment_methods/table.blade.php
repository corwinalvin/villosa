<table class="table table-responsive" id="paymentMethods-table">
    <thead>
        <tr>
        <th>Name</th>
        <th>Default</th>
        <th>Created By</th>
        <th>Action</th>
        </tr>
    </thead>
</table>

@section('scripts')
<script>
$(function() {
    $('#paymentMethods-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.paymentMethods') !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'default', "render":function(data, type, row){
                if(data == 1){
                    return 'Default';
                }else{
                    return 'Not Default';
                }
            } ,name: 'default' },
            { data: 'created_by.name', name: 'created_by' },
            { data : 'action' ,name:'action'}
        ]
    });
});
</script>
@stop