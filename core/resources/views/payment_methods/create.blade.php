@extends('layouts.app')
@section('title', 'Payment Methods')
@section('content')
    <section class="content-header">
        <h1>
            Payment Method
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-danger">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'paymentMethods.store']) !!}

                        @include('payment_methods.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
