<table class="table table-responsive" id="itemVariants-table">
    <thead>
        <tr>
            <th>Item Id</th>
        <th>Warna</th>
        <th>Pattern</th>
        <th>Size</th>
        <th>Roll</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($itemVariants as $itemVariant)
        <tr>
            <td>{!! $itemVariant->item_id !!}</td>
            <td>{!! $itemVariant->warna !!}</td>
            <td>{!! $itemVariant->pattern !!}</td>
            <td>{!! $itemVariant->size !!}</td>
            <td>{!! $itemVariant->roll !!}</td>
            <td>
                {!! Form::open(['route' => ['itemVariants.destroy', $itemVariant->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('itemVariants.show', [$itemVariant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('itemVariants.edit', [$itemVariant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>