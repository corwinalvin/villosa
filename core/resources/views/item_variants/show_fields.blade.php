<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $itemVariant->id !!}</p>
</div>

<!-- Item Id Field -->
<div class="form-group">
    {!! Form::label('item_id', 'Item Id:') !!}
    <p>{!! $itemVariant->item_id !!}</p>
</div>

<!-- Warna Field -->
<div class="form-group">
    {!! Form::label('warna', 'Warna:') !!}
    <p>{!! $itemVariant->warna !!}</p>
</div>

<!-- Pattern Field -->
<div class="form-group">
    {!! Form::label('pattern', 'Pattern:') !!}
    <p>{!! $itemVariant->pattern !!}</p>
</div>

<!-- Size Field -->
<div class="form-group">
    {!! Form::label('size', 'Size:') !!}
    <p>{!! $itemVariant->size !!}</p>
</div>

<!-- Roll Field -->
<div class="form-group">
    {!! Form::label('roll', 'Roll:') !!}
    <p>{!! $itemVariant->roll !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $itemVariant->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $itemVariant->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $itemVariant->deleted_at !!}</p>
</div>

