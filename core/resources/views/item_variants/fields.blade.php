<!-- Warna Field -->
<div class="form-group col-sm-6">
    {!! Form::label('warna', 'Warna:') !!}
    {!! Form::text('warna', null, ['class' => 'form-control']) !!}
</div>

<!-- Pattern Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pattern', 'Pattern:') !!}
    {!! Form::text('pattern', null, ['class' => 'form-control']) !!}
</div>

<!-- Size Field -->
<div class="form-group col-sm-6">
    {!! Form::label('size', 'Size:') !!}
    {!! Form::text('size', null, ['class' => 'form-control']) !!}
</div>

<!-- Roll Field -->
<div class="form-group col-sm-6">
    {!! Form::label('roll', 'Roll:') !!}
    {!! Form::text('roll', null, ['class' => 'form-control']) !!}
</div>

@foreach($stocks as $stock)
<div class="form-group col-sm-6">
    <label for="roll">Stock {{$stock->warehouses[0]->name}}:</label>
    <input type="text" name="value[{{$stock->warehouse}}]" value="{{$stock->value}}" class="form-control" />
</div>
@endforeach

<div class="form-group col-sm-6">
    {!! Form::label('price', 'Harga:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Picture Field  -->

@foreach(json_decode($itemVariant->picture) as $picture)
    @if($picture !== null)
    <div class="form-group col-sm-6">
        {!! Form::label('gambar', 'Gambar :') !!}
        {!! Form::text('picture[]', $picture, ['class' => 'form-control']) !!}
    </div>
    @else
    <div class="form-group col-sm-6">
        {!! Form::label('gambar', 'Gambar :') !!}
        <input type="text" name="picture[]" class="form-control">
    </div>
    @endif
@endforeach

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('items.index') !!}" class="btn btn-default">Cancel</a>
</div>
