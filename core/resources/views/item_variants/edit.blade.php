@extends('layouts.app')
@section('title', 'Item Variant')
@section('content')
    <section class="content-header">
        <h1>
            Item Variant
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($itemVariant, ['route' => ['itemVariants.update', $itemVariant->id_variant], 'method' => 'patch']) !!}

                        @include('item_variants.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection