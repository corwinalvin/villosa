@extends('layouts.app')
@section('title')
Dashboard
@endsection
@section('css')
<style type="text/css">
.bg-teal-gradient {
    background: #39cccc !important;
    background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #39cccc), color-stop(1, #7adddd)) !important;
    background: -ms-linear-gradient(bottom, #39cccc, #7adddd) !important;
    background: -moz-linear-gradient(center bottom, #39cccc 0, #7adddd 100%) !important;
    background: -o-linear-gradient(#7adddd, #39cccc) !important;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7adddd', endColorstr='#39cccc', GradientType=0) !important;
    color: #fff;
}
.info-box-text{
	margin-top: 15px;
}
</style>
@stop
@section('content')
<section class="content-header">
 <h1>
    Dashboard
    <small>Version 1.0</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<div class="content">
	 <div class="callout callout-success">
        <h4>Halo,&nbsp; {{Auth::user()->name}}</h4>
        <p>Silahkan menggunakan aplikasi Dashboard Maravillosa</p>
      </div>
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Profit SO</span>
					<span class="info-box-number" style="color:red;">{{$cost_value}}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">PO Cost</span>
					<span class="info-box-number" style="color:red;">{{$cost_po}}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<!-- fix for small devices only -->
		<div class="clearfix visible-sm-block"></div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Total Order (SO)</span>
					<span class="info-box-number">{{$total_order}}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-yellow"><i class="ion ion-load-c"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Pending Order (SO)</span>
					<span class="info-box-number">{{$pending_order}}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<section class="col-lg-12 connectedSortable ui-sortable">		
			<div class="box box-solid bg-teal-gradient" style="position: relative;left: 0px;top: 0px;">
				<div class="box-header ui-sortable-handle" style="cursor: move;">
	              <i class="fa fa-th"></i>
	              <h3 class="box-title">Sales Order Graph</h3>
	              <div class="box-tools pull-right">
	                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	              </div>
	            </div>
	            <div class="box-body border-radius-none">
	            	<div class="chart" id="line-chart" style="height: 250px;">
	            		
	            	</div>
	            </div>
			</div>
		</section>
		<section class="col-lg-12 connectedSortable ui-sortable">		
			<div class="box box-solid bg-green-gradient" style="position: relative;left: 0px;top: 0px;">
				<div class="box-header ui-sortable-handle" style="cursor: move;">
	              <i class="fa fa-th"></i>
	              <h3 class="box-title">Purchase Order Graph</h3>
	              <div class="box-tools pull-right">
	                <button type="button" class="btn bg-green btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	              </div>
	            </div>
	            <div class="box-body border-radius-none">
	            	<div class="chart" id="line-chartPO" style="height: 250px;">
	            		
	            	</div>
	            </div>
			</div>
		</section>
		<section class="col-lg-6 connectedSortable ui-sortable">
			<div class="box box-solid bg-info" style="position: relative;left: 0px;top: 0px;">
				<div class="box-header ui-sortable-handle" style="cursor: move;">
					<h3 class="box-title" style="color:#303030">Stock Warehouse</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn bg-re btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<div class="box-body border-radius-none">
					<div class="chart" id="bar-chart" style="height: 250px;">
						
					</div>
				</div>
			</div>
		</section>
		<section class="col-lg-6 connectedSortable ui-sortable">
			<div class="box box-solid bg-info" style="position: relative;left: 0px;top: 0px;">
				<div class="box-header ui-sortable-handle" style="cursor: move;">
					<h3 class="box-title" style="color:#303030">Pending SO or Invoices</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn bg-re btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<div class="box-body border-radius-none">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Reference</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Created By</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data_pending_order as $po)
								<tr>
									<td><a href="{{route('salesOrders.show', $po->id)}}">{{$po->reference}}</a></td>
									<td>{{$po->date->toDateString()}}</td>
									<td>{{rupiah($po->amount)}}</td>
									<td>{{$po->createdBy->name}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
var line = new Morris.Line({
    element          : 'line-chart',
    resize           : true,
    data             : [
     @foreach($report_graph as $report)
      { y: '{{$report->year}} Q{{$report->quarter}}', item1: {{$report->idcount}} },
     @endforeach
    ],
    xkey             : 'y',
    ykeys            : ['item1'],
    labels           : ['Total SO'],
    lineColors       : ['#303030'],
    lineWidth        : 2,
    hideHover        : 'auto',
    gridTextColor    : '#fff',
    gridStrokeWidth  : 0.4,
    pointSize        : 4,
    pointStrokeColors: ['#efefef'],
    gridLineColor    : '#efefef',
    gridTextFamily   : ['Source Sans Pro','Helvetica Neue','Helvetica,Arial,sans-serif'],
    gridTextSize     : 13
  });
var line = new Morris.Line({
    element          : 'line-chartPO',
    resize           : true,
    data             : [
     @foreach($report_graphPO as $report)
      { y: '{{$report->year}} Q{{$report->quarter}}', item1: {{$report->idcount}} },
     @endforeach
    ],
    xkey             : 'y',
    ykeys            : ['item1'],
    labels           : ['Total PO'],
    lineColors       : ['#303030'],
    lineWidth        : 2,
    hideHover        : 'auto',
    gridTextColor    : '#fff',
    gridStrokeWidth  : 0.4,
    pointSize        : 4,
    pointStrokeColors: ['#efefef'],
    gridLineColor    : '#efefef',
    gridTextFamily   : ['Source Sans Pro','Helvetica Neue','Helvetica,Arial,sans-serif'],
    gridTextSize     : 13
});
var line = new Morris.Bar({
    element          : 'bar-chart',
    resize           : true,
    data             : [
    @foreach($report_graphWH as $val)
      { item : '{{$val->name}}', a : {{$val->qtyItem}} },
    @endforeach
    ],
    xkey             : 'item',
    ykeys            : ['a'],
    labels           : ['Total Stock(s)'],
    barColors       : ['#303030'],
    gridTextColor    : '#888',
    pointSize        : 4,
    gridTextFamily   : ['Source Sans Pro','Helvetica Neue','Helvetica,Arial,sans-serif'],
    gridTextSize     : 13
  });
</script>
@stop