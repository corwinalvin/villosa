<table class="table table-responsive" id="brands-table">
    <thead>
        <tr>
            <th>Brand Name</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#brands-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.brands') !!}',
        columns: [
            { data: 'brand_name', name: 'brand_name' },
            { data : 'action' ,name:'action'}
        ]
    });
});
</script>
@stop