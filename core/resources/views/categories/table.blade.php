<table class="table table-responsive" id="categories-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Created By</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#categories-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.categories') !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'created_by.name', name: 'created_by' },
            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@stop