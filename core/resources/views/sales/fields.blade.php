@section('css')
<style>
.switch{position:relative;display:inline-block;width:60px;height:34px}.switch input{display:none}.slider{position:absolute;cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:#ccc;-webkit-transition:.4s;transition:.4s}.slider:before{position:absolute;content:"";height:26px;width:26px;left:4px;bottom:4px;background-color:#fff;-webkit-transition:.4s;transition:.4s}input:checked+.slider{background-color:#2196F3}input:focus+.slider{box-shadow:0 0 1px #2196F3}input:checked+.slider:before{-webkit-transform:translateX(26px);-ms-transform:translateX(26px);transform:translateX(26px)}.slider.round{border-radius:34px}.slider.round:before{border-radius:50%}
</style>
@stop
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Included Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_included', 'Tax Included:') !!}
    {!! Form::number('tax_included', null, ['class' => 'form-control']) !!}
</div>

<!-- Default Sales Types Field -->
<div class="form-group col-sm-6">
    {!! Form::label('default_sales_types', 'Default Sales Types:') !!}
    <br>
    <!-- Rounded switch -->
    <label class="switch">  
    {!! Form::hidden('default', false) !!}
    {!! Form::checkbox('default', '1', null) !!} 
      <span class="slider round"></span>
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('salesType.index') !!}" class="btn btn-default">Cancel</a>
</div>
