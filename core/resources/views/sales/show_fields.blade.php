<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sales->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $sales->name !!}</p>
</div>

<!-- Tax Included Field -->
<div class="form-group">
    {!! Form::label('tax_included', 'Tax Included:') !!}
    <p>{!! $sales->tax_included !!}</p>
</div>

<!-- Default Sales Types Field -->
<div class="form-group">
    {!! Form::label('default_sales_types', 'Default Sales Types:') !!}
    <p>{!! $sales->default_sales_types !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $sales->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sales->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sales->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $sales->deleted_at !!}</p>
</div>

