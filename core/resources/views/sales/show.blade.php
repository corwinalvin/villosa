@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Sales
        </h1>
    </section>
    <div class="content">
        <div class="box box-danger">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('sales.show_fields')
                    <a href="{!! route('salesType.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
