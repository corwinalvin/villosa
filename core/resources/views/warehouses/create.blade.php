@extends('layouts.app')
@section('title')
Create Warehouse
@stop
@section('content')
    <section class="content-header">
        <h1>
            Warehouse
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-danger">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'warehouses.store']) !!}

                        @include('warehouses.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
