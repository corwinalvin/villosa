@extends('layouts.app')
@section('title')
Warehouses
@stop
@section('content')
    <section class="content-header">
        <h1>
            Warehouse
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-danger">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($warehouse, ['route' => ['warehouses.update', $warehouse->id], 'method' => 'patch']) !!}

                        @include('warehouses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection