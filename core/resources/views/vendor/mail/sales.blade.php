@component('mail::message')
# {{$intro}}

Orderan Anda {{$order_id}} sudah {!!$status!!}

Terimakasih,<br>
{{ config('app.name') }}
@endcomponent
