<table class="table table-responsive" id="purchaseOrders-table">
    <thead>
        <tr>
            <th>Supplier</th>
            <th>Date</th>
            <th>Reference</th>
            <th>Status</th>
            <th>Created By</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#purchaseOrders-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.purchaseOrder') !!}',
        columns: [
            { data: 'suppliernya.name', name: 'suppliernya.name' },
            { data: 'date', name: 'date', render: function (data) {
                var date = new Date(data);
                var month = date.getMonth() + 1;
                return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();
            }},
            { data: 'reference',name: 'reference' },
            { data: 'status', name: 'status', render: function (data) {
                if(data==0){
                    return '<div style="cursor:pointer !important;border: 1px solid red !important;color: red !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;">UNAPPROVED</div>';
                }else{
                     return '<div style="border: 1px solid #3c8dbc !important;color: #3c8dbc !important;background-color: #FFFFFF;display: inline-block;padding: 5px 15px;border-radius: 2px;">APPROVED</div>';
                }
            } },
            { data : 'created_by.name' ,name:'createdBy'},            
            { data : 'action' ,name:'action'}
        ],
        order : [[1, "asc"]]
    });
});
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
</script>
@stop