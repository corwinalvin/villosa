@php
  $item_count = ceil(count($purchaseOrder->purchaseOrders) / 13);
  $temp_item  = 10;
  $curent     = 0;
  $item_data  = array_chunk($purchaseOrder->purchaseOrders->toArray(), 13);
@endphp
<!DOCTYPE html>
<html>
<head>
  <title>Purchase Order {{$purchaseOrder->reference}}</title>
</head>
<style>
html, body {
    /*margin: 0;*/
    font-size: 11px;
  line-height: 150%;
}

@page {
  size: 21.59cm 13.97cm;
}

.logo {
  width: 30%;
}

.borderless td, .borderless th {
    border: none !important;
}
.s {
   border: none !important;
   background:white !important;
}
.bordertable{
  border-left: 1px solid #000000;
  border-bottom: 1px solid #000000;
  border-top: 1px solid #000000;
  border-right: 1px solid #000000;
}

.borderth{
  border-bottom: 1px solid #000000;
  /*border-top: 1px solid #000000;
  bord*/er-right: 1px solid #000000;
}
.bordertd{
  border-right: 1px solid #000000;
}
.classtr{
  size: 21.59cm 13.97cm;
}
tr.spaceUnder > td
{
  padding-bottom: -0.5em;
}

.footer {
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  text-align: center;
}


.break {
  page-break-after: always
}

</style>
<body style="margin: -30px -30px -30px -30px; font-family: Tahoma,Arial, sans-serif;" >
  @for($i = 0; $i < count($item_data); $i++)
  <!-- For untuk masing2 page -->
    <div id="wrapper">  
      <table >
        <tr>
          <td style="width: 557px; font-size: 20px blod;" colspan="7">SALES INVOICE</td>
          <td colspan="2">No Invoice :</td>
          <td align="left">#{{$purchaseOrder->reference}}</td>
        </tr>
        <tr>
          <td valign="top" rowspan="4">Supplier</td>
          <td colspan="5" style="padding: 5px;" class="bordertable"  width="60">{{$purchaseOrder->suppliernya->name}}</td>
          <td></td>
          <td colspan="2">Invoice Date </td>
          <td></td>
        </tr>
        <tr>
          <td valign="top" rowspan="3" colspan="5" style="border-left: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;padding: 5px " width="60">
            {{$purchaseOrder->suppliernya->street}} <br>
            Email:{{$purchaseOrder->suppliernya->email}} Telp.{{$purchaseOrder->suppliernya->phone}}
          </td>
          <td></td>
          <td></td>
          <td colspan="2" align="center">{{ date('d M Y', strtotime($purchaseOrder->created_at)) }}</td>
        </tr>
        <tr>
          <td></td>
          <td>Term</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>Salesman</td>
          <td colspan="2" class="bordertable" align="center"> {{$purchaseOrder->createdBy->name}}</td>
        </tr>
      </table>
      <table id="salesInvoice">
        <tr>
          <td align="center" style="width: 200px" class="borderth">Item Description</td>
          <td align="center" style="width: 80px;" class="borderth">Qty</td>
          <td align="center" style="width: 100px;" class="borderth">Rate</td>
          <td align="center" style="width: 100px;" class="borderth">Amount</td>
        </tr>
        @php
          (float)$tot = $dis = $rp = $discP = $discPt = 0;
        @endphp
        @foreach($item_data[$i] as $k => $items)
            @php
              $itemF = (object)$items;
              $item = (object)$itemF->items;
              $itemV = (object)$itemF->item_variant;
            @endphp
            <tr class="spaceUnder">
            <td>{{ $item->name }} ({{$itemV->warna}} {{(isset($itemV->pattern)) ? ',' : '' }} {{$itemV->pattern}}{{(isset($itemV->size)) ? ',' : '' }} {{$itemV->size}}{{(isset($itemV->roll)) ? ',' : '' }} {{$itemV->roll}})</td>
            <td align="right">{{$itemF->qty}}</td>
            <td align="right">{{ rupiah($itemV->price,0,".",".")  }}</td>
            <td align="center">
                {{rupiah($rp = $itemV->price * $itemV->roll * $itemF->qty)}}
            </td>
            @php
              $tot +=$rp;
              @endphp
            </td>
          </tr>
        @endforeach
      </table>
      <br>
      <div class="footer">
        <table>
        <tr>
          <td  style="width: 379px; border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black"" colspan="7">{{ $purchaseOrder->note }}</td>
          <td></td>
          <td style="width: 100px;">&nbsp;</td>
          <td class="bordertable" style="width: 134px;">Sub Total</td>
          <td class="bordertable" colspan="2" align="right" style="width: 100px">{{ rupiah($tot) }}</td>
        </tr>
        <tr>
          <td colspan="2" align="center">Prepared By</td>
          <td></td>
          <td colspan="2" align="center">Approved By</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td class="bordertable" style="width: 134px;">Total Invoice</td>
          <td class="bordertable" colspan="2" align="right" style="width: 100px">
            {{ rupiah($tot) }}
          </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2" style="border-bottom: 1px solid #000000; height: 10px"></td>
          <td style="height: 25px"></td>
          <td colspan="2" style="border-bottom: 1px solid #000000"></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
      </div>
    </div>
    <div class="break"></div>
  @endfor

  
</body>
</html>