<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $warehouseTransfer->id !!}</p>
</div>

<!-- Warehouse From Field -->
<div class="form-group">
    {!! Form::label('warehouse_from', 'Warehouse From:') !!}
    <p>{!! $warehouseTransfer->warehouse_from !!}</p>
</div>

<!-- Warehouse To Field -->
<div class="form-group">
    {!! Form::label('warehouse_to', 'Warehouse To:') !!}
    <p>{!! $warehouseTransfer->warehouse_to !!}</p>
</div>

<!-- Item Variant Field -->
<div class="form-group">
    {!! Form::label('item_variant', 'Item Variant:') !!}
    <p>{!! $warehouseTransfer->item_variant !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $warehouseTransfer->value !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $warehouseTransfer->created_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $warehouseTransfer->created_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $warehouseTransfer->updated_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $warehouseTransfer->updated_by !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $warehouseTransfer->deleted_at !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
    {!! Form::label('deleted_by', 'Deleted By:') !!}
    <p>{!! $warehouseTransfer->deleted_by !!}</p>
</div>

