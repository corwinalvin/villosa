<table class="table table-responsive" id="warehouseTransfers-table">
    <thead>
        <tr>
            <th>No WTO</th>
            <th>Warehouse From</th>
            <th>Warehouse To</th>
            <th>Item Variant</th>
            <th>Value</th>
            <th>Created By</th>
            <th>Created At</th>
        </tr>
    </thead>
</table>

@section('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script>
$(function() {
    $('#warehouseTransfers-table').DataTable({
        processing: true,
        serverSide: true,
        dom : 'Blfrtip', 
        buttons : ['pdfHtml5', 'excelHtml5'],
        ajax: '{!! route('datatables.warehouseTransfer') !!}',
        columns: [
            { data: 'no_wto', name: 'no_wto'},
            { data: 'warehouse_from.name', name: 'warehouseFrom' },
            { data: 'warehouse_to.name', name : 'warehouseTo'},
            { data: function (data, row, metaData){
                    var p = c = s = r = "";
                    if(data.variants.pattern){
                        p = data.variants.pattern;
                    }
                    if(data.variants.warna){
                        c = data.variants.warna;
                    }
                    if(data.variants.size){
                        s = data.variants.size;
                    }
                    if(data.variants.roll){
                        r = data.variants.roll;
                    }

                    return data.item.name + " (" + p + " " + c + " " + s + " " + r +")"; 

            }, name : ['variants', 'item']},
            { data: 'value', name : 'value'},
            { data: 'created_by.name', name: 'created_by' },
            { data : 'created_at' ,name:'created_at', render: function (data) {
                var date = new Date(data);
                
                var month = date.getMonth() + 1;
                return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear() + " " + ("0"+date.getHours()).slice(-2)+":"+(date.getMinutes()<10?'0':'') + date.getMinutes()+":"+date.getSeconds();
            }}
        ]
    });
});
</script>
@stop