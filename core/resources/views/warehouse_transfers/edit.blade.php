@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Warehouse Transfer
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-danger">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($warehouseTransfer, ['route' => ['warehouseTransfers.update', $warehouseTransfer->id], 'method' => 'patch']) !!}

                        @include('warehouse_transfers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection