@extends('layouts.app')
@section('title')
Warehouse Transfer
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Warehouse Transfers</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('warehouseTransfers.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-danger">
            <div class="box-body">
                    @include('warehouse_transfers.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

