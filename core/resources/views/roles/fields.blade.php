<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Display Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('display_name', 'Display Name:') !!}
    {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('permission', 'Permission:') !!}
    <br>
    @if(!strpos(preg_replace('/[^A-Za-z0-9\-]/', '',Request::url()), "edit") === false)
    @foreach($permissions as $index =>  $permission)
        <div class="col-sm-3">
            @if(in_array($permission->id, $pers))
                <input type="checkbox" name="permission[]" value="{{$permission->id}}" checked="checked"> {{$permission->display_name}}
            @else
                <input type="checkbox" name="permission[]" value="{{$permission->id}}"> {{$permission->display_name}}
            @endif
        </div>
    @endforeach
    @else
    @foreach($permissions as $index =>  $permission)
        <div class="col-sm-3">
            <input type="checkbox" name="permission[]" value="{{$permission->id}}"> {{$permission->display_name}}
        </div>
    @endforeach
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('roles.index') !!}" class="btn btn-default">Cancel</a>
</div>
