@extends('layouts.app')
@section('title')
Suppliers
@stop
@section('content')
    <section class="content-header">
        <h1>
            Supplier
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-danger">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($supllier, ['route' => ['suplliers.update', $supllier->id], 'method' => 'patch']) !!}

                        @include('suplliers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection