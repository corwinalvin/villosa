<table class="table table-responsive" id="suplliers-table">
    <thead>
        <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Status</th>
        <th>Created By</th>
        <th>Action</th>
        </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#suplliers-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.suplliers') !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'status', name: 'status' },
            { data: 'created_by.name' ,name: 'created_by' },
            { data : 'action' ,name:'action'}
        ]
    });
});
</script>
@stop