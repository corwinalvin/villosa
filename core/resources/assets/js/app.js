
require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'vue-nprogress'
import axios  from "axios";
import VueAxios from "vue-axios";
import VueCarousel from "vue-carousel";
import underscore from "vue-underscore";
import lodash from 'lodash';
import Notifications from 'vue-notification'
import moment from 'moment';

window.Vue = Vue;
Vue.use(lodash)
Vue.use(underscore)
Vue.use(VueRouter)
Vue.use(NProgress)
Vue.use(VueCarousel)
Vue.use(VueAxios, axios)
Vue.use(Notifications)
Vue.filter('rupiah', rupiah)
Vue.filter('datetime', datetime)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

let Product = Vue.component('Product', require('./components/Product.vue'));
let App = Vue.component('App', require('./components/App.vue'));
let Home = Vue.component('Home', require('./components/Home.vue'));
let Cart = Vue.component('Cart', require('./components/Cart.vue'));
let Search = Vue.component('Search', require('./components/Search.vue'));

let PageNotFound = Vue.component("page-not-found", {
    template: "",
    created: function () {
        // Redirect outside the app using plain old javascript
        window.location.href = "blabla";
    }
});

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/:slug',
            name: 'product.detail',
            component: Product
        },
        {
            path : '/customer/cart',
            name : 'cart',
            component : Cart
        },
        {
            path : '/search/query=:search',
            name : 'search',
            component: Search
        },
        {
            path: "*",
            component: PageNotFound
        }
    ],
});

const nprogress = new NProgress({ parent: '.nprogress-container' })

const app = new Vue({
    el: '#app',
    components: { App, nprogress},
    router,
});

function rupiah(value) {
    if (value) {
        var rupiah = '';
        var angkarev = value.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
        return 'Rp ' + rupiah.split('', rupiah.length - 1).reverse().join('');
    }
}
function datetime(value) {
    if (value) {
        return moment(String(value)).format('DD MMMM YYYY - hh:mm')
    }
}